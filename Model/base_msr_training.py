from Model.datasets import MSRVTTBaseLMDB, MSRVTTBase
from Model.losses import TripletLoss, BatchHardTripetLoss, MeanInvertedRank
from Model.networks import BaseModel2017
from Model.trainer import fit
from Model.utils.logger import TrainingLogger
from torch.utils.data import DataLoader
from torch.optim.rmsprop import RMSprop
from torch.optim import lr_scheduler


import torch


if __name__ == '__main__':
    start_epoch = -1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = BaseModel2017(9449, 9614, 35, 500, 1024, 500, 2048, device)
    model = model.to(device)
    train_loss = BatchHardTripetLoss(0.2)
    val_loss = MeanInvertedRank()
    train_loss = train_loss.to(device)
    lr = 0.0001
    optimizer = RMSprop(model.parameters(), lr=lr)
    n_epochs = 50
    log_interval = 4
    cuda_on = True
    shuffle_dataset = True
    ramdom_seed = 42
    train_batch_size = 128

    save = True
    states_path = "/run/media/nbravo/Elements/states/base2/run1/"
    log_path = "Model/log/base2/run1/"
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            log_path + "val_loss.csv", log_path + "val_loss_epoch.csv",
                            log_path + "learning_rate.csv", states_path, save=save)

    training_dataset = MSRVTTBaseLMDB("Model/data/base2_lmdb_flickr30m/msr")
    validation_dataset = MSRVTTBaseLMDB("Model/data/base2_lmdb_flickr30m/vtt16_A")

    training_dataloader = DataLoader(training_dataset, batch_size=train_batch_size, shuffle=shuffle_dataset,
                                     num_workers=0)
    validation_dataloader = DataLoader(validation_dataset, batch_size=len(validation_dataset), shuffle=False,
                                       num_workers=0)

    fit(training_dataloader, validation_dataloader, model, train_loss, val_loss, optimizer, n_epochs, cuda_on,
        log_interval, logger, start_epoch=start_epoch)
