#!/usr/bin/env python
from Model.datasets import BaseVTT17MatchRankDataset
from Model.networks import Base2018
from Model.utils.utils import load_checkpoint
from torch import cosine_similarity
import torch


def match_and_rank(video_tensor, sentence_tensors):
    ranking = []  #list of objecst {index, distance}
    for i in range(len(sentence_tensors)):
        dist = cosine_similarity(video_tensor.squeeze(0), sentence_tensors[i], dim=0).item()
        ranking.append({"index": i + 1, "distance": dist})
    sort = sorted(ranking, key=lambda pair: pair['distance'], reverse=True)
    return list(map(lambda pair: pair['index'], sort))


if __name__ == '__main__':
    subset = "5"
    subsubsets = ["a", "b", "c", "d", "e"]
    save_path = "Model/data/vtt2017/base2018/msr_3/matching/subset{}/{}.match"

    with torch.no_grad():
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        print("Loading model...")
        model = Base2018()
        checkpoint = load_checkpoint("/run/media/nbravo/Elements/states/base2018_2/all_base_1/", 9)
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        model = model.to(device)
        print("Loading dataset...")
        dataset = BaseVTT17MatchRankDataset(subset, subsubsets, save_path, device)

        print("Loading sentences from subset {}...",format(subset))
        for k in range(len(subsubsets)):
            subsubset = subsubsets[k]
            print("Subsubset", subsubset)
            y_sentences = model.textBranch(torch.stack(dataset.sentences[k][0]),
                                           torch.stack(dataset.sentences[k][2]), torch.stack(dataset.sentences[k][3]).squeeze(1))
            for i in range(len(dataset.resnet152_vectors)):
                print("Video", dataset.videos_index[i])
                video = model.videoBranch(dataset.resnet152_vectors[i].unsqueeze(0), torch.Tensor([dataset.n_shots[i]]).cuda())
                ranking = match_and_rank(video, y_sentences)
                dataset.save_matching(i, subset, subsubset, ranking)
