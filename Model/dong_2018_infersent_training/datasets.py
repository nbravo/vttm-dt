import torch
import lmdb
import numpy as np
import pandas as pd
from torch.utils.data.dataset import Dataset
import pyarrow as pa
import os.path as osp
import pickle


def read_text(text_path):
    try:
        with open(text_path, "rb") as fin:
            text = pickle.load(fin)
        return text
    except Exception:
        print(text_path)
        exit(1)

class VTTOnlyInfersentLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1, rexnet101_model='mediamill'):
        super(VTTOnlyInfersentLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        assert rexnet101_model in ['mediamill', 'mxnet'], "invalid Resnext101 model"
        self.rexnext_index = 6 if rexnet101_model == 'mediamill' else 8
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        cursor.close()
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        # dataset_name, infersent \
        # resnext_vector, n_shots
        return self.dataset_name, vectors[5], \
               vectors[self.rexnext_index], int(vectors[self.rexnext_index + 1])

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def collate_data(self, data):
        dataset_name, infersent, resnext, n_shots= zip(*data)

        infersent = np.array(infersent)
        n_shots = np.array(n_shots)
        max_n_shots = min(max(n_shots), self.video_max_shots)
        resnext_mean = np.zeros((len(resnext), len(resnext[0][0])))
        rnn_resnext = np.zeros((len(resnext), max_n_shots + 2, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots + 2))
        for i in range(len(resnext)):
            resnext_mean[i] = np.mean(resnext[i], 0)
            end = max_n_shots if n_shots[i] > self.video_max_shots else n_shots[i]
            rnn_resnext[i, :end + 2] = np.concatenate(([self.video_init_vector], resnext[i][:end], [self.video_end_vector]), axis=0)
            video_masks[i, :end + 2] = 1.0
            n_shots[i] = end + 2

        return torch.Tensor(infersent), \
           torch.Tensor(resnext_mean), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots)


class VTTAllInfersentLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1, rexnet101_model='mediamill'):
        super(VTTAllInfersentLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        assert rexnet101_model in ['mediamill', 'mxnet'], "invalid Resnext101 model"
        self.rexnext_index = 6 if rexnet101_model == 'mediamill' else 8
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        cursor.close()
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        # dataset_name, bow, rnn, sent_len, infersent \
        # resnext_vector, n_shots
        return self.dataset_name, vectors[2],  vectors[3], int(vectors[4]), vectors[5], \
               vectors[self.rexnext_index], int(vectors[self.rexnext_index + 1])

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def collate_data(self, data):
        dataset_name, bow, seq_indices, lens, infersent, resnext, n_shots= zip(*data)

        max_len = max(lens)
        rnn_seq_indices = 2*np.ones((len(seq_indices), max_len))
        bow_mean = np.zeros((len(resnext), len(bow[0])))
        infersent = np.array(infersent)
        n_shots = np.array(n_shots)
        max_n_shots = min(max(n_shots), self.video_max_shots)
        resnext_mean = np.zeros((len(resnext), len(resnext[0][0])))
        rnn_resnext = np.zeros((len(resnext), max_n_shots + 2, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots + 2))
        for i in range(len(resnext)):
            bow_mean[i] = bow[i] / np.sum(bow[i])
            rnn_seq_indices[i, :lens[i]] = seq_indices[i]
            resnext_mean[i] = np.mean(resnext[i], 0)
            end = max_n_shots if n_shots[i] > self.video_max_shots else n_shots[i]
            rnn_resnext[i, :end + 2] = np.concatenate(([self.video_init_vector], resnext[i][:end], [self.video_end_vector]), axis=0)
            video_masks[i, :end + 2] = 1.0
            n_shots[i] = end + 2

        return torch.Tensor(bow_mean), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), torch.Tensor(infersent), \
           torch.Tensor(resnext_mean), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots)


class VTT18AllInfersentMatchRankDataset(Dataset):
    def __init__(self, sets, device, resnext_model="mediamill", video_init_vector=None, video_end_vector=None):
        super(VTT18AllInfersentMatchRankDataset, self).__init__()
        assert resnext_model in ['mediamill', 'mxnet'], "invalid Resnext101 model"
        self.device = device
        if resnext_model == "mediamill":
            self.videos_csv = pd.read_csv("Model/data/vtt2018/infersent/videos_mediamill.csv")
        else:
            self.videos_csv = pd.read_csv("Model/data/vtt2018/infersent/videos_mxnet.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        self.infersent = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s], self.infersent[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/infersent/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []
        infersent_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
            infersent_vectors.append(read_text(sentences.iloc[i, 4]))
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors, infersent_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, infersent, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.LongTensor(rnn_seq_indices).to(device), torch.Tensor(lens).to(device), torch.Tensor(infersent).to(device)


class VTT18AllInfersentMatchRankDatasetTraining(Dataset):

    def __init__(self, device, sample_folder, video_init_vector=None, video_end_vector=None):
        super(VTT18AllInfersentMatchRankDatasetTraining, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv(sample_folder + 'videos.csv')
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)
        self.sent_ids, self.bow_mean, self.rnn, self.lens, self.infersent = self.load_sentences(pd.read_csv(sample_folder + 'sentences.csv'))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []
        infersent_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
            infersent_vectors.append(read_text(sentences.iloc[i, 4]))
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors, infersent_vectors

    def save_matching(self, sample_folder, model, matching_sent_list):
        save_path = sample_folder + 'matching/' + model + '.match'
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(sent_id for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, infersent, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.LongTensor(rnn_seq_indices).to(device), torch.Tensor(lens).to(device), torch.Tensor(infersent).to(device)


class VTT18OnlyInfersentMatchRankDataset(Dataset):
    def __init__(self, sets, device, resnext_model="mediamill", video_init_vector=None, video_end_vector=None):
        super(VTT18OnlyInfersentMatchRankDataset, self).__init__()
        assert resnext_model in ['mediamill', 'mxnet'], "invalid Resnext101 model"
        self.device = device
        if resnext_model == "mediamill":
            self.videos_csv = pd.read_csv("Model/data/vtt2018/infersent/videos_mediamill.csv")
        else:
            self.videos_csv = pd.read_csv("Model/data/vtt2018/infersent/videos_mxnet.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.infersent = {}
        for s in sets:
            self.sent_ids[s], self.infersent[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/infersent/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        infersent_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            infersent_vectors.append(read_text(sentences.iloc[i, 4]))
        return sentence_id, infersent_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, infersent, device):
        infersent = np.array(infersent)
        return (torch.Tensor(infersent).to(device),)


class VTT18OnlyInfersentMatchRankDatasetTraining(Dataset):
    def __init__(self, device, sample_folder, video_init_vector=None, video_end_vector=None):
        super(VTT18OnlyInfersentMatchRankDatasetTraining, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv(sample_folder + 'videos.csv')
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids, self.infersent = self.load_sentences(pd.read_csv(sample_folder + 'sentences.csv'))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        infersent_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            infersent_vectors.append(read_text(sentences.iloc[i, 4]))
        return sentence_id, infersent_vectors

    def save_matching(self, sample_folder, model, matching_sent_list):
        save_path = sample_folder + 'matching/' + model + '.match'
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(sent_id for sent_id in ranking) + '\n')

    def collate_sentences(self, infersent, device):
        infersent = np.array(infersent)
        return (torch.Tensor(infersent).to(device),)