import torch
import torch.nn as nn
import pickle
import numpy as np
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


from Model.networks import MFC, l2norm, VideoBranch


class TextBranchOnlyInfersent(nn.Module):

    def __init__(self, infersent_size, csv_size, dropout):
        super(TextBranchOnlyInfersent, self).__init__()
        self.text_mapping = MFC([infersent_size, csv_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, infersent_tensor):
        features = self.text_mapping(infersent_tensor)
        features = l2norm(features)
        return features


class TextBranchAllInfersent(nn.Module):

    def __init__(self, voc_size, w2v_dim, infersent_size, gru_hidden_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn):  # kernel num = 512, kernel sizes 2-3-4
        super(TextBranchAllInfersent, self).__init__()
        self.rnn_output_size = 2 * gru_hidden_size
        self.dropout = nn.Dropout(dropout)
        self.embedding = nn.Embedding(voc_size + 4, w2v_dim, padding_idx=2)
        if rnn == 'gru':
            self.rnn = nn.GRU(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        self.text_mapping = MFC([voc_size + infersent_size + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout,
                                have_bn=True, have_last_bn=True)

        self.init_weights()

    def init_weights(self):
        embeddings = pickle.load(open("TextVectorization/objects/msr_tgif_msvd/rnn/vectors.pkl", 'rb'))
        for i in range(4):
            embeddings[i] = np.random.uniform(-1, 1, 500)
        self.embedding.weight.data.copy_(torch.from_numpy(embeddings))

    def forward(self, bow_mean, gru_vector, lengths, infersent):
        # Level 1
        org_out = bow_mean

        # Level 2
        embs = self.embedding(gru_vector)
        embs = pack_padded_sequence(embs, lengths, batch_first=True, enforce_sorted=False)
        gru_init_out, _ = self.rnn(embs)
        padded = pad_packed_sequence(gru_init_out, batch_first=True)
        gru_init_out = padded[0]
        gru_out = torch.zeros(padded[0].size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(padded[0]):
            gru_out[i] = torch.mean(batch[:int(lengths[i]), :], 0)
        gru_out = self.dropout(gru_out)

        # Level 3
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((gru_out, con_out, org_out, infersent), 1)
        features = self.text_mapping(features)
        features = l2norm(features)
        return features


class Base2018OnlyInfersent(nn.Module):

    def __init__(self, infersent_size, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018OnlyInfersent, self).__init__()
        self.textBranch = TextBranchOnlyInfersent(infersent_size, cvs_size, dropout)
        self.videoBranch = VideoBranch(2048, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, infersent, resnext101_mean, resnext101_tensor, video_masks, n_shots):
        y_text = self.textBranch(infersent)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots)
        return y_video, y_text


class Base2018AllInfersent(nn.Module):

    def __init__(self, voc_size, infersent_size, text_rnn_size, text_kernel_num, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018AllInfersent, self).__init__()
        self.textBranch = TextBranchAllInfersent(voc_size, 500, infersent_size, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranch(2048, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, bow_mean, gru_vector, lengths, infersent, resnext101_mean, resnext101_tensor, video_masks, n_shots):
        y_text = self.textBranch(bow_mean, gru_vector, lengths, infersent)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots)
        return y_video, y_text
