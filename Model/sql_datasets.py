import psycopg2
import random
import torch
import torch.cuda
import numpy as np

from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader
from psycopg2.extras import RealDictCursor


def get_table_ids(cursor, table):
    query = "SELECT id from {}".format(table)
    cursor.execute(query)
    return [row for (row,) in cursor.fetchall()]


def batch(indices, batch_size):
    for i in range(0, len(indices), batch_size):
        yield indices[i:i + batch_size]


class MsvdSQLDataset(Dataset):

    def __init__(self, database, table, batch_size, shuffle=False):
        super(MsvdSQLDataset, self).__init__()
        self.conn = psycopg2.connect(user="nbravo", host="127.0.0.1", port=5432, database=database)
        self.cursor = self.conn.cursor()
        self.indices = get_table_ids(self.cursor, table)
        self.batch_size = batch_size
        self.table = table
        if shuffle:
            idx_copy = self.indices.copy()
            random.shuffle(idx_copy)
            self.batched_indices = list(batch(idx_copy, self.batch_size))
        else:
            self.batched_indices = list(batch(self.indices, self.batch_size))

    def __getitem__(self, index):
        # batched get
        indices = self.batched_indices[index]
        query = """SELECT video_id::INT, resnext101, bow, w2v, gru, sent_len, infersent 
                   FROM %s 
                   WHERE id = ANY(ARRAY%s)""" % (self.table, indices)
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        video_ids = []
        resnet = []
        bow = []
        w2v = []
        rnn = []
        lens = []
        infersent = []
        for row in data:
            video_ids.append(row[0])
            #resnet.append(row[1])
            bow.append(row[2])
            w2v.append(row[3])
            rnn.append(row[4])
            lens.append(row[5])
            infersent.append(row[6])
        video_ids = torch.Tensor(video_ids)
        #resnet = torch.Tensor(resnet)
        bow = torch.Tensor(bow)
        w2v = torch.Tensor(w2v)
        rnn = torch.LongTensor(rnn)
        lens = torch.Tensor(lens)
        infersent = torch.Tensor(lens)
        return index, [video_ids,bow, w2v, rnn ,lens, infersent]

    def __len__(self):
        return len(self.indices)

    def close(self):
        self.cursor.close()
        self.conn.close()


if __name__ == '__main__':

    d = DataLoader(MsvdSQLDataset("vtt", "msvd", 128, shuffle=True), shuffle=False, batch_size=1, num_workers=0)
    for index, input in d:
        print(len(input))