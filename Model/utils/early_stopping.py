import numpy as np


class EarlyStopping:
    def __init__(self, step_patience, early_stop_patience, verbose=False):
        self.step_patience = step_patience
        self.early_stop_patience = early_stop_patience
        self.verbose = verbose
        self.step_counter = 0
        self.early_stop_counter = 0
        self.best_score = None
        self.step = False
        self.early_stop = False
        self.val_loss_min = np.Inf

    def __call__(self, val_loss):

        score = val_loss

        if self.best_score is None:
            self.best_score = score
        elif score <= self.best_score:
            self.step_counter += 1
            self.early_stop_counter += 1
            print('Step counter: {} out of {}'.format(self.step_counter, self.step_patience))
            print('Early stop counter: {} out of {}'.format(self.early_stop_counter, self.early_stop_patience))
            if self.step_counter >= self.step_patience:
                self.step = True
                self.step_counter = 0
            if self.early_stop_counter >= self.early_stop_patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.step_counter = 0
            self.early_stop_counter = 0