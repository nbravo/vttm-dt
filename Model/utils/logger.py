from torch import load as torch_load, save as torch_save


class TrainingLogger:

    def __init__(self, train_loss_path, train_loss_epoch_path, val_loss_path, val_loss_epoch_path, lr_path, states_path,
                 save=True, n_flush=10, val_metric=False):
        self.save = save
        self.train_loss_file = None
        self.train_log_lines = 0
        self.val_loss_file = None
        self.val_log_lines = 0
        self.states_path = states_path
        self.lr_path = lr_path
        self.n_flush = n_flush
        self.train_loss_epoch_path = train_loss_epoch_path
        self.val_loss_epoch_path = val_loss_epoch_path
        if self.save:
            with open(self.lr_path, "w") as f:
                f.write("epoch,lr\n")
            with open(self.train_loss_epoch_path, "w") as f:
                f.write("epoch,loss\n")
            with open(self.val_loss_epoch_path, "w") as f:
                f.write("epoch,loss\n")
            self.train_loss_file = open(train_loss_path, "a")
            self.train_loss_file.write("epoch,index,loss\n")
            if val_loss_path:
                self.val_loss_file = open(val_loss_path, "a")
                if val_metric:
                    self.val_loss_file.write("epoch,index,r1,r5,r10,medr,meanr\n")
                else:
                    self.val_loss_file.write("epoch,index,loss\n")

    def log_learning_rate(self, epoch, lr):
        if self.save:
            with open(self.lr_path, "a") as f:
                f.write(str(epoch) + "," + str(lr) + "\n")

    def log_train_loss(self, epoch, index, loss):
        if self.save:
            self.train_loss_file.write(str(epoch) + "," + str(index) + "," + "{:.4f}".format(loss) + "\n")
            self.train_log_lines += 1
            if self.train_log_lines % self.n_flush == 0:
                self.train_loss_file.flush()

    def log_train_loss_epoch(self, epoch, loss):
        if self.save:
            with open(self.train_loss_epoch_path, "a") as f:
                f.write(str(epoch) + "," + "{:.4f}".format(loss) + "\n")

    def log_val_loss(self, epoch, index, loss):
        if self.save and self.val_loss_file:
            self.val_loss_file.write(str(epoch) + "," + str(index) + "," + "{:.4f}".format(loss) + "\n")
            self.val_log_lines += 1
            if self.val_log_lines % self.n_flush == 0:
                self.val_loss_file.flush()

    def log_val_loss_metrics(self, epoch, index, r1, r5, r10, medr, meanr):
        if self.save and self.val_loss_file:
            self.val_loss_file.write(str(epoch) + "," + str(index) +
                                     "," + "{:.3f}".format(r1) +
                                     "," + "{:.3f}".format(r5) +
                                     "," + "{:.3f}".format(r10) +
                                     "," + "{:.0f}".format(medr) +
                                     "," + "{:.3f}".format(meanr) + "\n")
            self.val_log_lines += 1
            if self.val_log_lines % self.n_flush == 0:
                self.val_loss_file.flush()

    def log_val_loss_epoch(self, epoch, loss):
        if self.save and self.val_loss_epoch_path:
            with open(self.val_loss_epoch_path, "a") as f:
                f.write(str(epoch) + "," + "{:.4f}".format(loss) + "\n")

    def save_checkpoint(self, epoch, checkpoint):
        if self.save:
            target_state = self.states_path + 'check' + str(epoch) + '.pytorch'
            torch_save(checkpoint, target_state)
            print('Epoch saved')

    def load_checkpoint(self, epoch):
        return torch_load(self.states_path + 'check' + str(epoch) + '.pytorch')

    def save_best_model(self, checkpoint):
        if self.save:
            target_state = self.states_path + "best_model.pytorch"
            torch_save(checkpoint, target_state)

    def load_best_model(self):
        return torch_load(self.states_path + "best_model.pytorch")

    def close(self):
        if self.save:
            self.train_loss_file.close()
            self.train_log_lines = 0
            if self.val_loss_file:
                self.val_loss_file.close()
                self.val_log_lines = 0
