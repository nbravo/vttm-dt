import os.path as osp
import lmdb
import pyarrow as pa
from Model.datasets import MSRVTTBase
from torch.utils.data import DataLoader
import random


def dumps_pyarrow(obj):
    """
    Serialize an object.
    Returns:
        Implementation-dependent bytes-like object
    """
    return pa.serialize(obj).to_buffer()

def create_lmdb():
    write_frequency = 100
    training_dataset = MSRVTTBase("/home/nbravo/vttm-dt/Model/data/VTT2016/vtt16_A_dataset_infersent_3.csv", 69, for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x, batch_size=1, shuffle=True)
    lmdb_path = "../data/infersent_lmdb/vtt16_A"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*500, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        index, bow_tensor, w2v_tensor, infersent_tensor, gru_tensor, lens_tensor, resnet152_tensor, n_shots = data[0]
        txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, bow_tensor, w2v_tensor, infersent_tensor, gru_tensor, lens_tensor, resnet152_tensor, n_shots)))
        if idx % write_frequency == 0:
            print("[%d/%d]" % (idx, len(msr_dl)))
            txn.commit()
            txn = db.begin(write=True)

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(len(training_dataset))]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()

if __name__ == '__main__':
    create_lmdb()
