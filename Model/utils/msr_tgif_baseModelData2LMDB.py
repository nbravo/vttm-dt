import os.path as osp
import lmdb
import pyarrow as pa
from Model.datasets import MSRVTTBase
from torch.utils.data import DataLoader


def dumps_pyarrow(obj):
    """
    Serialize an object.
    Returns:
        Implementation-dependent bytes-like object
    """
    return pa.serialize(obj).to_buffer()


def create_lmdb():
    BATCH_SIZE = 100
    write_frequency = 1000
    training_dataset = MSRVTTBase("/home/nbravo/vttm-dt/Model/data/msr/msr_tgif_dataset_base.csv", 69, for_lmdb=True)
    dataset = DataLoader(training_dataset, collate_fn=lambda x: x, batch_size=BATCH_SIZE)
    lmdb_path = "../data/base_lmdb/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*250, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    loaded = 0
    index = 0
    for idx, data in enumerate(dataset):
        try:
            for i in range(BATCH_SIZE):
                index, bow_tensor, w2v_tensor, infersent_tensor, gru_tensor, lens_tensor, resnet152_tensor, n_shots = data[i]
                if n_shots == 0:
                    continue
                txn.put(u'{}'.format(loaded).encode('ascii'), dumps_pyarrow((index, bow_tensor, w2v_tensor, infersent_tensor, gru_tensor, lens_tensor, resnet152_tensor, n_shots)))
                loaded += 1
            if loaded % write_frequency == 0:
                print("[%d/%d]" % (loaded, len(dataset)*BATCH_SIZE))
                txn.commit()
                txn = db.begin(write=True)
        except Exception:
            print("Index Error", index)
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(loaded)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


if __name__ == '__main__':
    create_lmdb()
