import os
import pandas as pd
import csv
import pickle

DATASET_PATH = "/run/media/nbravo/Elements/VTT2016/"
GT_FILE = "vtt.gt"

RESNET152_PATH = DATASET_PATH + "resnet152/embeddings/"

INFERSENT_PATH = DATASET_PATH + "sentences_{}/infersent/"

video_names = pd.Series(os.listdir(RESNET152_PATH)).map(lambda x: x.split(".")[0]).tolist()


def base_model_dataset():
    with open("vtt16_A_dataset_infersent_2.csv", 'w') as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("video_id", "resnet152_path", "infersent_path"))
        gt_file = open(GT_FILE, 'r')
        line = gt_file.readline().strip()
        while line:
            video_id, sentence_a, sentence_b = line.split(" ")
            try:
                video_names.index(video_id)
                writer.writerow((
                    video_id,
                    RESNET152_PATH + video_id + ".pkl",
                    INFERSENT_PATH.format('A') + "sentence" + sentence_a + ".pkl",
                ))
            except ValueError:
                pass
            line = gt_file.readline().strip()


if __name__ == '__main__':
    base_model_dataset()

