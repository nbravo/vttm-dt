import csv
import os
import pickle

DATASET_PATH = "/run/media/nbravo/Elements/VTT2019/"

VIDEOS_PATH = DATASET_PATH + "videos/"
RESNET152_MAX_PATH = DATASET_PATH + "resnet152/max/"
RESNEXT101_MAX_PATH = DATASET_PATH + "resnext101/max/"
DT_PATH = DATASET_PATH + "dts_hist/"

BOW_PATH = DATASET_PATH + "subset_{}/bow/"
W2V_PATH = DATASET_PATH + "subset_{}/w2v/"
GRU_PATH = DATASET_PATH + "subset_{}/gru/"
LEN_PATH = DATASET_PATH + "subset_{}/lens/"

SENT_SUBSET_FILE = DATASET_PATH + "matching.ranking.files/tv19.vtt.descriptions.{}"

SUBSETS = ["A", "B", "C", "D", "E"]


# with open("videos.csv", 'w') as out_f:
#    writer = csv.writer(out_f, delimiter=',', quotechar='"')
#    writer.writerow(("video_index", "resnet152_path", "resnext101_path", "dt_path"))
#    videos = os.listdir(VIDEOS_PATH)
#    for file in videos:
#        video_id, extension = file.split(".")
#        writer.writerow((video_id, RESNET152_MAX_PATH + video_id + ".pkl", RESNEXT101_MAX_PATH + video_id + ".pkl", DT_PATH + file + ".json"))


for ss in  SUBSETS:
    subset_file = SENT_SUBSET_FILE.format(ss)
    with open("sentences_{}.csv".format(ss), "w") as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("id", "bow_path", "w2v_path", "gru_path", "len"))
        with open(subset_file, 'r') as fin:
            line = fin.readline()
            while line:
                index, _ = line.split(" ", 1)
                filename = "sentence" + str(index) + ".pkl"
                writer.writerow((index,
                             BOW_PATH.format(ss) + filename, W2V_PATH.format(ss) + filename, GRU_PATH.format(ss) + filename,
                             pickle.load(open(LEN_PATH.format(ss) + filename, "rb"))))
                line = fin.readline()

