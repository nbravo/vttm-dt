import os
import pandas as pd
import csv
import pickle

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/MSR/"

RESNET152_PATH = "/run/media/nbravo/Elements/MSR/resnet152/embeddings/"
RESNEXT101_PATH = DATASET_PATH + "resnext101/max/"
DT_PATH = DATASET_PATH + "dts_hist/"

SAVE_PATH = "/run/media/nbravo/Elements/MSR/"

INFERSENT_PATH = SAVE_PATH + "infersent/"

banned_w2v = pd.read_csv("no_w2v.csv")['id'].unique()

video_names = pd.Series(os.listdir(RESNET152_PATH))
video_names = video_names.map(lambda x: x.split(".")[0])

sentences = pd.Series(os.listdir(INFERSENT_PATH))
sentence_only_video_name = sentences.map(lambda x: x.split("_")[1].split(".")[0])


def base_model_dataset():
    with open("msr_dataset_infersent.csv", 'w') as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("video_id", "resnet152_path", "infersent_path"))
        for i in range(0, len(video_names)):
            video_name = video_names[i]
            #if video_name in banned:
            #        continue
            for sentence_index in list(sentence_only_video_name[sentence_only_video_name == video_name].index):
                if sentences[sentence_index] in banned_w2v:
                    continue
                writer.writerow((video_name[5:],
                                 RESNET152_PATH + video_name + ".pkl",
                                 INFERSENT_PATH + sentences[sentence_index]))
            if i % 100 == 0:
                print(i, "of", len(video_names))


if __name__ == '__main__':
    base_model_dataset()


