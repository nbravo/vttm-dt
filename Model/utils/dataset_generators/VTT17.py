import csv
import os
import pickle

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017/"

VIDEOS_PATH = DATASET_PATH + "videos/"
RESNET152_MAX_PATH = "/run/media/nbravo/Elements/VTT2017/resnet152/embeddings/"
RESNEXT101_MAX_PATH = DATASET_PATH + "resnext101/max/"
DT_PATH = DATASET_PATH + "dts_hist/"

BOW_PATH = DATASET_PATH + "flickr30m_sw/subset_{}/{}/bow/"
W2V_PATH = DATASET_PATH + "flickr30m_sw/subset_{}/{}//w2v/"
GRU_PATH = DATASET_PATH + "flickr30m_sw/subset_{}/{}/gru/"
LEN_PATH = DATASET_PATH + "flickr30m_sw/subset_{}/{}/lens/"

SENT_SUBSET_FILE = DATASET_PATH + "matching.ranking.files/tv19.vtt.descriptions.{}"
VIDEO_SET_FILE = "/run/media/nbravo/nbravo-2TB/VTT2017_INFO/matching.ranking.eval.readme/vtt.matching.ranking.set.{}.gt"

SET = "5"
SUBSETS = ["A", "B", "C", "D", "E"]


with open("videos{}.csv".format(SET), 'w') as out_f:
   writer = csv.writer(out_f, delimiter=',', quotechar='"')
   writer.writerow(("video_index", "resnet152_path"))
   videos = VIDEO_SET_FILE.format(SET)
   with open(videos, 'r') as f:
       line = f.readline()
       while line:
           video_id  = line.split(" ")[0]
           writer.writerow((video_id, RESNET152_MAX_PATH + video_id + ".pkl"))
           line = f.readline()

