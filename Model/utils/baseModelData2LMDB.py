import os.path as osp
import lmdb
import pyarrow as pa
from Model.datasets import MSRVTTBase
from torch.utils.data import DataLoader


def dumps_pyarrow(obj):
    """
    Serialize an object.
    Returns:
        Implementation-dependent bytes-like object
    """
    return pa.serialize(obj).to_buffer()


def msr():
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/msr_tgif_msvd/msr_dataset.csv", for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/base_lmdb/msr/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*100, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots = data[0]
            if n_shots <= 1:
                assert_errors += 1
                print("ERROR:", index, "len", n_shots)
                continue
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots)))
            if idx % write_frequency == 0:
                print("MSR [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def tgif():
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/msr_tgif_msvd/tgif_dataset.csv", for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/base_lmdb/tgif/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*100, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots)))
            if idx % write_frequency == 0:
                print("TGIF [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def msvd():
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/msr_tgif_msvd/msvd_dataset.csv", for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/base_lmdb/msvd/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*70, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots)))
            if idx % write_frequency == 0:
                print("MSVD [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def vtt16(subset):
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/VTT2016/vtt16_{}_all_voc_dataset.csv".format(subset), for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/base_lmdb/VTT2016_{}/".format(subset)
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*5, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens_tensor, resnext101_tensor, n_shots)))
            if idx % write_frequency == 0:
                print("VTT16 [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def vtt16_videos(subset):
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/VTT2016/vtt16_{}_dataset.csv".format(subset), 60, for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/infersent_lmdb/VTT2016_{}_videos/".format(subset)
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*5, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, infersent_tensor, resnext101_tensor, n_shots = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, resnext101_tensor, n_shots)))
            if idx % write_frequency == 0:
                print("VTT16 [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def vtt16_sentences(subset):
    write_frequency = 100
    training_dataset = MSRVTTBase("../data/VTT2016/vtt16_{}_dataset.csv".format(subset), for_lmdb=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/infersent_lmdb/VTT2016_{}_sentences/".format(subset)
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*5, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, infersent_tensor, resnext101_tensor, n_shots = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, infersent_tensor)))
            if idx % write_frequency == 0:
                print("VTT16 [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


if __name__ == '__main__':
    tgif()