import os.path as osp
import lmdb
import pyarrow as pa
from Model.datasets import AllDataset
from torch.utils.data import DataLoader


def dumps_pyarrow(obj):
    """
    Serialize an object.
    Returns:
        Implementation-dependent bytes-like object
    """
    return pa.serialize(obj).to_buffer()


def msr():
    write_frequency = 100
    training_dataset = AllDataset("../data/msr_tgif_msvd/all/msr_dataset.csv")
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/all_lmdb/msr/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*90, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor, resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor,
                                                                      resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y)))
            if idx % write_frequency == 0:
                print("MSR [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def tgif():
    write_frequency = 100
    training_dataset = AllDataset("../data/msr_tgif_msvd/all/tgif_dataset.csv")
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/all_lmdb/tgif/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*130, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor, resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor,
                                                                      resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y)))
            if idx % write_frequency == 0:
                print("TGIF [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def msvd():
    write_frequency = 100
    training_dataset = AllDataset("../data/msr_tgif_msvd/all/msvd_dataset.csv")
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/all_lmdb/msvd/"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*100, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor, resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor,
                                                                      resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y)))
            if idx % write_frequency == 0:
                print("MSVD [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


def vtt16(subset):
    write_frequency = 100
    training_dataset = AllDataset("/home/nbravo/vttm-dt/Model/data/msr_tgif_msvd/all/vtt16_A_dataset.csv".format(subset))
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "../data/all_lmdb/VTT2016_{}/".format(subset)
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*5, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor, resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor,
                                                                      resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y)))
            if idx % write_frequency == 0:
                print("VTT16 [%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()


if __name__ == '__main__':
    vtt16("A")
