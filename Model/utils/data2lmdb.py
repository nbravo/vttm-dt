import os.path as osp
import lmdb
import pyarrow as pa
from Model.datasets import MSRVTT
from torch.utils.data import DataLoader


def dumps_pyarrow(obj):
    """
    Serialize an object.
    Returns:
        Implementation-dependent bytes-like object
    """
    return pa.serialize(obj).to_buffer()


def create_lmdb():
    write_frequency = 100
    training_dataset = MSRVTT("data/msr/msr_dataset_training_new.csv", train=True)
    msr_dl = DataLoader(training_dataset, collate_fn=lambda x: x)
    lmdb_path = "/run/media/nbravo/Elements/lmdb/msr"
    isdir = osp.isdir(lmdb_path)
    print("Generating")
    db = lmdb.open(lmdb_path, subdir=isdir,
                    map_size=1073741824*500, readonly=False,
                    meminit=False, map_async=True)
    assert_errors = 0
    txn = db.begin(write=True)
    for idx, data in enumerate(msr_dl):
        try:
            index, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, resnet152_tensor, resnext101_tensor, dts_tensor = data[0]
            txn.put(u'{}'.format(idx).encode('ascii'), dumps_pyarrow((index, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, resnet152_tensor, resnext101_tensor, dts_tensor)))
            if idx % write_frequency == 0:
                print("[%d/%d]" % (idx, len(msr_dl)))
                txn.commit()
                txn = db.begin(write=True)
        except AssertionError:
            assert_errors += 1
            continue

     # finish iterating through dataset
    txn.commit()
    keys = [u'{}'.format(k).encode('ascii') for k in range(idx + 1 - assert_errors)]
    with db.begin(write=True) as txn:
        txn.put(b'__keys__', dumps_pyarrow(keys))
        txn.put(b'__len__', dumps_pyarrow(len(keys)))

    print("Flushing database ...")
    db.sync()
    db.close()

if __name__ == '__main__':
    create_lmdb()