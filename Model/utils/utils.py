import json
import pickle
from torch import save as torch_save, load as torch_load
import re
import torch


def read_obj(objs_path):
    with open(objs_path, "r") as fin:
        objs = json.load(fin)['name']
    obj_histogram = []
    for object_class in sorted(objs.keys()):
        obj_histogram.append(objs[object_class])
    assert len(obj_histogram) == 80, "Object histogram must have exactly 80  values"
    return obj_histogram


def read_torch(path):
    return torch.load(path)


def read_dt(dt_path):
    with open(dt_path, "r") as fin:
        dts_hist = json.load(fin)
    return dts_hist


def read_text(text_path):
    try:
        with open(text_path, "rb") as fin:
            text = pickle.load(fin)
        return text
    except Exception:
        print(text_path)
        exit(1)

def read_kmeans(i):
    with open("objects/kmeans" + str(i) + ".pkl", 'rb') as fin:
        kmeans = pickle.load(fin)
    return kmeans


def read_pca(i):
    with open("objects/pca" + str(i) + ".pkl", "rb") as fin:
        pca = pickle.load(fin)
    return pca


def normalize(s):
    """
    Given a text, cleans and normalizes it.
    """
    s = s.lower()
    # Replace ips
    s = re.sub(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', ' _ip_ ', s)
    # Isolate punctuation
    s = re.sub(r'([\'\"\.\(\)\!\?\-\\\/\,])', r' \1 ', s)
    # Remove some special characters
    s = re.sub(r'([\;\:\|•«\n])', ' ', s)
    # Replace numbers and symbols with language
    s = s.replace('&', ' and ')
    s = s.replace('@', ' at ')
    s = s.replace('0', ' zero ')
    s = s.replace('1', ' one ')
    s = s.replace('2', ' two ')
    s = s.replace('3', ' three ')
    s = s.replace('4', ' four ')
    s = s.replace('5', ' five ')
    s = s.replace('6', ' six ')
    s = s.replace('7', ' seven ')
    s = s.replace('8', ' eight ')
    s = s.replace('9', ' nine ')
    return s


def text_to_vector(text, model):
    """
    Given a string, normalizes it and convert it to a sentence vector.
    """
    text = normalize(text)
    return model.get_sentence_vector(text)


def load_checkpoint(states_path, epoch):
    return torch_load(states_path + 'check' + str(epoch) + '.pytorch')


def load_best_model(model_path):
    return torch_load(model_path + "best_model.pytorch")