from Model.datasets import MSRVTTBaseLMDB, MSRVTTBase
from Model.losses import TripletLoss, MeanInvertedRank
from Model.networks import Base2018InfersentNoGru
from Model.trainer import fit
from Model.utils.logger import TrainingLogger
from torch.utils.data import DataLoader
from torch.optim import Adam
from torch.optim import lr_scheduler


import torch


if __name__ == '__main__':
    start_epoch = -1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = Base2018InfersentNoGru()
    model = model.to(device)
    train_loss = TripletLoss(margin=0.2)
    val_loss = MeanInvertedRank()
    train_loss = train_loss.to(device)
    lr = 0.0001
    optimizer = Adam(model.parameters(), lr=lr)
    n_epochs = 50
    log_interval = 4
    cuda_on = True
    shuffle_dataset = True
    ramdom_seed = 42
    train_batch_size = 128

    save = True
    states_path = "/run/media/nbravo/Elements/states/dong2018/msr_all_second_infersent_3/"
    log_path = "Model/log/dong2018/msr_all_second_infersent_3/"
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            log_path + "val_loss.csv", log_path + "val_loss_epoch.csv",
                            log_path + "learning_rate.csv", states_path, save=save)

    training_dataset = MSRVTTBaseLMDB("Model/data/infersent_lmdb/msr_all")
    validation_dataset = MSRVTTBaseLMDB("Model/data/infersent_lmdb/vtt17_2_A")

    training_dataloader = DataLoader(training_dataset, batch_size=train_batch_size, shuffle=shuffle_dataset,
                                     num_workers=5)
    validation_dataloader = DataLoader(validation_dataset, batch_size=len(validation_dataset), shuffle=False,
                                       num_workers=0)

    fit(training_dataloader, validation_dataloader, model, train_loss, val_loss, optimizer, n_epochs, cuda_on,
        log_interval, logger, start_epoch=start_epoch)
