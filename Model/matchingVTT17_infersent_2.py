from Model.datasets import BaseVTT17MatchRankDataset
from Model.networks import BaseModel
from Model.utils.utils import load_checkpoint
from torch import cosine_similarity
import torch


def match_and_rank(video_tensor, sentence_tensors):
    ranking = []  #list of objecst {index, distance}
    for i in range(len(sentence_tensors)):
        dist = cosine_similarity(video_tensor, sentence_tensors[i], dim=0).item()
        ranking.append({"index": i + 1, "distance": dist})
    sort = sorted(ranking, key=lambda pair: pair['distance'], reverse=True)
    return list(map(lambda pair: pair['index'], sort))


if __name__ == '__main__':
    subset = "2"
    subsubsets = ["a", "b"]

    with torch.no_grad():
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        print("Loading model...")
        model = BaseModel(10258, 10423, 35, 500, 1024, 4096, 2048, device)
        checkpoint = load_checkpoint("/run/media/nbravo/Elements/states/base_msr_tgif/infersent/", 36)
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        model = model.to(device)
        print("Loading dataset...")
        dataset = BaseVTT17MatchRankDataset(subset, subsubsets, device)

        print("Loading sentences from subset {}...",format(subset))
        for k in range(len(subsubsets)):
            subsubset = subsubsets[k]
            print("Subsubset", subsubset)
            y_sentences = model.textBranch(torch.stack(dataset.sentences[k][0]), torch.stack(dataset.sentences[k][1]),
                                           torch.stack(dataset.sentences[k][2]), torch.stack(dataset.sentences[k][3]).squeeze(1))
            for i in range(len(dataset.resnet152_vectors)):
                print("Subset", subsubset, "Video", dataset.videos_index[i])
                video = dataset.resnet152_vectors[i].sum(dim=0) / dataset.n_shots[i]
                ranking = match_and_rank(video, y_sentences)
                dataset.save_matching(i, subset, subsubset, ranking)
