from Model.datasets import MSRVTTBaseLMDB
from Model.losses import TripletLoss, MeanInvertedRank, MixRankLoss
from Model.networks import Base2018
from Model.trainer import fit
from Model.utils.logger import TrainingLogger
from Model.utils.early_stopping import EarlyStopping

import torch
import os
import time

from torch.utils.data import DataLoader, ConcatDataset
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter
from bayes_opt import BayesianOptimization


def check_dir(dir_path, save=True, overwrite=False):
    if save:
        try:
            os.listdir(dir_path)
        except FileNotFoundError:
            os.makedirs(dir_path)


def train_model(lr, train_batch_size,
                t_rnn_size, t_kernel_size, v_rnn_size, v_kernel_size, cvs, dropout):
    message = "lr {:.4f} batch_size {:.0f}\ntext_rnn_size {:.0f} text_kernel_size {:.0f}\nvideo_rnn_size {:.0f} video_kernel_size {:.0f}\ncvs {:.0f}\ndropout {:.4f}".format(
        lr, train_batch_size, t_rnn_size, t_kernel_size, v_rnn_size, v_kernel_size, cvs, dropout
    )
    print(message)
    init_time = time.time()
    start_epoch = -1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    loss = TripletLoss(max_violation=True, margin=0.2, direction='all')
    loss = loss.to(device)
    val_loss = MixRankLoss()
    val_loss = val_loss.to(device)
    n_epochs = 1
    log_interval = 100
    cuda_on = True
    workers = 8
    earlystopping = EarlyStopping(3, 10, verbose=True)
    msr_dataset = MSRVTTBaseLMDB("Model/data/base_lmdb/msr", "MSR", max_readers=workers)
    tgif_dataset = MSRVTTBaseLMDB("Model/data/base_lmdb/tgif", "TGIF", max_readers=workers)
    msvd_dataset = MSRVTTBaseLMDB("Model/data/base_lmdb/msvd", "MSVD", max_readers=workers)
    training_dataset = ConcatDataset((msr_dataset, tgif_dataset, msvd_dataset))

    validation_dataset_A = MSRVTTBaseLMDB("Model/data/base_lmdb/VTT2016_A", "VTT2016_A")

    model = Base2018(10192, int(t_rnn_size), int(t_kernel_size), int(v_rnn_size), int(v_kernel_size),
                     int(cvs), dropout=dropout, rnn='gru')
    model = model.to(device)
    optimizer = Adam(model.parameters(), lr=lr)

    training_dataloader = DataLoader(training_dataset, batch_size=int(train_batch_size), shuffle=True,
                                     num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)
    validation_dataloader = DataLoader(validation_dataset_A, batch_size=int(train_batch_size), shuffle=False,
                                       num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)

    val = fit(training_dataloader, validation_dataloader, model, loss, val_loss, optimizer, earlystopping, n_epochs, cuda_on,
        log_interval, None, None, device, start_epoch=start_epoch)
    print("Time:", time.time() - init_time)
    return val



if __name__ == '__main__':
    pbounds = {'lr': (0.0, 0.0005),
               'train_batch_size': (32, 128),
               't_rnn_size': (64, 512),
               't_kernel_size': (256, 512),
               'v_rnn_size': (64, 512),
               'v_kernel_size': (256, 1024),
               'cvs': (128, 4096),
               'dropout': (0.2, 0.4)}

    optimizer = BayesianOptimization(
        f=train_model,
        pbounds=pbounds,
        verbose=2,
        random_state=42
    )

    optimizer.maximize(init_points=10, n_iter=100)

    print(optimizer.max)
