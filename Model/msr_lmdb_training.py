from datasets import MSRVTTLMDB
from losses import TripletLoss
from networks import BaseModel
from torch.utils.data import DataLoader
from trainer import fit
from torch.optim.rmsprop import RMSprop
from torch.optim import lr_scheduler
from torch.utils.data.sampler import SubsetRandomSampler

import numpy as np

import torch

if __name__ == '__main__':
    margin = 1.
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = BaseModel(7217, 32, 300, 1024, 300, 2048, 2048, 2048, device)
    model = model.to(device)
    train_loss = TripletLoss(margin=0.2)
    train_loss = train_loss.to(device)
    lr = 0.0001
    optimizer = RMSprop(model.parameters(), lr=lr)
    scheduler = lr_scheduler.StepLR(optimizer, 1, gamma=0.5, last_epoch=-1)
    n_epochs = 50
    log_interval = 100
    save = False
    cuda_on = True
    validation_split = .1
    shuffle_dataset = True
    ramdom_seed = 42

    training_dataset = MSRVTTLMDB("/run/media/nbravo/Elements/lmdb/msr_padded/", True)
    dataset_size = len(training_dataset)
    indices = list(range(dataset_size))
    indices.remove(18277)  # corrupted example
    split = int(np.floor(validation_split * dataset_size))
    train_batch_size = 30
    if shuffle_dataset:
        np.random.seed(ramdom_seed)
        np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]
    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)

    training_dataloader = DataLoader(training_dataset, batch_size=train_batch_size, sampler=train_sampler,
                                     num_workers=0)
    validation_dataloader = DataLoader(training_dataset, batch_size=train_batch_size, sampler=val_sampler,
                                       num_workers=0)

    fit(training_dataloader, validation_dataloader, model, train_loss, optimizer, scheduler, n_epochs, cuda_on,
        log_interval, save, validation_split, start_epoch=-1)
