#!/usr/bin/env python
from Model.datasets import BaseVTT18MatchRankDataset
from Model.networks import Base2018, Base2018FineTune
from Model.utils.utils import load_checkpoint, load_best_model
from torch import cosine_similarity
import torch
import os
import numpy as np

def check_dir(dir_path):
    try:
        os.listdir(dir_path)
    except FileNotFoundError:
        os.makedirs(dir_path)


def match_and_rank(video_tensor, sentence_ids, sentence_tensors):
    scores = -1*video_tensor.unsqueeze(0).mm(sentence_tensors.t()).cpu().numpy()
    inds = np.argsort(scores)[0]
    return sentence_ids[inds].tolist()


if __name__ == '__main__':
    sets = ["A", "B", "C", "D", "E"]
    save_path = "/home/nbravo/vttm-dt/Model/matching/dong_2018_all_mir_ori/run3/"
    check_dir(save_path)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Loading model...")
    model = Base2018(10192, 1024, 512, 1024, 512, 2048, dropout=0.2, rnn='gru')
    checkpoint = load_best_model("/run/media/nbravo/Elements/states/dong_2018_all_mir_ori/run3/")
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    model = model.to(device)
    print("Loading dataset...")
    dataset = BaseVTT18MatchRankDataset(sets, device, video_init_vector=checkpoint['video_init_vector'],
                                        video_end_vector=checkpoint['video_end_vector'])

    with torch.no_grad():
        print("Computing video vectors...")
        y_videos = []
        for j in range(0, len(dataset.videos_index), 128):
            y_video = model.videoBranch(torch.stack(dataset.resnext_mean[j: j + 128]).cuda(),
                                        torch.stack(dataset.resnet152_vectors[j: j + 128]).cuda(),
                                        torch.stack(dataset.video_masks[j: j + 128]).float().cuda(),
                                        torch.Tensor(dataset.n_shots[j: j + 128]).cuda())
            for v in y_video:
                y_videos.append(v)
        y_videos = torch.stack(y_videos).cpu()
        for k in range(len(sets)):
            s = sets[k]
            sentence_ids = np.array( dataset.sent_ids[s])
            print("Set", s)
            matching = []
            print("Computing sentence vectors...")
            y_sentences = model.textBranch(*dataset.collate_sentences(dataset.bow_mean[s], dataset.rnn[s], dataset.lens[s], device))
            y_sentences = y_sentences.cpu()
            for i in range(len(dataset.videos_index)):
                if (i + 1) % 500 == 0:
                    print(str(s) + ":", "Video", i + 1, "of", len(dataset.videos_index))
                ranking = match_and_rank(y_videos[i], sentence_ids, y_sentences)
                matching.append((dataset.videos_index[i], ranking))
            print(str(s) + ":", "Video", i + 1, "of", len(dataset.videos_index))
            dataset.save_matching(s, matching, save_path)
