import os
import pickle
import random
import csv
import pandas as pd

MSR_VIDEOS = '/run/media/nbravo/Elements/MSR_all/resnext101/embeddings_original/'
MSVD_INFO = '/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv'
MSVD_VIDEOS = '/run/media/nbravo/nbravo-2TB/MSVD/videos/'
TGIF_GIFS = '/run/media/nbravo/nbravo-2TB/TGIF/gifs/'


def sample_msvd(n_by_set):
    df = pd.read_csv(MSVD_INFO)
    videos = map(lambda x: x.rsplit(".", 1)[0], os.listdir(MSVD_VIDEOS))
    df['videos_ids'] = df['VideoID'] + '_' + df['Start'].astype(str) + '_' + df['End'].astype(str)
    df = df[df['videos_ids'].isin(videos)]
    msvd = df['videos_ids'].tolist()
    return random.sample(msvd, n_by_set)


def create_dataset(sample_name, n=1915):
    n_by_set = n // 3
    msr = os.listdir(MSR_VIDEOS)
    tgif = os.listdir(TGIF_GIFS)

    msr_sample = random.sample(msr, n_by_set+1)
    msvd_sample = sample_msvd(n_by_set)
    tgif_sample = random.sample(tgif, n_by_set)

    subset = {'msr': msr_sample, 'msvd': msvd_sample, 'tgif': tgif_sample}
    pickle.dump(subset, open(sample_name + '.pkl', 'wb'))
    return subset


def create_csv_tgif(sample_file, video_csv_file, sentence_csv_file):
    sample_videos = pickle.load(open(sample_file, 'rb'))
    resnext_path = '/run/media/nbravo/Elements/TGIF/resnext101/embeddings_original/'
    dt_path = '/home/nbravo/data/TGIF/dt_hist/'
    with open(video_csv_file, 'w') as out_f:
       writer = csv.writer(out_f, delimiter=',', quotechar='"')
       writer.writerow(("video_index", "resnext101_path", "n_shots", "dt_path"))
       for v in sample_videos['tgif']:
           video_id = v.split(".")[0]
           writer.writerow(('tgif_' + video_id, resnext_path + video_id + ".pkl", len(pickle.load(open(resnext_path + video_id + ".pkl", 'rb'))), dt_path + video_id + '.pkl'))


    bow_path = '/run/media/nbravo/Elements/TGIF/sentences_new_gru/bow/'
    gru_path = '/run/media/nbravo/Elements/TGIF/sentences_new_gru/rnn/'
    infersent_path = '/run/media/nbravo/Elements/TGIF/sentences/infersent/'
    len_path = '/run/media/nbravo/Elements/TGIF/sentences_new_gru/len/'

    with open(sentence_csv_file, "w") as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("sentence_id", "bow_path", "gru_path", "len", "infersent"))
        for v in sample_videos['tgif']:
            video_id = v.split(".")[0]
            filename = "sentence" + video_id + ".pkl"
            writer.writerow(('tgif_' + video_id,
                             bow_path + filename,
                             gru_path + filename,
                             pickle.load(open(len_path + filename, "rb")),
                             infersent_path + filename))


def select_random_msr_sentence(sentence_list, video_id):
    filtered = list(filter(lambda x: x.split('_')[1].split('.')[0] == video_id, sentence_list))
    return random.sample(filtered, 1)[0].split('_')[0]


def create_csv_msr(sample_file, video_csv_file, sentence_csv_file):
    sample_videos = pickle.load(open(sample_file, 'rb'))
    resnext_path = '/run/media/nbravo/Elements/MSR_all/resnext101/embeddings_original/'
    dt_path = '/home/nbravo/data/MSR-VTT/dt_hist/'
    with open(video_csv_file, 'w') as out_f:
       writer = csv.writer(out_f, delimiter=',', quotechar='"')
       writer.writerow(("video_index", "resnext101_path", "n_shots", "dt_path"))
       for v in sample_videos['msr']:
           video_id = v.split(".")[0]
           writer.writerow(('msr_' + video_id, resnext_path + video_id + ".pkl", len(pickle.load(open(resnext_path + video_id + ".pkl", 'rb'))), dt_path + video_id + '.pkl'))


    bow_path = '/run/media/nbravo/Elements/MSR_all/sentences_new_gru/bow/'
    gru_path = '/run/media/nbravo/Elements/MSR_all/sentences_new_gru/rnn/'
    infersent_path = '/run/media/nbravo/Elements/MSR_all/sentences_new_gru/infersent/'
    len_path = '/run/media/nbravo/Elements/MSR_all/sentences_new_gru/len/'

    sentences_list = os.listdir(bow_path)
    with open(sentence_csv_file, "w") as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("sentence_id", "bow_path", "gru_path", "len", "infersent"))
        for v in sample_videos['msr']:
            video_id = v.split(".")[0]
            selected_sentence_id = select_random_msr_sentence(sentences_list, video_id)
            filename = selected_sentence_id + '_' + video_id + ".pkl"
            writer.writerow(('msr_' + video_id,
                             bow_path + filename,
                             gru_path + filename,
                             pickle.load(open(len_path + filename, "rb")),
                             infersent_path + filename))


def select_random_msvd_sentence(sentence_list, video_id):
    try:
        video_id_clean = video_id.rsplit('_', 2)[0]
        filtered = list(filter(lambda x: x.split('_', 1)[1].split('.')[0] == video_id_clean, sentence_list))
        return random.sample(filtered, 1)[0]
    except ValueError:
        print(video_id)
        exit(1)

def create_csv_msvd(sample_file, video_csv_file, sentence_csv_file):
    sample_videos = pickle.load(open(sample_file, 'rb'))
    resnext_path = '/run/media/nbravo/nbravo-2TB/MSVD/resnext101/embeddings_original/'
    dt_path = '/home/nbravo/data/MSVD/dt_hist/'
    with open(video_csv_file, 'w') as out_f:
       writer = csv.writer(out_f, delimiter=',', quotechar='"')
       writer.writerow(("video_index", "resnext101_path", "n_shots", "dt_path"))
       for v in sample_videos['msvd']:
           video_id = v.split(".")[0]
           writer.writerow(('msvd_' + video_id, resnext_path + video_id + ".pkl", len(pickle.load(open(resnext_path + video_id + ".pkl", 'rb'))), dt_path + video_id + '.pkl'))


    bow_path = '/run/media/nbravo/nbravo-2TB/MSVD/sentences_new_gru/bow/'
    gru_path = '/run/media/nbravo/nbravo-2TB/MSVD/sentences_new_gru/rnn/'
    infersent_path = '/run/media/nbravo/nbravo-2TB/MSVD/sentences/infersent/'
    len_path = '/run/media/nbravo/nbravo-2TB/MSVD/sentences_new_gru/len/'

    sentences_list = os.listdir(bow_path)
    with open(sentence_csv_file, "w") as out_f:
        writer = csv.writer(out_f, delimiter=',', quotechar='"')
        writer.writerow(("sentence_id", "bow_path", "gru_path", "len", "infersent"))
        for v in sample_videos['msvd']:
            video_id = v.split(".")[0]
            filename = select_random_msvd_sentence(sentences_list, video_id)
            writer.writerow(('msvd_' + video_id,
                             bow_path + filename,
                             gru_path + filename,
                             pickle.load(open(len_path + filename, "rb")),
                             infersent_path + filename))


def fuse_datasets(sample_folder):
    msr_videos = pd.read_csv(sample_folder + 'msr_videos.csv')
    msr_sentences = pd.read_csv(sample_folder + 'msr_sentences.csv')
    tgif_videos = pd.read_csv(sample_folder + 'tgif_videos.csv')
    tgif_sentences = pd.read_csv(sample_folder + 'tgif_sentences.csv')
    msvd_videos = pd.read_csv(sample_folder + 'msvd_videos.csv')
    msvd_sentences = pd.read_csv(sample_folder + 'msvd_sentences.csv')

    videos = pd.concat((msr_videos, tgif_videos, msvd_videos))
    sentences = pd.concat((msr_sentences, tgif_sentences, msvd_sentences))

    videos.to_csv(sample_folder + 'videos.csv', index=False)
    sentences.to_csv(sample_folder + 'sentences.csv', index=False)


if __name__ == '__main__':
    create_dataset('training_samples/sample_3/sample_3')
    create_csv_tgif('training_samples/sample_3/sample_3.pkl', 'training_samples/sample_3/tgif_videos.csv', 'training_samples/sample_3/tgif_sentences.csv')
    create_csv_msr('training_samples/sample_3/sample_3.pkl', 'training_samples/sample_3/msr_videos.csv', 'training_samples/sample_3/msr_sentences.csv')
    create_csv_msvd('training_samples/sample_3/sample_3.pkl', 'training_samples/sample_3/msvd_videos.csv', 'training_samples/sample_3/msvd_sentences.csv')
    fuse_datasets('training_samples/sample_3/')