import torch
import torch.cuda
from torch.utils.data.dataset import Dataset
import pandas as pd
import numpy as np
import Model.utils.utils as utils
import os.path as osp
import lmdb
import pyarrow as pa
import pickle
import os
import json


class VTTDataset(Dataset):
    def __init__(self, csv_path, train):
        super(VTTDataset, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.train = train
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        #self.yolo_paths = np.asarray(self.data_info.iloc[:, 1])
        self.dts_paths = np.asarray(self.data_info.iloc[:, 2])
        self.text_paths = np.asarray(self.data_info.iloc[:, 3])
        self.minibatch_size = 0.1
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if not self.train:
            self.all_videos = [self.__getvideo(index) for index in self.indices]

    def __getitem__(self, index):
        text_tensor = self.__gettext(index)
        if self.train:
            video_tensor = self.__getvideo(index)
            return text_tensor, video_tensor
        else:

            return index, text_tensor

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        #yolo_histogram = utils.read_obj(self.yolo_paths[index])
        dts_histogram  = utils.read_dt(self.dts_paths[index])
        video_representation = dts_histogram
        video_tensor = torch.Tensor(video_representation)
        return video_tensor

    def __gettext(self, index):
        text_representation = utils.read_text(self.text_paths[index])
        text_tensor = torch.Tensor(text_representation)
        return text_tensor

    def update_negatives_minibatch(self):
        self.hardest_negative_minibatch = [self.__gettext(index)
                                for index in np.random.choice(self.indices, int(self.data_len * self.minibatch_size))]


class VTTDatasetTestInterface(Dataset):
    def __init__(self, csv_path):
        super(VTTDatasetTestInterface, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.video_index = np.asarray(self.data_info.iloc[:, 0])
        #self.yolo_paths = np.asarray(self.data_info.iloc[:, 1])
        self.dts_paths  = np.asarray(self.data_info.iloc[:, 2])
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.all_videos = [self.__getvideo(index).unsqueeze(0) for index in self.indices]

    def __getitem__(self, index):
        return self.all_videos[index]

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        #yolo_histogram = utils.read_obj(self.yolo_paths[index])
        dts_histogram  = utils.read_dt(self.dts_paths[index])
        video_representation = dts_histogram
        video_tensor = torch.Tensor(video_representation).to(self.device)
        return video_tensor


class BaseVTT17MatchRankDataset(Dataset):
    def __init__(self, subset, subsubsets, save_path, device):
        super(BaseVTT17MatchRankDataset, self).__init__()
        self.device = device
        self.max_resnet152_len = 69
        self.videos_csv = pd.read_csv("Model/data/vtt2017/videos{}.csv".format(subset))
        self.videos_index = self.videos_csv.video_index
        self.resnet152_vectors, self.n_shots = self.load_videos(self.videos_csv)
        self.save_path = save_path

        self.sentences = []
        for s in subsubsets:
            self.sentences.append(self.load_sentences(pd.read_csv("Model/data/vtt2017/base2_flickr30m/sentences_{}_{}.csv".format(subset, s.upper()))))

    def load_videos(self, videos):
        resnet152_vectors = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_resnet152_len:
                resnet152_narray = resnet152_narray[:self.max_resnet152_len]
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(self.max_resnet152_len)
            else:
                diff_max_len = self.max_resnet152_len - n_shots
                resnet152_tensor = torch.from_numpy(resnet152_narray)
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.double)),
                                             0).float().to(self.device)
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(n_shots)
        return resnet152_vectors, n_shots_list

    def load_sentences(self, sentences):
        bow_vectors = []
        w2v_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_vectors.append(torch.Tensor(bow_representation).to(self.device))
            w2v_representation = utils.read_text(sentences.iloc[i, 2])
            w2v_vectors.append(torch.Tensor(w2v_representation).to(self.device))
            gru_representation = utils.read_text(sentences.iloc[i, 3])
            gru_vectors.append(torch.LongTensor(gru_representation).to(self.device))
            lens_vectors.append(torch.Tensor([sentences.iloc[i, 4]]).to(self.device))
        return bow_vectors, w2v_vectors, gru_vectors, lens_vectors

    def save_matching(self, video_id, subset, subsubset, matching_sent_list):
        save_path = self.save_path.format(subset, subsubset.upper())
        with open(save_path, "a") as fin:
            fin.write(str(self.videos_index[video_id]) + ", " + ", ".join(repr(sent_id) for sent_id in matching_sent_list) + '\n')


class BaseVTT17MatchRankDatasetInfersent(Dataset):
    def __init__(self, subset, subsubsets, device):
        super(BaseVTT17MatchRankDatasetInfersent, self).__init__()
        self.device = device
        self.max_resnet152_len = 69
        self.videos_csv = pd.read_csv("Model/data/vtt2017/videos{}.csv".format(subset))
        self.videos_index = self.videos_csv.video_index
        self.resnet152_vectors, self.n_shots = self.load_videos(self.videos_csv)

        self.sentences = []
        for s in subsubsets:
            self.sentences.append(self.load_sentences(pd.read_csv("Model/data/vtt2017/base2018_infersent/sentences_{}_{}.csv".format(subset, s.upper()))))

    def load_videos(self, videos):
        resnet152_vectors = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_resnet152_len:
                resnet152_narray = resnet152_narray[:self.max_resnet152_len]
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(self.max_resnet152_len)
            else:
                diff_max_len = self.max_resnet152_len - n_shots
                resnet152_tensor = torch.from_numpy(resnet152_narray)
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.double)),
                                             0).float().to(self.device)
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(n_shots)
        return resnet152_vectors, n_shots_list

    def load_sentences(self, sentences):
        bow_vectors = []
        w2v_vectors = []
        infersent_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_vectors.append(torch.Tensor(bow_representation).to(self.device))
            w2v_representation = utils.read_text(sentences.iloc[i, 2])
            w2v_vectors.append(torch.Tensor(w2v_representation).to(self.device))
            infersent_representation = utils.read_text(sentences.iloc[i, 3])
            infersent_vectors.append(torch.Tensor(infersent_representation).to(self.device))
            gru_representation = utils.read_text(sentences.iloc[i, 4])
            gru_vectors.append(torch.LongTensor(gru_representation).to(self.device))
            lens_vectors.append(torch.Tensor([sentences.iloc[i, 5]]).to(self.device))
        return bow_vectors, w2v_vectors, infersent_vectors, gru_vectors, lens_vectors

    def save_matching(self, video_id, subset, subsubset, matching_sent_list, match_path):
        save_path = (match_path + "{}.match").format(subset, subsubset.upper())
        with open(save_path, "a") as fin:
            fin.write(str(self.videos_index[video_id]) + ", " + ", ".join(repr(sent_id) for sent_id in matching_sent_list) + '\n')


class BaseVTT18MatchRankDataset(Dataset):
    def __init__(self, sets, device, resnext_model="mediamill", video_init_vector=None, video_end_vector=None):
        super(BaseVTT18MatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("Model/data/vtt2018/base_2018/videos.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/base_2018/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = utils.read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)


class BaseVTT18DTMatchRankDataset(Dataset):
    def __init__(self, sets, device, msr_voc=False, video_init_vector=None, video_end_vector=None):
        super(BaseVTT18DTMatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("Model/data/vtt2018/dt/videos.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots, self.dt_x, self.dt_y = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/dt/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        dt_x_list = []
        dt_y_list = []
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            dt_x, dt_y = pickle.load(open(videos.iloc[i, 3], 'rb'))
            dt_x_list.append(torch.from_numpy(dt_x).float())
            dt_y_list.append(torch.from_numpy(dt_y).float())
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                    [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list, dt_x_list, dt_y_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = utils.read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)


class BaseVTT18InfersentMatchRankDataset(Dataset):
    def __init__(self, sets, device, video_init_vector=None, video_end_vector=None):
        super(BaseVTT18InfersentMatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("Model/data/vtt2018/infersent/videos.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        self.infersent = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s], self.infersent[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/infersent/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []
        infersent_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = utils.read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
            infersent_vectors.append(utils.read_text(sentences.iloc[i, 4]))
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors, infersent_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, infersent, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return (torch.Tensor(infersent).to(device),)


class BaseVTT16MatchRankDatasetInfersent(Dataset):
    def __init__(self, sets, device):
        super(BaseVTT16MatchRankDatasetInfersent, self).__init__()
        self.device = device
        self.max_resnet152_len = 60
        self.videos_id = {}
        self.resnet152_vectors = {}
        self.n_shots = {}

        for s in sets:
            self.videos_id[s], self.resnet152_vectors[s], self.n_shots[s] = self.load_videos(pd.read_csv("Model/data/VTT2016/vtt16_{}_dataset.csv".format(s.upper())))

        self.sent_ids = {}
        self.bow = {}
        self.w2v = {}
        self.rnn = {}
        self.lens = {}
        for s in sets:
            self.sent_ids[s], self.bow[s], self.w2v[s], self.rnn[s], self.lens[s] = self.load_sentences(
                pd.read_csv("Model/data/VTT2016/vtt16_{}_dataset.csv".format(s.upper())))

    def load_videos(self, videos):
        video_ids = []
        resnet152_vectors = []
        n_shots_list = []
        for i in range(len(videos)):
            video_ids.append(videos.iloc[i, 0])
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 2], 'rb')))
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_resnet152_len:
                resnet152_narray = resnet152_narray[:self.max_resnet152_len]
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(self.max_resnet152_len)
            else:
                diff_max_len = self.max_resnet152_len - n_shots
                resnet152_tensor = torch.from_numpy(resnet152_narray)
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                resnet152_vectors.append(resnet152_tensor)
                n_shots_list.append(n_shots)
        return video_ids, resnet152_vectors, n_shots_list

    def load_sentences(self, sentences):
        sentence_ids = []
        bow_vectors = []
        w2v_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_ids.append(sentences.iloc[i, 1])
            bow_representation = utils.read_text(sentences.iloc[i, 3])
            bow_vectors.append(bow_representation)
            w2v_representation = utils.read_text(sentences.iloc[i, 4])
            w2v_vectors.append(w2v_representation)
            gru_representation = utils.read_text(sentences.iloc[i, 5])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 6]])
        return sentence_ids, bow_vectors, w2v_vectors, gru_vectors, lens_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(
                    repr(sent_id) for sent_id in ranking) + '\n')

class VTT19MatchRankDataset(Dataset):
    def __init__(self, subset, device):
        super(VTT19MatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("data/vtt19/videos.csv")
        self.videos_index = self.videos_csv.video_index
        self.resnet152_vectors, self.resnext101_vectors, self.dts_vectors = self.load_videos(self.videos_csv)

        self.sentences = self.load_sentences(pd.read_csv("data/vtt19/sentences_{}.csv".format(subset)))

    def load_videos(self, videos):
        resnet152_vectors = []
        resnext101_vectors = []
        dts_vectors = []
        for i in range(len(videos)):
            resnet152_vectors.append(utils.read_torch(videos.iloc[i, 1]).to(self.device))
            resnext101_vectors.append(utils.read_torch(videos.iloc[i, 2]).to(self.device))
            video_representation = utils.read_dt(videos.iloc[i, 3])
            dts_vectors.append(torch.Tensor(video_representation).to(self.device))
        return resnet152_vectors, resnext101_vectors, dts_vectors

    def load_sentences(self, sentences):
        bow_vectors = []
        w2v_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            bow_representation = utils.read_text(sentences.iloc[i, 1])
            bow_vectors.append(torch.Tensor(bow_representation).to(self.device))
            w2v_representation = utils.read_text(sentences.iloc[i, 2])
            w2v_vectors.append(torch.Tensor(w2v_representation).to(self.device))
            gru_representation = utils.read_text(sentences.iloc[i, 3])
            gru_vectors.append(torch.Tensor(gru_representation).to(self.device))
            lens_vectors.append(torch.Tensor([sentences.iloc[i, 4]]).to(self.device))
        return bow_vectors, w2v_vectors, gru_vectors, lens_vectors

    def save_matching(self, video_id, subset, matching_sent_list):
        save_path = "data/vtt19/matching/{}.match".format(subset)
        with open(save_path, "a") as fin:
            fin.write(str(self.videos_index[video_id]) + ", " + ", ".join(repr(sent_id) for sent_id in matching_sent_list) + '\n')


class MSRVTT(Dataset):
    def __init__(self, csv_path, train):
        super(MSRVTT, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.train = train
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.resnet152_paths = np.array(self.data_info.iloc[:, 1])
        self.resnext101_paths = np.array(self.data_info.iloc[:, 2])
        self.dts_paths = np.array(self.data_info.iloc[:, 3])
        self.bow_paths = np.array(self.data_info.iloc[:, 4])
        self.w2v_paths = np.array(self.data_info.iloc[:, 5])
        self.gru_paths = np.array(self.data_info.iloc[:, 6])
        self.lenghts = np.array(self.data_info.iloc[:, 7])
        self.minibatch_size = 0.1
        self.device = "cpu"
        if not self.train:
            self.all_videos = [self.__getvideo(index) for index in self.indices]

    def __getitem__(self, index):
        bow_tensor, w2v_tensor, gru_tensor, lens_tensor = self.__gettext(index)
        if self.train:
            resnet152_tensor, resnext101_tensor, dts_tensor = self.__getvideo(index)
            return index, bow_tensor, w2v_tensor, gru_tensor, lens_tensor, resnet152_tensor, resnext101_tensor, dts_tensor
        else:
            return index, bow_tensor, w2v_tensor, gru_tensor, lens_tensor

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        resnet152_tensor = utils.read_torch(self.resnet152_paths[index])
        resnext101_tensor = utils.read_torch(self.resnext101_paths[index])
        dts_histogram = utils.read_dt(self.dts_paths[index])
        dts_tensor = torch.Tensor(dts_histogram)
        assert len(resnet152_tensor.size()) == 1 and resnet152_tensor.size(0) == 2048, "Bad resnet152 in " + str(index)
        assert len(resnext101_tensor.size()) == 1 and resnext101_tensor.size(0) == 2048, "Bad resnext101 in " + str(index)
        assert len(dts_histogram) == 800, "Bad DT in " + str(index)
        return resnet152_tensor.tolist(), resnext101_tensor.tolist(), dts_tensor.tolist()

    def __gettext(self, index):
        bow_representation = utils.read_text(self.bow_paths[index])
        bow_tensor = torch.Tensor(bow_representation)
        w2v_representation = utils.read_text(self.w2v_paths[index])
        w2v_tensor = torch.Tensor(w2v_representation)
        gru_representation = utils.read_text(self.gru_paths[index])
        gru_tensor = torch.Tensor(gru_representation)
        lens_tensor = [self.lenghts[index]]
        assert len(bow_tensor.size()) == 1 and bow_tensor.size(0) == 7217, "Bad BoW in " + str(index)
        assert len(w2v_tensor.size()) == 1 and w2v_tensor.size(0) == 300, "Bad W2V in " + str(index)
        assert len(gru_tensor.size()) == 1 and gru_tensor.size(0) == 32, "Bad GRU in " + str(index)
        return bow_tensor.tolist(), w2v_tensor.tolist(), gru_tensor.tolist(), lens_tensor

    def update_negatives_minibatch(self):
        self.hardest_negative_minibatch = [self.__gettext(index)
                                for index in np.random.choice(self.indices, int(self.data_len * self.minibatch_size))]


class MSRVTTLMDB(Dataset):

    def __init__(self, lmdb_path, train):
        super(MSRVTTLMDB, self).__init__()
        self.db_path = lmdb_path
        self.train = train
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False)
        with self.env.begin(write=False) as txn:
            self.length = pa.deserialize(txn.get(b'__len__'))
            self.keys = pa.deserialize(txn.get(b'__keys__'))

    def __getitem__(self, index):
        env = self.env
        with env.begin(write=False) as txn:
            byteflow = txn.get(self.keys[index])
        vectors = pa.deserialize(byteflow)
        tensors = [vectors[0]]
        for v in vectors[1:]:
            tensors.append(torch.Tensor(v))
        return tensors

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'


class MSRVTTBase(Dataset):
    def __init__(self, csv_path, for_lmdb=True):
        super(MSRVTTBase, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.video_ids = np.array(self.data_info.iloc[:, 0])
        self.resnet152_paths = np.array(self.data_info.iloc[:, 1])
        self.bow_paths = np.array(self.data_info.iloc[:, 2])
        self.gru_paths = np.array(self.data_info.iloc[:, 3])
        self.lenghts = np.array(self.data_info.iloc[:, 4])
        # self.infersent_paths = np.array(self.data_info.iloc[:, 5])
        self.for_lmdb = for_lmdb
        self.video_max_shots = 64

    def __getitem__(self, index):
        bow_tensor, gru_tensor, lens = self.__gettext(index)
        video_id, resnet152_vectors, n_shots = self.__getvideo(index)
        return index, video_id, bow_tensor, gru_tensor, lens, resnet152_vectors, n_shots

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        video_id = str(self.video_ids[index])
        resnet152_narray = np.array(pickle.load(open(self.resnet152_paths[index], 'rb')))
        n_shots = len(resnet152_narray)
        if self.for_lmdb:
            return video_id, resnet152_narray, n_shots
        return video_id, resnet152_narray, n_shots

    def __gettext(self, index):
        bow_representation = utils.read_text(self.bow_paths[index])
        # infersent_representation = utils.read_text(self.infersent_paths[index])
        gru_representation = utils.read_text(self.gru_paths[index])
        lens = self.lenghts[index]

        # if infersent_representation is None and self.for_lmdb:
        #     infersent_representation = np.zeros(4096)

        assert len(bow_representation) == 10192, "Bad BoW in " + str(index)

        return bow_representation, gru_representation, lens

    def collate_data(self, data):
        text, bow, seq_indices, lens, infersent, resnext, n_shots = zip(*data)

        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow = np.array(bow)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))

        n_shots = np.array(n_shots)
        max_n_shots = max(n_shots)
        if max_n_shots > self.video_max_shots:
            max_n_shots = self.video_max_shots
        resnext = np.array(resnext)
        rnn_resnext = np.zeros((len(resnext), max_n_shots, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots))
        for i in range(0, len(resnext)):
            rnn_resnext[i] = np.concatenate((resnext[i], np.zeros((max_n_shots - n_shots[i], 2048))))
            video_masks[i, :n_shots[i]] = 1.0

        return text, torch.Tensor(bow), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
               torch.Tensor(infersent), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots)


class BaseDTInfersent(Dataset):
    def __init__(self, csv_path, for_lmdb=True, tgif=False):
        super(BaseDTInfersent, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.video_ids = np.array(self.data_info.iloc[:, 0])
        self.resnet152_paths = np.array(self.data_info.iloc[:, 1])
        self.dt_paths = np.array(self.data_info.iloc[:, 2])
        self.bow_paths = np.array(self.data_info.iloc[:, 3])
        self.gru_paths = np.array(self.data_info.iloc[:, 4])
        self.lenghts = np.array(self.data_info.iloc[:, 5])
        self.infersent_paths = np.array(self.data_info.iloc[:, 6])
        self.for_lmdb = for_lmdb
        self.video_max_shots = 64
        self.tgif = tgif

    def __getitem__(self, index):
        bow_tensor, gru_tensor, lens, infersent_tensor = self.__gettext(index)
        video_id, resnet152_vectors, n_shots, dt_x, dt_y = self.__getvideo(index)
        return index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor, resnet152_vectors, n_shots, dt_x, dt_y

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        video_id = str(self.video_ids[index])
        resnet152_narray = np.array(pickle.load(open(self.resnet152_paths[index], 'rb')))
        n_shots = len(resnet152_narray)
        dt = pickle.load(open(self.dt_paths[index], 'rb'))
        dt_x = np.array(dt[0])
        dt_y = np.array(dt[1])
        if self.for_lmdb:
            return video_id, resnet152_narray, n_shots, dt_x, dt_y
        return video_id, resnet152_narray, n_shots, dt_x, dt_y

    def __gettext(self, index):
        bow_representation = utils.read_text(self.bow_paths[index])
        infersent_representation = utils.read_text(self.infersent_paths[index])
        gru_representation = utils.read_text(self.gru_paths[index])
        lens = self.lenghts[index]

        if infersent_representation is None and self.for_lmdb:
            infersent_representation = np.zeros(4096)

        assert len(bow_representation) == 10192, "Bad BoW in " + str(index)

        return bow_representation, gru_representation, lens, infersent_representation

    def collate_data(self, data):
        text, bow, seq_indices, lens, infersent, resnext, n_shots, dts = zip(*data)

        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow = np.array(bow)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))

        n_shots = np.array(n_shots)
        dts = np.array(dts)
        max_n_shots = max(n_shots)
        if max_n_shots > self.video_max_shots:
            max_n_shots = self.video_max_shots
        resnext = np.array(resnext)
        rnn_resnext = np.zeros((len(resnext), max_n_shots, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots))
        for i in range(0, len(resnext)):
            rnn_resnext[i] = np.concatenate((resnext[i], np.zeros((max_n_shots - n_shots[i], 2048))))
            video_masks[i, :n_shots[i]] = 1.0

        return text, torch.Tensor(bow), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
               torch.Tensor(infersent), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots), torch.Tensor(dts)


class AllDataset(Dataset):
    def __init__(self, csv_path):
        super(AllDataset, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.video_ids = np.array(self.data_info.iloc[:, 0])
        self.resnext101_ori_paths = np.array(self.data_info.iloc[:, 1])
        self.resnext101_mxnet_paths = np.array(self.data_info.iloc[:, 2])
        self.dt_paths = np.array(self.data_info.iloc[:, 3])
        self.bow_paths = np.array(self.data_info.iloc[:, 4])
        self.gru_paths = np.array(self.data_info.iloc[:, 5])
        self.lenghts = np.array(self.data_info.iloc[:, 6])
        self.infersent_paths = np.array(self.data_info.iloc[:, 7])
        self.video_max_shots = 64

    def __getitem__(self, index):
        bow_tensor, gru_tensor, lens, infersent_tensor = self.__gettext(index)
        video_id, resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y = self.__getvideo(index)
        return index, video_id, bow_tensor, gru_tensor, lens, infersent_tensor,  \
               resnext101_ori_vectors, n_shots_ori, resnext101_mxnet_vectors, n_shots_mxnet, dt_x, dt_y

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        video_id = str(self.video_ids[index])
        resnext101_ori_array = np.array(pickle.load(open(self.resnext101_ori_paths[index], 'rb')))
        resnext101_mxnet_array = np.array(pickle.load(open(self.resnext101_mxnet_paths[index], 'rb')))
        n_shots_ori = len(resnext101_ori_array)
        n_shots_mxnet = len(resnext101_mxnet_array)
        dt = pickle.load(open(self.dt_paths[index], 'rb'))
        dt_x = np.array(dt[0])
        dt_y = np.array(dt[1])
        return video_id, resnext101_ori_array, n_shots_ori, resnext101_mxnet_array, n_shots_mxnet, dt_x, dt_y

    def __gettext(self, index):
        bow_representation = utils.read_text(self.bow_paths[index])
        infersent_representation = utils.read_text(self.infersent_paths[index])
        gru_representation = utils.read_text(self.gru_paths[index])
        lens = self.lenghts[index]

        if infersent_representation is None and self.for_lmdb:
            infersent_representation = np.zeros(4096)

        assert len(bow_representation) == 10192, "Bad BoW in " + str(index)

        return bow_representation, gru_representation, lens, infersent_representation

    def collate_data(self, data):
        text, bow, seq_indices, lens, infersent, resnext, n_shots, dts = zip(*data)

        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow = np.array(bow)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        infersent = np.array(infersent)
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))

        n_shots = np.array(n_shots)
        dts = np.array(dts)
        max_n_shots = max(n_shots)
        if max_n_shots > self.video_max_shots:
            max_n_shots = self.video_max_shots
        resnext = np.array(resnext)
        rnn_resnext = np.zeros((len(resnext), max_n_shots, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots))
        for i in range(0, len(resnext)):
            rnn_resnext[i] = np.concatenate((resnext[i], np.zeros((max_n_shots - n_shots[i], 2048))))
            video_masks[i, :n_shots[i]] = 1.0

        return text, torch.Tensor(bow), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
               torch.Tensor(infersent), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots), torch.Tensor(dts)


class VTTInfersent(Dataset):
    def __init__(self, csv_path, max_resnet152_len, for_lmdb=False):
        super(VTTInfersent, self).__init__()
        self.data_info = pd.read_csv(csv_path)
        self.data_len = len(self.data_info.index)
        self.indices = self.data_info.index.values
        self.max_resnet152_len = max_resnet152_len
        self.resnet152_paths = np.array(self.data_info.iloc[:, 1])
        self.infersent_paths = np.array(self.data_info.iloc[:, 2])
        self.for_lmdb = for_lmdb

    def __getitem__(self, index):
        infersent_tensor = self.__gettext(index)
        resnet152_vectors, n_shots = self.__getvideo(index)
        return index, infersent_tensor, resnet152_vectors, n_shots

    def __len__(self):
        return self.data_len

    def __getvideo(self, index):
        resnet152_narray = np.array(pickle.load(open(self.resnet152_paths[index], 'rb')))
        n_shots = len(resnet152_narray)
        if n_shots >= self.max_resnet152_len:
            resnet152_narray = resnet152_narray[:self.max_resnet152_len]
            if self.for_lmdb:
                return resnet152_narray, self.max_resnet152_len
            resnet152_tensor = torch.from_numpy(resnet152_narray).float()  # n_shots x 2048
            return resnet152_tensor, self.max_resnet152_len
        else:
            diff_max_len = self.max_resnet152_len - n_shots
            resnet152_tensor = torch.from_numpy(resnet152_narray)
            resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.double)), 0).float()
            if self.for_lmdb:
                return resnet152_tensor.numpy(), n_shots
            return resnet152_tensor, n_shots

    def __gettext(self, index):
        infersnet_representation = utils.read_text(self.infersent_paths[index])
        infersent_tensor = torch.Tensor(infersnet_representation)
        assert len(infersnet_representation) == 4096, "Invalid infersent tensor"
        if self.for_lmdb:
            return infersent_tensor.numpy()
        return infersent_tensor


class VTTBaseLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, resnext_model='medialmill', max_readers=1):
        super(VTTBaseLMDB, self).__init__()
        assert resnext_model in ('mediamill', 'mxnet')
        max_readers = max(1, max_readers)
        self.rexnext_index = 6 if resnext_model == 'mediamill' else 8
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)
        cursor.close()

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        return vectors[2],  vectors[3], int(vectors[4]), \
               vectors[self.rexnext_index], int(vectors[self.rexnext_index + 1])

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def collate_data(self, data):
        bow, seq_indices, lens, resnext, n_shots = zip(*data)

        max_len = max(lens)
        rnn_seq_indices = 2*np.ones((len(seq_indices), max_len))
        bow_mean = np.zeros((len(resnext), len(bow[0])))
        n_shots = np.array(n_shots)
        max_n_shots = min(max(n_shots), self.video_max_shots)
        resnext_mean = np.zeros((len(resnext), len(resnext[0][0])))
        rnn_resnext = np.zeros((len(resnext), max_n_shots + 2, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots + 2))
        for i in range(len(resnext)):
            bow_mean[i] = bow[i] / np.sum(bow[i])
            rnn_seq_indices[i, :lens[i]] = seq_indices[i]
            resnext_mean[i] = np.mean(resnext[i], 0)
            end = max_n_shots if n_shots[i] > self.video_max_shots else n_shots[i]
            rnn_resnext[i, :end + 2] = np.concatenate(([self.video_init_vector], resnext[i][:end], [self.video_end_vector]), axis=0)
            video_masks[i, :end + 2] = 1.0
            n_shots[i] = end + 2

        return torch.Tensor(bow_mean), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
           torch.Tensor(resnext_mean), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots)


class VTTBaseOldLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1):
        super(VTTBaseOldLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)
        cursor.close()

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        return vectors[2],  vectors[3], int(vectors[4]), vectors[5]

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()


class VTTOnlyDTLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1):
        super(VTTOnlyDTLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        cursor.close()
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        # dataset_name, bow, rnn, sent_len,
        # dt_x, dt_y
        return self.dataset_name, vectors[2],  vectors[3], int(vectors[4]), \
               vectors[10], vectors[11]

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def __dt_hist(self, name, dt):
        hist = np.zeros(2500)
        if max(dt) == 0:
            return hist
        for i in dt:
            hist[i] += 1
        return hist

    def collate_data(self, data):
        dataset_name, bow, seq_indices, lens, dt_x, dt_y = zip(*data)

        max_len = max(lens)
        rnn_seq_indices = 2*np.ones((len(seq_indices), max_len))
        bow_mean = np.zeros((len(bow), len(bow[0])))
        dt_x = np.array(dt_x)
        dt_y = np.array(dt_y)
        for i in range(len(bow)):
            bow_mean[i] = bow[i] / np.sum(bow[i])
            rnn_seq_indices[i, :lens[i]] = seq_indices[i]

        return torch.Tensor(bow_mean), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
               torch.Tensor(dt_x), torch.Tensor(dt_y)


class VTTInfersentLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1):
        super(VTTInfersentLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)
        cursor.close()

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        return self.dataset_name, vectors[2], vectors[3], int(vectors[4]), vectors[5], vectors[6], int(vectors[7])

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def __dt_hist(self, name, dt):
        hist = np.zeros(2500)
        if max(dt) == 0:
            return hist
        for i in dt:
            hist[i] += 1
        return hist

    def collate_data(self, data):
        dataset_name, bow, seq_indices, lens, infersent, resnext, n_shots = zip(*data)

        infersent = np.array(infersent)
        max_len = max(lens)
        rnn_seq_indices = 2*np.ones((len(seq_indices), max_len))
        bow_mean = np.zeros((len(resnext), len(bow[0])))
        n_shots = np.array(n_shots)
        max_n_shots = min(max(n_shots), self.video_max_shots)
        resnext_mean = np.zeros((len(resnext), len(resnext[0][0])))
        rnn_resnext = np.zeros((len(resnext), max_n_shots + 2, 2048))
        video_masks = np.zeros((len(resnext), max_n_shots + 2))
        for i in range(len(resnext)):
            bow_mean[i] = bow[i] / np.sum(bow[i])
            rnn_seq_indices[i, :lens[i]] = seq_indices[i]
            resnext_mean[i] = np.mean(resnext[i], 0)
            end = max_n_shots if n_shots[i] > self.video_max_shots else n_shots[i]
            rnn_resnext[i, :end + 2] = np.concatenate(([self.video_init_vector], resnext[i][:end], [self.video_end_vector]), axis=0)
            video_masks[i, :end + 2] = 1.0
            n_shots[i] = end + 2

        return torch.Tensor(bow_mean), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), torch.Tensor(infersent), \
           torch.Tensor(resnext_mean), torch.Tensor(rnn_resnext), torch.Tensor(video_masks), torch.Tensor(n_shots)



class DenseTrajectoriesHistogramDataset(Dataset):

    def __init__(self, dir_path):
        super(DenseTrajectoriesHistogramDataset, self).__init__()
        self.dir_path = dir_path
        self.length = len(os.listdir(self.dir_path))

    def __getitem__(self, video_id):
        filename = self.dir_path + "video" + str(video_id) + ".mkv.json"
        dts = []
        with open(filename, "rb") as fin:
            dts = pickle.load(fin)
        if len(dts) == 0:
            return torch.Tensor([])
        else:
            return torch.Tensor(list(dts))

    def __len__(self):
        return self.length
