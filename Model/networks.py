import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import pickle
import numpy as np
import torch.nn.functional as F
from torch.autograd import Variable


def l2norm(X):
    """L2-normalize columns of X
    """
    norm = torch.pow(X, 2).sum(dim=1, keepdim=True).sqrt()
    X = torch.div(X, norm)
    return X


def xavier_init_fc(fc):
    """Xavier initialization for the fully connected layer
    """
    r = np.sqrt(6.) / np.sqrt(fc.in_features +
                             fc.out_features)
    fc.weight.data.uniform_(-r, r)
    fc.bias.data.fill_(0)


class MFC(nn.Module):
    """
    Multi Fully Connected Layers
    """
    def __init__(self, fc_layers, dropout, have_dp=True, have_bn=False, have_last_bn=False):
        super(MFC, self).__init__()
        # fc layers
        self.n_fc = len(fc_layers)
        if self.n_fc > 1:
            if self.n_fc > 1:
                self.fc1 = nn.Linear(fc_layers[0], fc_layers[1])

            # dropout
            self.have_dp = have_dp
            if self.have_dp:
                self.dropout = nn.Dropout(p=dropout)

            # batch normalization
            self.have_bn = have_bn
            self.have_last_bn = have_last_bn
            if self.have_bn:
                if self.n_fc == 2 and self.have_last_bn:
                    self.bn_1 = nn.BatchNorm1d(fc_layers[1])

        self.init_weights()

    def init_weights(self):
        """Xavier initialization for the fully connected layer
        """
        if self.n_fc > 1:
            xavier_init_fc(self.fc1)

    def forward(self, inputs):

        if self.n_fc <= 1:
            features = inputs

        elif self.n_fc == 2:
            features = self.fc1(inputs)
            # batch noarmalization
            if self.have_bn and self.have_last_bn:
                features = self.bn_1(features)
            if self.have_dp:
                features = self.dropout(features)

        return features


class GRUBranch(nn.Module):

    def __init__(self, voc_size, embedding_dim, hidden_size, dropout):  # XXXX, 500, 500
        super(GRUBranch, self).__init__()
        self.rnn_output_size = 2*hidden_size
        self.embedding = nn.Embedding(voc_size + 4, embedding_dim, padding_idx=2)
        self.__init_embedding()
        self.gru = nn.GRU(embedding_dim, hidden_size, batch_first=True, bidirectional=True)
        self.dropout = nn.Dropout(dropout)

    def forward(self, seq_indices, lengths):
        embs = self.embedding(seq_indices)
        embs = pack_padded_sequence(embs, lengths, batch_first=True, enforce_sorted=False)
        gru_init_out, _ = self.gru(embs)
        padded, _ = pad_packed_sequence(gru_init_out, batch_first=True)
        gru_init_out = padded
        gru_out = Variable(torch.zeros(gru_init_out.size(0), self.rnn_output_size)).cuda()
        for i, batch in enumerate(padded):
            gru_out[i] = torch.mean(batch[:int(lengths[i]), :], 0)
        gru_out = self.dropout(gru_out)
        return gru_out, gru_init_out

    def __init_embedding(self):
        self.embedding.weight.data.copy_(
            torch.from_numpy(pickle.load(open("TextVectorization/objects/msr_all/rnn/vectors.pkl", 'rb'))))
        self.embedding.weight.requires_grad = False


class TextBranch(nn.Module):

    def __init__(self, voc_size, w2v_dim, gru_hidden_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn): # kernel num = 512, kernel sizes 2-3-4
        super(TextBranch, self).__init__()
        self.rnn_output_size = 2*gru_hidden_size
        self.dropout = nn.Dropout(dropout)
        self.embedding = nn.Embedding(voc_size + 4, w2v_dim, padding_idx=2)
        if rnn == 'gru':
            self.rnn = nn.GRU(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        self.text_mapping = MFC([voc_size + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout, have_bn=True, have_last_bn=True)

        self.init_weights()

    def init_weights(self):
        embeddings = pickle.load(open("TextVectorization/objects/msr_tgif_msvd/rnn/vectors.pkl", 'rb'))
        for i in range(4):
            embeddings[i] = np.random.uniform(-1, 1, 500)
        self.embedding.weight.data.copy_(torch.from_numpy(embeddings))

    def forward(self, bow_mean, gru_vector, lengths):
        # Level 1
        org_out = bow_mean

        # Level 2
        embs = self.embedding(gru_vector)
        embs = pack_padded_sequence(embs, lengths, batch_first=True, enforce_sorted=False)
        gru_init_out, _ = self.rnn(embs)
        padded = pad_packed_sequence(gru_init_out, batch_first=True)
        gru_init_out = padded[0]
        gru_out = torch.zeros(padded[0].size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(padded[0]):
            gru_out[i] = torch.mean(batch[:int(lengths[i]), :], 0)
        gru_out = self.dropout(gru_out)

        # Level 3
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)
        
        features = torch.cat((gru_out, con_out, org_out), 1)
        features = self.text_mapping(features)
        features = l2norm(features)
        return features


class TextBranchOnlyInfersent(nn.Module):

    def __init__(self, infersent_size, csv_size, dropout):
        super(TextBranchOnlyInfersent, self).__init__()
        self.text_mapping = MFC([infersent_size, csv_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, infersent_tensor):
        features = self.text_mapping(infersent_tensor)
        features = l2norm(features)
        return features


class TextBranchNoGru(nn.Module):

    def __init__(self, voc_size, infersent_size, kernel_num, kernel_sizes):  # kernel num = 512, kernel sizes 2-3-4
        super(TextBranchNoGru, self).__init__()

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, infersent_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        self.dropout = nn.Dropout(0.2)

        self.text_mapping = MFC([voc_size + infersent_size + kernel_num * len(kernel_sizes), 2048], 0.2,
                                have_bn=True, have_last_bn=True)

    def forward(self, bow_vector, infersent_vector, lengths):
        # Level 1
        org_out = bow_vector / bow_vector.sum(dim=1).view(bow_vector.size(0), -1)

        # Level 3
        con_out = infersent_vector.unsqueeze(1).unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((infersent_vector, con_out, org_out), 1)
        features = self.text_mapping(features)
        features = l2norm(features)
        return features


class TextBranchInfersentAll(nn.Module):

    def __init__(self, voc_size, w2v_dim, infersent_size, gru_hidden_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn):  # kernel num = 512, kernel sizes 2-3-4
        super(TextBranchInfersentAll, self).__init__()
        self.rnn_output_size = 2 * gru_hidden_size
        self.dropout = nn.Dropout(dropout)
        self.embedding = nn.Embedding(voc_size + 4, w2v_dim, padding_idx=2)
        if rnn == 'gru':
            self.rnn = nn.GRU(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(w2v_dim, gru_hidden_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        self.text_mapping = MFC([voc_size + infersent_size + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout,
                                have_bn=True, have_last_bn=True)

        self.init_weights()

    def init_weights(self):
        embeddings = pickle.load(open("TextVectorization/objects/msr_tgif_msvd/rnn/vectors.pkl", 'rb'))
        for i in range(4):
            embeddings[i] = np.random.uniform(-1, 1, 500)
        self.embedding.weight.data.copy_(torch.from_numpy(embeddings))

    def forward(self, bow_mean, gru_vector, lengths, infersent):
        # Level 1
        org_out = bow_mean

        # Level 2
        embs = self.embedding(gru_vector)
        embs = pack_padded_sequence(embs, lengths, batch_first=True, enforce_sorted=False)
        gru_init_out, _ = self.rnn(embs)
        padded = pad_packed_sequence(gru_init_out, batch_first=True)
        gru_init_out = padded[0]
        gru_out = torch.zeros(padded[0].size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(padded[0]):
            gru_out[i] = torch.mean(batch[:int(lengths[i]), :], 0)
        gru_out = self.dropout(gru_out)

        # Level 3
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((gru_out, con_out, org_out, infersent), 1)
        features = self.text_mapping(features)
        features = l2norm(features)
        return features


class VideoBranch(nn.Module):

    def __init__(self, embedding_dim, rnn_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn):
        super(VideoBranch, self).__init__()
        self.rnn_output_size = 2*rnn_size
        self.dropout = nn.Dropout(dropout)
        if rnn == 'gru':
            self.rnn = nn.GRU(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
            ])

        self.visual_mapping = MFC([embedding_dim + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, resnext_mean, resnext_tensor, video_mask, n_shots): # lista de lista de vectoires de 2048 (con padding al final)
        resnext_tensor = resnext_tensor[:, :int(max(n_shots).item()), :]
        video_mask = video_mask[:, :int(max(n_shots).item())]
        # Level 1
        org_out = resnext_mean
        # Level 2
        rnn_input = pack_padded_sequence(resnext_tensor, n_shots, batch_first=True, enforce_sorted=False)
        rnn_output, _ = self.rnn(rnn_input)
        padded = pad_packed_sequence(rnn_output, batch_first=True)
        gru_init_out = padded[0]
        mean_gru = torch.zeros(gru_init_out.size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(gru_init_out):
            mean_gru[i] = torch.mean(batch[:int(n_shots[i])], 0)
        gru_out = mean_gru
        gru_out = self.dropout(gru_out)

        # Level 3
        # video_mask = torch.zeros((resnext_tensor.size(0), 60)).cuda()
        # for i in range(n_shots.size(0)):
        #     video_mask[i, :int(n_shots[i].item())] = 1.0
        video_masks = video_mask.unsqueeze(2).expand(-1, -1, gru_init_out.size(2))
        gru_init_out = gru_init_out * video_masks
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((gru_out,con_out,org_out), 1)
        features = self.visual_mapping(features)
        features = l2norm(features)
        return features


class VideoBranchDT(nn.Module):

    def __init__(self, embedding_dim, dt_hist_size, rnn_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn):
        super(VideoBranchDT, self).__init__()
        self.dt_hist_size = dt_hist_size
        self.rnn_output_size = 2 * rnn_size
        self.dropout = nn.Dropout(dropout)
        if rnn == 'gru':
            self.rnn = nn.GRU(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        # self.mapping_x = MFC([dt_hist_size, 2048], dropout, have_bn=True, have_last_bn=True)
        # self.mapping_y = MFC([dt_hist_size, 2048], dropout, have_bn=True, have_last_bn=True)
        self.visual_mapping = MFC([2*dt_hist_size + embedding_dim + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, resnext_mean, resnext_tensor, video_mask, n_shots, dt_x, dt_y):
        # dt_x_sum = torch.sum(dt_x, 1)
        # dt_x_sum[dt_x_sum == 0] = 1
        # dt_y_sum = torch.sum(dt_y, 1)
        # dt_y_sum[dt_y_sum == 0] = 1
        # dt_x_mean = dt_x / dt_x_sum.unsqueeze(1)
        # dt_y_mean = dt_y / dt_y_sum.unsqueeze(1)
        #
        # feature_x = self.mapping_x(dt_x_mean)
        # feature_y = self.mapping_y(dt_y_mean)

        resnext_tensor = resnext_tensor[:, :int(max(n_shots).item()), :]
        video_mask = video_mask[:, :int(max(n_shots).item())]
        # Level 1
        org_out = resnext_mean

        # Level 2
        rnn_input = pack_padded_sequence(resnext_tensor, n_shots, batch_first=True, enforce_sorted=False)
        rnn_output, _ = self.rnn(rnn_input)
        padded = pad_packed_sequence(rnn_output, batch_first=True)
        gru_init_out = padded[0]
        mean_gru = torch.zeros(gru_init_out.size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(gru_init_out):
            mean_gru[i] = torch.mean(batch[:int(n_shots[i])], 0)
        gru_out = mean_gru
        gru_out = self.dropout(gru_out)

        # Level 3
        # video_mask = torch.zeros((resnext_tensor.size(0), 60)).cuda()
        # for i in range(n_shots.size(0)):
        #     video_mask[i, :int(n_shots[i].item())] = 1.0
        video_masks = video_mask.unsqueeze(2).expand(-1, -1, gru_init_out.size(2))
        gru_init_out = gru_init_out * video_masks
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((gru_out,con_out,org_out, dt_x, dt_y), 1)
        features = self.visual_mapping(features)
        features = l2norm(features)
        return features


class Base2018(nn.Module):

    def __init__(self, voc_size, text_rnn_size, text_kernel_num, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018, self).__init__()
        self.textBranch = TextBranch(voc_size, 500, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranch(2048, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, bow_mean, gru_vector, lengths,
                resnext101_mean, resnext101_tensor, video_masks, n_shots):
        y_text = self.textBranch(bow_mean, gru_vector, lengths)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots)
        return y_video, y_text


class Base2018dt(nn.Module):

    def __init__(self, voc_size, text_rnn_size, text_kernel_num, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018dt, self).__init__()
        self.textBranch = TextBranch(voc_size, 500, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranchDT(2048, 2500, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, bow_mean, gru_vector, lengths, resnext101_mean, resnext101_tensor, video_masks, n_shots, dt_x, dt_y):
        y_text = self.textBranch(bow_mean, gru_vector, lengths)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots, dt_x, dt_y)
        return y_video, y_text


class Base2018InfersentAll(nn.Module):

    def __init__(self, voc_size, text_rnn_size, text_kernel_num, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018InfersentAll, self).__init__()
        self.textBranch = TextBranchInfersentAll(voc_size, 500, 4096, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranch(2048, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, bow_mean, gru_vector, lengths, infersent, resnext101_mean, resnext101_tensor, video_masks, n_shots):
        y_text = self.textBranch(infersent)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots)
        return y_video, y_text


class Base2018FineTune(nn.Module):

    def __init__(self, base_model, fc_sizes):
        super(Base2018FineTune, self).__init__()
        self.base_model = base_model
        self.fc_text = MFC(fc_sizes, 0.2, have_bn=True, have_last_bn=True)
        self.fc_video = MFC(fc_sizes, 0.2, have_bn=True, have_last_bn=True)

    def forward(self, bow_vector, w2v_vector, gru_vector, lengths, infersent_vector,
                resnext101_tensor, video_masks, n_shots):
        y_text_base, y_video_base = self.base_model(bow_vector, w2v_vector, gru_vector, lengths, infersent_vector,
                resnext101_tensor, video_masks, n_shots)
        y_text = self.fc_text(y_text_base)
        y_video = self.fc_video(y_video_base)
        return y_text, y_video



class Base2018OnlyInfersent(nn.Module):

    def __init__(self):
        super(Base2018OnlyInfersent, self).__init__()
        self.textBranch = TextBranchOnlyInfersent(4096)
        self.videoBranch = VideoBranch(2048, 1024, 512, [2, 3, 4, 5])

    def forward(self, bow_vector, w2v_vector, gru_vector, lengths, infersent_vector,
                resnet152_vector, n_shots):
        y_text = self.textBranch(infersent_vector)
        y_video = self.videoBranch(resnet152_vector, n_shots)
        return y_text, y_video


class Base2018InfersentNoGru(nn.Module):

    def __init__(self):
        super(Base2018InfersentNoGru, self).__init__()
        self.textBranch = TextBranchNoGru(7991, 4096, 512, [2, 3, 4])
        self.videoBranch = VideoBranch(2048, 1024, 512, [2, 3, 4, 5])

    def forward(self, bow_vector, w2v_vector, gru_vector, lengths, infersent_vector,
                resnet152_vector, n_shots):
        y_text = self.textBranch(bow_vector, infersent_vector, lengths)
        y_video = self.videoBranch(resnet152_vector, n_shots)
        return y_text, y_video



class TextBranch2017(nn.Module):

    def __init__(self, voc_size, gru_input_size, gru_hidden_size, gru_n_hidden, w2v_dim, out_dim, device):
        super(TextBranch2017, self).__init__()
        #por ahora solo usaremos w2v como embedding
        #self.gru_branch = GRUBranch(voc_size, gru_input_size, gru_hidden_size, gru_n_hidden, device)
        self.dropout = nn.Dropout(0.2)
        self.dense_1 = nn.Linear(w2v_dim, out_dim)
        self.dense_1_bn = nn.BatchNorm1d(out_dim)
        self.dense_2 = nn.Linear(out_dim, out_dim)
        self.dense_2_bn = nn.BatchNorm1d(out_dim)

    def forward(self, bow_vector, w2v_vector, gru_vector, lengths):
        #y_gru = self.gru_branch(gru_vector, lengths)
        #y_concat = torch.cat((bow_vector, w2v_vector, y_gru), 1)
        #y_concat = y_concat.view(y_concat.size(0), -1)
        y = self.dense_1(w2v_vector)
        y = self.dense_1_bn(y)
        y = nn.ReLU()(y)
        y = self.dropout(y)
        y = self.dense_2(y)
        y = self.dense_2_bn(y)
        y = nn.ReLU()(y)
        return y


class VideoBranch2017(nn.Module):

    def __init__(self, resnet152_dim, gru_dim, att_dim, out_dim, device):  # 2048, 1024, 1024, 2048, gpu:0

        super(VideoBranch2017, self).__init__()
        self.device = device
        #self.gru = nn.GRU(resnet152_dim, gru_dim, batch_first=True)

        # self.w_v = nn.Linear(resnet152_dim, att_dim)
        # self.w_s = nn.Linear(resnet152_dim + gru_dim, att_dim)
        # self.w_m = LinearVector(att_dim)

        self.dense_1 = nn.Linear(resnet152_dim, out_dim)
        self.dense_1_bn = nn.BatchNorm1d(out_dim)
        self.dropout = nn.Dropout(0.2)
        self.dense_2 = nn.Linear(out_dim, out_dim)
        self.dense_2_bn = nn.BatchNorm1d(out_dim)

    def forward(self, resnet152_vectors, n_frames):  #B = batch size, N = max length sequence
        N = n_frames.max().item()
        batch_size = resnet152_vectors.size(0)
        resnet152_vectors = resnet152_vectors[:, :N, :] # delete unnecesary padding.  size = [B, N, 2048]
        # embs = pack_padded_sequence(resnet152_vectors, n_frames, batch_first=True, enforce_sorted=False)
        # h_packed, _ = self.gru(embs)
        # h, _ = pad_packed_sequence(h_packed, batch_first=True) # size of h = [B, N, 1024]
        #
        # # attention mechanism
        # s = torch.cat((h, resnet152_vectors), 2).view(batch_size, N, -1) # size = [B, N, 1024 + 2048]
        # v = resnet152_vectors.sum(dim=1) / n_frames.view(batch_size, -1).float()  # resnet152_vectors.mean() ignoring padding. size = [B, 2048]
        # y_s = self.w_s(s).tanh()  # size = [B, N, 1024] 1024 is the att_size
        # y_v = self.w_v(v).tanh()  # size = [B, 1024]
        # m = torch.mul(y_s, y_v.view(batch_size, 1, -1)) # equation (1), size=[B, N, 1024]
        # r = self.w_m(m)  # equation (2), size=[B, N, 1024]
        # alpha = torch.sum(r, dim=2).softmax(dim=1) # equation (3), size = [B, N]
        # alpha = alpha.view(batch_size, N, 1)   # size = [B, N, 1]
        # v_a = torch.sum(torch.mul(alpha, s), dim=1) # equation (4), size = [B, 2048]

        y = resnet152_vectors.sum(dim=1) / n_frames.view(batch_size, -1).float()

        # fully-connected mapping
        y = self.dense_1(y)
        y = self.dense_1_bn(y)
        y = nn.ReLU()(y)
        y = self.dropout(y)
        y = self.dense_2(y)
        y = self.dense_2_bn(y)
        y = nn.ReLU()(y)
        return y


class BaseModel2017(nn.Module):

    def __init__(self, voc_size, gru_input_size, gru_hidden_size, gru_n_hidden, w2v_dim, text_out_dim, resnet152_dim, video_gru_dim, video_att_dim, video_out_dim, device):
        super(BaseModel2017, self).__init__()
        self.textBranch = TextBranch2017(voc_size, gru_input_size, gru_hidden_size, gru_n_hidden, w2v_dim, text_out_dim, device)
        self.videoBranch = VideoBranch2017(resnet152_dim, video_gru_dim, video_att_dim, video_out_dim, device)

    def forward(self, bow_vector, w2v_vector, gru_vector, lengths,
                resnet152_vector, n_shots):
        y_video = self.videoBranch(resnet152_vector, n_shots)
        y_text = self.textBranch(bow_vector, w2v_vector, gru_vector, lengths)
        return y_video, y_text
