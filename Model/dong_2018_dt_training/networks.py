import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from Model.networks import MFC, l2norm, TextBranch


class VideoBranchAllDT(nn.Module):

    def __init__(self, embedding_dim, dt_hist_size, rnn_size, kernel_num, kernel_sizes, cvs_size, dropout, rnn):
        super(VideoBranchAllDT, self).__init__()
        self.dt_hist_size = dt_hist_size
        self.rnn_output_size = 2 * rnn_size
        self.dropout = nn.Dropout(dropout)
        if rnn == 'gru':
            self.rnn = nn.GRU(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        elif rnn == 'lstm':
            self.rnn = nn.LSTM(embedding_dim, rnn_size, batch_first=True, bidirectional=True)
        else:
            raise ValueError("Only GRU and LSTM supported")

        self.convs1 = nn.ModuleList([
            nn.Conv2d(1, kernel_num, (window_size, self.rnn_output_size), padding=(window_size - 1, 0))
            for window_size in kernel_sizes
        ])

        self.visual_mapping = MFC([2*dt_hist_size + embedding_dim + self.rnn_output_size + kernel_num * len(kernel_sizes), cvs_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, resnext_mean, resnext_tensor, video_mask, n_shots, dt_x, dt_y):
        resnext_tensor = resnext_tensor[:, :int(max(n_shots).item()), :]
        video_mask = video_mask[:, :int(max(n_shots).item())]
        # Level 1
        org_out = resnext_mean

        # Level 2
        rnn_input = pack_padded_sequence(resnext_tensor, n_shots, batch_first=True, enforce_sorted=False)
        rnn_output, _ = self.rnn(rnn_input)
        padded = pad_packed_sequence(rnn_output, batch_first=True)
        gru_init_out = padded[0]
        mean_gru = torch.zeros(gru_init_out.size(0), self.rnn_output_size).cuda()
        for i, batch in enumerate(gru_init_out):
            mean_gru[i] = torch.mean(batch[:int(n_shots[i])], 0)
        gru_out = mean_gru
        gru_out = self.dropout(gru_out)

        # Level 3
        video_masks = video_mask.unsqueeze(2).expand(-1, -1, gru_init_out.size(2))
        gru_init_out = gru_init_out * video_masks
        con_out = gru_init_out.unsqueeze(1)
        con_out = [F.relu(conv(con_out)).squeeze(3) for conv in self.convs1]
        con_out = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in con_out]
        con_out = torch.cat(con_out, 1)
        con_out = self.dropout(con_out)

        features = torch.cat((gru_out,con_out,org_out, dt_x, dt_y), 1)
        features = self.visual_mapping(features)
        features = l2norm(features)
        return features


class Base2018Alldt(nn.Module):

    def __init__(self, voc_size, text_rnn_size, text_kernel_num, video_rnn_size, video_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018Alldt, self).__init__()
        self.textBranch = TextBranch(voc_size, 500, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranchAllDT(2048, 2500, video_rnn_size, video_kernel_num, [2, 3, 4, 5], cvs_size, dropout, rnn)

    def forward(self, bow_mean, gru_vector, lengths, resnext101_mean, resnext101_tensor, video_masks, n_shots, dt_x, dt_y):
        y_text = self.textBranch(bow_mean, gru_vector, lengths)
        y_video = self.videoBranch(resnext101_mean, resnext101_tensor, video_masks, n_shots, dt_x, dt_y)
        return y_video, y_text
