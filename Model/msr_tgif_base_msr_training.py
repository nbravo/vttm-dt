from Model.datasets import MSRVTTBaseLMDB
from Model.losses import TripletLoss, MeanInvertedRank, BatchHardTripetLoss, ContrastiveLoss
from Model.networks import Base2018
from Model.trainer import fit
from Model.utils.logger import TrainingLogger
from torch.utils.data import DataLoader
from Model.utils.early_stopping import EarlyStopping

from torch.optim import Adam
from torch.utils.data import ConcatDataset, random_split
from torch.utils.tensorboard import SummaryWriter
import torch

if __name__ == '__main__':
    start_epoch = -1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_loss = ContrastiveLoss(max_violation=True, margin=0.2)
    val_loss = ContrastiveLoss(max_violation=True, margin=0.2)
    train_loss = train_loss.to(device)
    val_loss = val_loss.to(device)
    lr = 0.0001
    n_epochs = 50
    log_interval = 4
    cuda_on = True
    train_batch_size = 128
    save = True
    workers = 4
    earlystopping = EarlyStopping(3, 10, verbose=True)
    #run1 = run2: loss de ellos
    #run3: loss mia
    tb = SummaryWriter(log_dir='/home/nbravo/vttm-dt/tb/dong_2018_msr/run6/')
    states_path = "/run/media/nbravo/Elements/states/dong_2018_msr/run6/"
    log_path = "Model/log/dong_2018_msr/run6/"
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            log_path + "val_loss.csv", log_path + "val_loss_epoch.csv",
                            log_path + "learning_rate.csv", states_path, save=save)

    dataset_split = 0.8
    msr_dataset = MSRVTTBaseLMDB("Model/data/infersent_lmdb/msr", "MSR", max_readers=workers)
    train_dataset, val_dataset = random_split(msr_dataset, [int(len(msr_dataset)*dataset_split), len(msr_dataset) - int(len(msr_dataset)*dataset_split)])

    # msvd_dataset = MSRVTTBaseLMDB("Model/data/infersent_lmdb/msvd", "MSVD", max_readers=5)
    # tgif_dataset = MSRVTTBaseLMDB("Model/data/infersent_lmdb/tgif", "TGIF", max_readers=5)
    # training_dataset = ConcatDataset([msr_dataset])   #, msvd_dataset, tgif_dataset])
    # validation_dataset_A = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_A", "VTT2016_A")
    # validation_dataset_B = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_B", "VTT2016_B")

    model = Base2018(2048)
    model = model.to(device)
    optimizer = Adam(model.parameters(), lr=lr)

    training_dataloader = DataLoader(train_dataset, batch_size=train_batch_size, shuffle=True,
                                     num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)
    validation_dataloader = DataLoader(val_dataset, batch_size=train_batch_size, shuffle=False,
                                       num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)
    # validation_dataloader_B = DataLoader(validation_dataset_B, batch_size=len(validation_dataset_B), shuffle=False,
    #                                    num_workers=0, collate_fn=msr_dataset.collate_data)
    # validation_dataloaders = (validation_dataloader_A, validation_dataloader_B)

    fit(training_dataloader, validation_dataloader, model, train_loss, val_loss, optimizer, earlystopping, n_epochs, cuda_on,
        log_interval, logger, tb, device, start_epoch=start_epoch)
