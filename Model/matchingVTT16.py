#!/usr/bin/env python
from Model.datasets import BaseVTT16MatchRankDatasetInfersent
from Model.networks import Base2018
from Model.utils.utils import load_checkpoint
import torch


def match_and_rank(video_tensor, sentence_ids, sentence_tensors):
    ranking = []  #list of objecst {index, distance}
    distances = []
    for u in range(sentence_tensors.size(0)):
        dist = video_tensor.unsqueeze(0).mm(sentence_tensors[u].unsqueeze(0).t()).item()
        ranking.append({"index": sentence_ids[u], "distance": dist})
        distances.append(dist)
    sort = sorted(ranking, key=lambda pair: pair['distance'], reverse=True)
    return list(map(lambda pair: pair['index'], sort))


if __name__ == '__main__':
    sets = ["A", "B"]
    save_path = "/home/nbravo/vttm-dt/Model/matching/dong_2018_vtt16_ft/run1/"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Loading model...")
    model = Base2018()
    checkpoint = load_checkpoint("/run/media/nbravo/Elements/states/dong_2018_msr_ft/run1/", 49)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    model = model.to(device)
    print("Loading dataset...")
    dataset = BaseVTT16MatchRankDatasetInfersent(sets, device)

    with torch.no_grad():
        for k in range(len(sets)):
            s = sets[k]
            print("Set", s)
            matching = []
            y_videos = []
            print("Computing video vectors...")
            for j in range(0, len(dataset.videos_id[s]), 128):
                y_video = model.videoBranch(torch.stack(dataset.resnet152_vectors[s][j: j + 128]).cuda(),
                                            torch.Tensor(dataset.n_shots[s][j: j + 128]).cuda())
                for v in y_video:
                    y_videos.append(v)
            y_videos = torch.stack(y_videos)
            print("Computing sentence vectors...")
            y_sentences = model.textBranch(torch.Tensor(dataset.bow[s]).cuda(),
                                          torch.Tensor(dataset.rnn[s]).long().cuda(),
                                          torch.Tensor(dataset.lens[s]).squeeze(1).long().cuda())
            for i in range(len(dataset.videos_id[s])):
                print(str(s) + ":", "Video", dataset.videos_id[s][i])
                ranking = match_and_rank(y_videos[i], dataset.sent_ids[s], y_sentences)
                matching.append((dataset.videos_id[s][i], ranking))
            dataset.save_matching(s, matching, save_path)
