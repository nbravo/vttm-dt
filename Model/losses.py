import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
from sklearn.metrics.pairwise import euclidean_distances


def cosine_sim(im, s):
    """Cosine similarity between all the image and sentence pairs
    """
    return im.mm(s.t())


def euclidean_sim(im, s):
    """Order embeddings similarity measure $max(0, s-im)$
    """
    n = im.size(0)
    d = s.size(0)
    x = s.size(1)
    YmX = (s.unsqueeze(1).expand(d, n, x) - im.unsqueeze(0).expand(d, n, x))
    score = -YmX.pow(2).sum(2).t()
    return score


def binary_search(x, init, end, l):
    "Resturns the index of x in the descending sorted list"
    if end - init <= 1:
        if x > l[init]:
            return max(0, init - 1)
        elif x < l[end]:
            return end + 1
        elif x < l[init]:
            return init + 1
        elif x > l[end]:
            return max(0, end - 1)
        elif x == l[init]:
            return init
        elif x == l[end]:
            return end
    half = init + int((end - init)  / 2)
    if x == l[half]:
        return half
    if x > l[half]:
        return binary_search(x, init, half - 1, l)
    elif x < l[half]:
        return binary_search(x, half + 1, end, l)


class MeanInvertedRank(nn.Module):

    def __init__(self, divide=True):
        super(MeanInvertedRank, self).__init__()
        self.divide = divide

    def forward(self, y_videos, y_text):
        scores = -1*cosine_sim(y_videos, y_text)
        inverted_rank_sum = 0
        for i in range(y_videos.size(0)):
            video_i_distances = scores[i, :].tolist()
            inds = np.argsort(video_i_distances)
            rank = np.where(inds == i)[0][0] + 1
            inverted_rank_sum += 1.0 / rank
        if self.divide:
            return inverted_rank_sum / y_videos.size(0)
        else:
            return inverted_rank_sum


class MixRankLoss(nn.Module):
    
    def __init__(self, metric="cosine"):
        super(MixRankLoss, self).__init__()
        assert metric in ("cosine", "euclidian"), "{} not supported as evaluation metric (euclidian or cosine)".format(metric)
        self.metric = metric

    def forward(self, y_videos, y_text):
        if self.metric == "cosine":
            scores = -1*cosine_sim(y_videos, y_text)
        elif self.metric == "euclidian":
            scores = euclidean_distances(y_videos.cpu().numpy(), y_text.cpu().numpy())
        ranks = np.zeros(y_videos.size(0))
        for i in range(y_videos.size(0)):
            video_i_distances = scores[i, :].tolist()
            inds = np.argsort(video_i_distances)
            rank = np.where(inds == i)[0][0]
            ranks[i] = rank

        r1 = 100.0 * len(np.where(ranks < 1)[0]) / len(ranks)
        r5 = 100.0 * len(np.where(ranks < 5)[0]) / len(ranks)
        r10 = 100.0 * len(np.where(ranks < 10)[0]) / len(ranks)
        medr = np.floor(np.median(ranks)) + 1
        meanr = ranks.mean() + 1

        return list(map(float, [r1, r5, r10, medr, meanr]))

class TripletLoss(nn.Module):
    """
    triplet ranking loss
    """

    def __init__(self, margin=0, max_violation=False, cost_style='sum', direction='all'):
        super(TripletLoss, self).__init__()
        self.margin = margin
        self.cost_style = cost_style
        self.direction = direction
        self.sim = euclidean_sim
        self.max_violation = max_violation

    def forward(self, y_videos, y_sentences):
        # compute image-sentence score matrix
        scores = self.sim(y_videos, y_sentences)
        diagonal = scores.diag().view(y_videos.size(0), 1)
        d1 = diagonal.expand_as(scores)
        d2 = diagonal.t().expand_as(scores)

        # clear diagonals
        I = torch.eye(scores.size(0)) > .5
        if torch.cuda.is_available():
            I = I.cuda()

        cost_s = None
        cost_im = None
        # compare every diagonal score to scores in its column
        if self.direction in ['i2t', 'all']:
            # caption retrieval
            cost_s = (self.margin + scores - d1).clamp(min=0)
            cost_s = cost_s.masked_fill_(I, 0)
        # compare every diagonal score to scores in its row
        if self.direction in ['t2i', 'all']:
            # image retrieval
            cost_im = (self.margin + scores - d2).clamp(min=0)
            cost_im = cost_im.masked_fill_(I, 0)

        # keep the maximum violating negative for each query
        if self.max_violation:
            if cost_s is not None:
                cost_s = cost_s.max(1)[0]
            if cost_im is not None:
                cost_im = cost_im.max(0)[0]

        if cost_s is None:
            cost_s = torch.zeros(1, requires_grad=True).cuda()
        if cost_im is None:
            cost_im = torch.zeros(1, requires_grad=True).cuda()

        if self.cost_style == 'sum':
            return cost_s.sum() + cost_im.sum()
        else:
            return cost_s.mean() + cost_im.mean()


class BatchHardTripetLoss(nn.Module):

    def __init__(self, margin):
        super(BatchHardTripetLoss, self).__init__()
        self.margin = margin

    def forward(self, v_embeddings, s_embeddings):
        N = v_embeddings.size(0)
        device = v_embeddings.device

        distance_v_s = cosine_sim(v_embeddings, s_embeddings)
        distance_s_v = cosine_sim(s_embeddings, v_embeddings)

        # shape (batch_size, 1)
        hardest_positive_mask = torch.eye(v_embeddings.size(0), device=device).bool()

        diag_mask = 3 * torch.eye(v_embeddings.size(0), device=device)
        if torch.cuda.is_available():
            diag_mask = diag_mask.cuda()

        # harsdest negative video->sentence
        distances_no_positives = distance_v_s - diag_mask
        hardest_negative = distances_no_positives.argmax(dim=1).view(v_embeddings.size(0), 1)
        v_s_hardest_negative_mask = torch.zeros(N, N, device=device).scatter(1, hardest_negative,
                                                                         torch.ones(N, N, device=device)).bool()

        # hardest negative sentence -> video
        distances_no_positives = distance_s_v - diag_mask
        hardest_negative = distances_no_positives.argmax(dim=1).view(v_embeddings.size(0), 1)
        s_v_hardest_negative_mask = torch.zeros(N, N, device=device).scatter(1, hardest_negative,
                                                                         torch.ones(N, N, device=device)).bool()

        tl_v_s = self.margin + distance_v_s[v_s_hardest_negative_mask] - distance_v_s[hardest_positive_mask]
        tl_s_v = self.margin + distance_s_v[s_v_hardest_negative_mask] - distance_s_v[hardest_positive_mask]
        tl_v_s[tl_v_s < 0] = 0
        tl_s_v[tl_s_v < 0] = 0
        tl = tl_v_s + tl_s_v
        triplet_loss = tl.sum()

        return triplet_loss


class ContrastiveLoss(nn.Module):

    def __init__(self, margin=0, max_violation=False):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin
        self.sim = euclidean_sim
        self.max_violation = max_violation

    def forward(self, s, im):

        scores = self.sim(im, s)
        diagonal = scores.diag().view(im.size(0), 1)
        d1 = diagonal.expand_as(scores)
        d2 = diagonal.t().expand_as(scores)

        cost_s = (self.margin + scores - d1).clamp(min=0)
        cost_im = (self.margin + scores - d2).clamp(min=0)

        mask = torch.eye(scores.size(0)) > .5
        I = Variable(mask)
        if torch.cuda.is_available():
            I = I.cuda()
        cost_s = cost_s.masked_fill_(I, 0)
        cost_im = cost_im.masked_fill_(I, 0)

        if self.max_violation:
            cost_s = cost_s.max(1)[0]
            cost_im = cost_im.max(0)[0]

        return cost_s.sum() + cost_im.sum()
