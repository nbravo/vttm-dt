from Model.datasets import VTT19MatchRankDataset
from Model.networks import DTW2VV
from Model.utils import load_checkpoint
import torch


def match_and_rank(video_tensor, sentence_tensors):
    ranking = []  #list of objecst {index, distance}
    for i in range(0, len(sentence_tensors)):
        dist = abs(torch.cosine_similarity(video_tensor.squeeze(), sentence_tensors[i], dim=0).item())
        ranking.append({"index": i + 1, "distance": dist})
    sort = sorted(ranking, key=lambda pair: pair['distance'])
    return list(map(lambda pair: pair['index'], sort))


if __name__ == '__main__':
    subset = "E"

    with torch.no_grad():
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        print("Loading model...")
        model = DTW2VV(7217, 32, 300, 1024, 300, 2048, 2048, 2048, 800, 2048, device)
        checkpoint = load_checkpoint(4)
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        model = model.to(device)
        print("Loading dataset...")
        dataset = VTT19MatchRankDataset(subset, device)

        print("Loading sentences from subset {}...",format(subset))
        y_sentences = model.textBranch(torch.stack(dataset.sentences[0]), torch.stack(dataset.sentences[1]),
                                       torch.stack(dataset.sentences[2]), torch.stack(dataset.sentences[3]))
        for i in range(len(dataset.resnet152_vectors)):
            print("Video", dataset.videos_index[i])
            video = model.videoBranch(dataset.resnet152_vectors[i].unsqueeze(0), dataset.resnext101_vectors[i].unsqueeze(0), dataset.dts_vectors[i].unsqueeze(0))
            ranking = match_and_rank(video, y_sentences)
            dataset.save_matching(i, subset, ranking)
