from Model.datasets import VTTBaseLMDB
from Model.losses import TripletLoss, MixRankLoss
from Model.networks import Base2018
from Model.trainer import fit
from Model.utils.logger import TrainingLogger
from Model.utils.early_stopping import EarlyStopping

import torch
import os

from torch.utils.data import DataLoader, ConcatDataset
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter


def check_dir(dir_path, save=True):
    if save:
        try:
            os.listdir(dir_path)
        except FileNotFoundError:
            os.makedirs(dir_path)


if __name__ == '__main__':
    start_epoch = -1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    loss = TripletLoss(max_violation=True, margin=2.0, direction='all')
    loss = loss.to(device)
    val_loss = MixRankLoss(metric="euclidian")
    val_loss = val_loss.to(device)
    resnext_model = 'mediamill'
    lr = 0.0001
    n_epochs = 50
    log_interval = 100
    cuda_on = True
    train_batch_size = 128
    save = True
    workers = 8
    earlystopping = EarlyStopping(3, 10, verbose=True)
    run = 1
    tb = SummaryWriter(log_dir='/home/nbravo/vttm-dt/tb/dong_2018_{}_base_l2/run{}/'.format(resnext_model, run))
    check_dir(tb.log_dir, save=save)
    states_path = "/run/media/nbravo/Elements/states/dong_2018_{}_base_l2/run{}/".format(resnext_model, run)
    check_dir(states_path, save=save)
    log_path = "/home/nbravo/vttm-dt/Model/log/dong_2018_{}_base_l2/run{}/".format(resnext_model, run)
    check_dir(log_path, save=save)
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            log_path + "val_loss.csv", log_path + "val_loss_epoch.csv",
                            log_path + "learning_rate.csv", states_path, save=save, val_metric=True)

    msr_dataset = VTTBaseLMDB("Model/data/all_lmdb/msr", "MSR", resnext_model=resnext_model, max_readers=workers)
    tgif_dataset = VTTBaseLMDB("Model/data/all_lmdb/tgif", "TGIF", resnext_model=resnext_model, max_readers=workers)
    msvd_dataset = VTTBaseLMDB("Model/data/all_lmdb/msvd", "MSVD", resnext_model=resnext_model, max_readers=workers)
    training_dataset = ConcatDataset((msr_dataset, tgif_dataset))

    validation_dataset_A = VTTBaseLMDB("Model/data/all_lmdb/VTT2016_A", "VTT2016_A", resnext_model=resnext_model)

    model = Base2018(10192, 1024, 512, 1024, 512, 2048, dropout=0.2, rnn='gru')
    model = model.to(device)
    optimizer = Adam(model.parameters(), lr=lr)

    training_dataloader = DataLoader(training_dataset, batch_size=train_batch_size, shuffle=True,
                                     num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)
    validation_dataloader = DataLoader(validation_dataset_A, batch_size=train_batch_size, shuffle=False,
                                       num_workers=workers, pin_memory=True, collate_fn=msr_dataset.collate_data)

    fit(training_dataloader, validation_dataloader, model, loss, val_loss, optimizer, earlystopping, n_epochs, cuda_on,
        log_interval, logger, tb, device, start_epoch=start_epoch)
