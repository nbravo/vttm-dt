import numpy as np
import pyarrow as pa
import lmdb
import os.path as osp
import torch
from torch.utils.data.dataset import Dataset
import pickle
import pandas as pd


def read_text(text_path):
    try:
        with open(text_path, "rb") as fin:
            text = pickle.load(fin)
        return text
    except Exception:
        print(text_path)
        exit(1)


class VTTOnlyDTLMDB(Dataset):

    def __init__(self, lmdb_path, dataset_name, max_readers=1):
        super(VTTOnlyDTLMDB, self).__init__()
        max_readers = max(1, max_readers)
        self.db_path = lmdb_path
        self.dataset_name = dataset_name
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.env = lmdb.open(lmdb_path, subdir=osp.isdir(lmdb_path),
                             readonly=True, lock=False,
                             readahead=False, meminit=False, max_readers=max_readers)
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        self.length = pa.deserialize(cursor.get(b'__len__'))
        self.keys = pa.deserialize(cursor.get(b'__keys__'))
        cursor.close()
        self.video_max_shots = 64
        self.video_init_vector = np.random.uniform(-1, 1, 2048)
        self.video_end_vector = np.random.uniform(-1, 1, 2048)

    def __getitem__(self, index):
        txn = self.env.begin(write=False)
        cursor = txn.cursor()
        byteflow = cursor.get(self.keys[index])
        cursor.close()
        vectors = pa.deserialize(byteflow)
        # dataset_name, bow, rnn, sent_len,
        # dt_x, dt_y
        return self.dataset_name, vectors[2],  vectors[3], int(vectors[4]), \
               vectors[10], vectors[11]

    def __len__(self):
        return self.length

    def __repr__(self):
        return self.__class__.__name__ + ' (' + self.db_path + ')'

    def close(self):
        self.env.close()

    def __dt_hist(self, name, dt):
        hist = np.zeros(2500)
        if max(dt) == 0:
            return hist
        for i in dt:
            hist[i] += 1
        return hist

    def collate_data(self, data):
        dataset_name, bow, seq_indices, lens, dt_x, dt_y = zip(*data)

        max_len = max(lens)
        rnn_seq_indices = 2*np.ones((len(seq_indices), max_len))
        bow_mean = np.zeros((len(bow), len(bow[0])))
        dt_x = np.array(dt_x)
        dt_y = np.array(dt_y)
        for i in range(len(bow)):
            bow_mean[i] = bow[i] / np.sum(bow[i])
            rnn_seq_indices[i, :lens[i]] = seq_indices[i]

        return torch.Tensor(bow_mean), torch.Tensor(rnn_seq_indices).long(), torch.Tensor(lens), \
               torch.Tensor(dt_x), torch.Tensor(dt_y)


class VTT18OnlyDTMatchRankDataset(Dataset):
    def __init__(self, sets, device):
        super(VTT18OnlyDTMatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("Model/data/vtt2018/dt/videos_mediamill.csv")
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.dt_x, self.dt_y = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/dt/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        dt_x_list = []
        dt_y_list = []
        for i in range(len(videos)):
            dt_x, dt_y = pickle.load(open(videos.iloc[i, 3], 'rb'))
            dt_x_list.append(torch.from_numpy(dt_x).float())
            dt_y_list.append(torch.from_numpy(dt_y).float())
        return dt_x_list, dt_y_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)


class VTT18OnlyDTMatchRankDatasetTraining(Dataset):
    def __init__(self, sample_folder, device):
        super(VTT18OnlyDTMatchRankDatasetTraining, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv(sample_folder + 'videos.csv')
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.dt_x, self.dt_y = self.load_videos(self.videos_csv)

        self.sent_ids, self.bow_mean, self.rnn, self.lens = self.load_sentences(pd.read_csv(sample_folder + 'sentences.csv'))

    def load_videos(self, videos):
        dt_x_list = []
        dt_y_list = []
        for i in range(len(videos)):
            dt_x, dt_y = pickle.load(open(videos.iloc[i, 3], 'rb'))
            dt_x_list.append(torch.from_numpy(dt_x).float())
            dt_y_list.append(torch.from_numpy(dt_y).float())
        return dt_x_list, dt_y_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, sample_folder, model, matching_sent_list):
        save_path = sample_folder + 'matching/' + model + '.match'
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(sent_id for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)


class BaseVTT18MatchRankDataset(Dataset):
    def __init__(self, sets, device, resnext_model="mediamill", video_init_vector=None, video_end_vector=None):
        super(BaseVTT18MatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv("Model/data/vtt2018/base_2018/videos_{}.csv".format(resnext_model))
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids = {}
        self.bow_mean = {}
        self.rnn = {}
        self.lens = {}
        for s in sets:
            self.sent_ids[s], self.bow_mean[s], self.rnn[s], self.lens[s] = self.load_sentences(pd.read_csv("Model/data/vtt2018/base_2018/sentences_{}.csv".format(s.upper())))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, SET, matching_sent_list, matching_path):
        save_path = (matching_path + "{}.match").format(SET.upper())
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(repr(sent_id) for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)


class BaseTrainingSampleMatchRankDataset(Dataset):
    def __init__(self, device, sample_folder, video_init_vector=None, video_end_vector=None):
        super(BaseTrainingSampleMatchRankDataset, self).__init__()
        self.device = device
        self.videos_csv = pd.read_csv(sample_folder + 'videos.csv')
        self.max_video_len = max(self.videos_csv.n_shots)
        self.videos_index = self.videos_csv.video_index
        self.video_init_vector = video_init_vector
        self.video_end_vector = video_end_vector
        self.resnext_mean, self.resnet152_vectors, self.video_masks, self.n_shots = self.load_videos(self.videos_csv)

        self.sent_ids, self.bow_mean, self.rnn, self.lens = self.load_sentences(pd.read_csv(sample_folder + 'sentences.csv'))

    def load_videos(self, videos):
        resnext_mean = []
        resnet152_vectors = []
        video_masks = []
        n_shots_list = []
        for i in range(len(videos)):
            resnet152_narray = np.array(pickle.load(open(videos.iloc[i, 1], 'rb')))
            resnext_mean.append(torch.from_numpy(np.mean(resnet152_narray, 0)))
            video_mask = np.zeros(self.max_video_len + 2)
            n_shots = len(resnet152_narray)
            if n_shots >= self.max_video_len:
                resnet152_narray = np.concatenate(([self.video_init_vector], resnet152_narray[:self.max_video_len],
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_narray).float().to(self.device)  # n_shots x 2048
                video_mask[:] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(self.max_video_len + 2)
            else:
                diff_max_len = self.max_video_len - n_shots
                resnet152_padding = np.concatenate(([self.video_init_vector], resnet152_narray,
                                                   [self.video_end_vector]), axis=0)
                resnet152_tensor = torch.from_numpy(resnet152_padding).float()
                resnet152_tensor = torch.cat((resnet152_tensor, torch.zeros(diff_max_len, 2048, dtype=torch.float)),
                                             0).float().to(self.device)
                video_mask[:n_shots + 2] = 1.0
                video_mask = torch.from_numpy((video_mask))
                resnet152_vectors.append(resnet152_tensor)
                video_masks.append(video_mask)
                n_shots_list.append(n_shots + 2)
        return resnext_mean, resnet152_vectors, video_masks, n_shots_list

    def load_sentences(self, sentences):
        sentence_id = []
        bow_mean_vectors = []
        gru_vectors = []
        lens_vectors = []

        for i in range(len(sentences)):
            sentence_id.append(sentences.iloc[i, 0])
            bow_representation = read_text(sentences.iloc[i, 1])
            bow_mean_vectors.append(bow_representation / np.sum(bow_representation))
            gru_representation = read_text(sentences.iloc[i, 2])
            gru_vectors.append(gru_representation)
            lens_vectors.append([sentences.iloc[i, 3]])
        return sentence_id, bow_mean_vectors, gru_vectors, lens_vectors

    def save_matching(self, sample_folder, model, matching_sent_list):
        save_path = sample_folder + 'matching/' + model + '.match'
        with open(save_path, "w") as fin:
            for video_id, ranking in matching_sent_list:
                fin.write(str(video_id) + ", " + ", ".join(sent_id for sent_id in ranking) + '\n')

    def collate_sentences(self, bow_mean, seq_indices, lens, device):
        lens = np.array([len(seq) for seq in seq_indices])
        max_len = max(lens)
        bow_mean = np.array(bow_mean)      # we sort by desceing sentence length order, we must keep in mind this rearrenge must affect every other data
        rnn_seq_indices = np.zeros((len(seq_indices), max_len))
        for i in range(len(seq_indices)):
            rnn_seq_indices[i] = np.concatenate((seq_indices[i], np.repeat(2, max_len - lens[i])))
        return torch.Tensor(bow_mean).to(device), torch.Tensor(rnn_seq_indices).long().to(device), torch.Tensor(lens).to(device)