from Model.dong_2018_base_training.datasets import BaseTrainingSampleMatchRankDataset
from Model.networks import Base2018
from Model.utils.utils import load_best_model
import numpy as np
import torch


def match_and_rank(video_tensor, sentence_ids, sentence_tensors):
    scores = -1*video_tensor.unsqueeze(0).mm(sentence_tensors.t()).cpu().numpy()
    inds = np.argsort(scores[0])
    return sentence_ids[inds].tolist()


if __name__ == '__main__':
    sample_folder = "/home/nbravo/vttm-dt/Model/training_samples/sample_3/"
    run = 3
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = Base2018(10192, 1024, 512, 1024, 512, 2048, dropout=0.2, rnn='gru')
    resnext_model = 'mediamill'
    checkpoint = load_best_model("/run/media/nbravo/Elements/states/dong_2018_{}_base/run{}/".format(resnext_model, run))
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    model = model.to(device)
    dataset = BaseTrainingSampleMatchRankDataset(device, sample_folder,
                                         video_init_vector=checkpoint['video_init_vector'],
                                         video_end_vector=checkpoint['video_end_vector'])

    with torch.no_grad():
        print("Computing video vectors...")
        y_videos = []
        for j in range(0, len(dataset.videos_index), 128):
            y_video = model.videoBranch(torch.stack(dataset.resnext_mean[j: j + 128]).cuda(),
                                        torch.stack(dataset.resnet152_vectors[j: j + 128]).cuda(),
                                        torch.stack(dataset.video_masks[j: j + 128]).float().cuda(),
                                        torch.Tensor(dataset.n_shots[j: j + 128]).cuda())
            for v in y_video:
                y_videos.append(v)
        y_videos = torch.stack(y_videos).cpu()
        sentence_ids = np.array(dataset.sent_ids)
        matching = []
        print("Computing sentence vectors...")
        y_sentences = model.textBranch(*dataset.collate_sentences(dataset.bow_mean,
                                                                  dataset.rnn,
                                                                  dataset.lens,
                                                                  device))
        y_sentences = y_sentences.cpu()
        for i in range(len(dataset.videos_index)):
            if (i + 1) % 500 == 0:
                print("Video", i + 1, "of", len(dataset.videos_index))
            ranking = match_and_rank(y_videos[i], sentence_ids, y_sentences)
            matching.append((dataset.videos_index[i], ranking))
        dataset.save_matching(sample_folder, 'base_mediamill_{}'.format(run), matching)
