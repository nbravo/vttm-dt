import torch
import torch.nn as nn
from Model.networks import TextBranch, MFC, l2norm


class VideoBranchOnlyDT(nn.Module):

    def __init__(self, dt_hist_size, cvs_size, dropout):
        super(VideoBranchOnlyDT, self).__init__()
        self.dt_hist_size = dt_hist_size
        self.dropout = nn.Dropout(dropout)
        self.visual_mapping = MFC([2*dt_hist_size, cvs_size], dropout, have_bn=True, have_last_bn=True)

    def forward(self, dt_x, dt_y):
        features = torch.cat((dt_x, dt_y), 1)
        features = self.visual_mapping(features)
        features = l2norm(features)
        return features


class Base2018Onlydt(nn.Module):

    def __init__(self, voc_size, text_rnn_size, text_kernel_num, cvs_size, dropout=0.2, rnn='gru'):
        super(Base2018Onlydt, self).__init__()
        self.textBranch = TextBranch(voc_size, 500, text_rnn_size, text_kernel_num, [2, 3, 4], cvs_size, dropout, rnn)
        self.videoBranch = VideoBranchOnlyDT(2500, cvs_size, dropout)

    def forward(self, bow_mean, gru_vector, lengths, dt_x, dt_y):
        y_text = self.textBranch(bow_mean, gru_vector, lengths)
        y_video = self.videoBranch(dt_x, dt_y)
        return y_video, y_text