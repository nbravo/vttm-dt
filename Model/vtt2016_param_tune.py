from Model.datasets import MSRVTTBaseLMDB
from Model.losses import TripletLoss, ContrastiveLoss
from Model.networks import Base2018
from Model.trainer import fit_param_tune
from Model.utils.logger import TrainingLogger
from torch.utils.data import DataLoader
from Model.utils.early_stopping import EarlyStopping

import random
import os
import torch
from torch.utils.data.dataset import random_split
from torch.utils.data.dataset import ConcatDataset
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter
import pickle


def check_dir(dir_path):
    try:
        os.listdir(dir_path)
    except FileNotFoundError:
        os.makedirs(dir_path)


def check_configuration(lr, train_batch_size, common_vector_space_size, train_dataset, val_dataset):
    n_epochs = 50
    cuda_on = True
    log_interval = 4
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_loss = ContrastiveLoss(max_violation=True, margin=0.2)
    train_loss = train_loss.to(device)
    val_loss = TripletLoss(max_violation=True, margin=0.2)
    val_loss = val_loss.to(device)
    log_dir = "run_base2018_vtt2016_lr_{:.6f}_batchsize_{}_csvsize_{}".format(lr, train_batch_size, common_vector_space_size)
    check_dir("/home/nbravo/vttm-dt/tb/dong_2018_vtt2016_param_tune_new_loss/{}/".format(log_dir))
    check_dir("Model/log/dong_2018_vtt2016_param_tune_new_loss/{}/".format(log_dir))
    tb = SummaryWriter(log_dir="/home/nbravo/vttm-dt/tb/dong_2018_vtt2016_param_tune_new_loss/{}/".format(log_dir))
    log_path = "Model/log/dong_2018_vtt2016_param_tune_new_loss/{}/".format(log_dir)
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            log_path + "val_loss.csv", log_path + "val_loss_epoch.csv",
                            log_path + "learning_rate.csv", None, save=True)

    earlystopping = EarlyStopping(3, 10, verbose=True)
    model = Base2018(common_vector_space_size)
    model = model.to(device)
    optimizer = Adam(model.parameters(), lr=lr)

    train_dataloader = DataLoader(train_dataset, batch_size=train_batch_size, shuffle=True,
                                       num_workers=workers, pin_memory=True, collate_fn=dataset_A.collate_data)
    val_dataloader = DataLoader(val_dataset, batch_size=train_batch_size, shuffle=False,
                                        num_workers=workers, pin_memory=True, collate_fn=dataset_A.collate_data)

    print("Run lr={:.4f} batchsize={} csvsize={}".format(lr, train_batch_size, common_vector_space_size))
    fit_param_tune(train_dataloader, val_dataloader, model, train_loss, val_loss, optimizer, earlystopping, n_epochs, cuda_on, log_interval, logger, tb)


if __name__ == "__main__":
    n_runs = 10
    lr_range = (0.00001, 0.01)
    batch_size_range = (50, 150)
    csv_size_range = (512, 4096)

    workers = 2

    dataset_A = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_A", "VTT2016_A", max_readers=workers)
    dataset_B = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_B", "VTT2016_B", max_readers=workers)
    dataset = ConcatDataset((dataset_A, dataset_B))

    dataset_split = 0.7
    train_dataset, val_dataset = random_split(dataset, [int(len(dataset)*dataset_split), len(dataset) - int(len(dataset)*dataset_split)])

    run = 0
    while run < n_runs:
        lr = random.uniform(lr_range[0], lr_range[1])
        #train_batch_size = random.randint(batch_size_range[0], batch_size_range[1])
        train_batch_size = 128
        # csv_size = random.randint(csv_size_range[0], csv_size_range[1])
        csv_size = 2048
        check_configuration(lr, train_batch_size, csv_size, train_dataset, val_dataset)
        run += 1