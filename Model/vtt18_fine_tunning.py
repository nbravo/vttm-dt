from Model.datasets import MSRVTTBaseLMDB
from Model.losses import TripletLoss
from Model.networks import Base2018, Base2018FineTune
from Model.trainer import fine_tune
from Model.utils.utils import load_checkpoint
from Model.utils.logger import TrainingLogger
from torch.utils.data import DataLoader

from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter
from Model.utils.early_stopping import EarlyStopping


import torch

if __name__ == '__main__':
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_loss = TripletLoss(max_violation=True, margin=0.2)
    train_loss = train_loss.to(device)
    n_epochs = 100
    log_interval = 4
    cuda_on = True
    train_batch_size = 128
    save = True
    workers = 2
    earlystopping = EarlyStopping(3, 10, verbose=True)
    tb = SummaryWriter(log_dir='/home/nbravo/vttm-dt/tb/dong_2018_msr_ft/run4/')
    states_path = "/run/media/nbravo/Elements/states/dong_2018_msr_ft/run4/"
    log_path = "Model/log/dong_2018_msr_ft/run4/"
    logger = TrainingLogger(log_path + "train_loss.csv", log_path + "train_loss_epoch.csv",
                            None, None,
                            log_path + "learning_rate.csv", states_path, save=save)

    dataset_A = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_A", "VTT2016_A", max_readers=workers)
    dataset_B = MSRVTTBaseLMDB("Model/data/infersent_lmdb/VTT2016_B", "VTT2016_B", max_readers=workers)

    checkpoint = load_checkpoint("/run/media/nbravo/Elements/states/dong_2018_msr/run3/", epoch=22)
    model = Base2018()
    model.load_state_dict(checkpoint['model_state_dict'])

    # model = Base2018FineTune(base_model, [2048, 4096, 2048])

    # for param in model.base_model.parameters():
    #     param.require_grad = False

    model = model.to(device)
    # lr = checkpoint['lr']
    optimizer = Adam(model.parameters(), lr=0.001)
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

    dataloader_A = DataLoader(dataset_A, batch_size=train_batch_size, shuffle=True,
                                       num_workers=workers, pin_memory=True, collate_fn=dataset_A.collate_data)
    dataloader_B = DataLoader(dataset_B, batch_size=train_batch_size, shuffle=True,
                                        num_workers=workers, pin_memory=True, collate_fn=dataset_A.collate_data)
    ft_dataloaders = (dataloader_A, dataloader_B)

    fine_tune(ft_dataloaders, model, train_loss, optimizer, earlystopping, n_epochs, cuda_on, log_interval, logger, tb)
