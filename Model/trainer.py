import numpy as np
import torch
from torch.nn.utils.clip_grad import clip_grad_norm_
from random import randint


def fit(train_loader, val_loader, model, train_loss_fn, val_loss_fn, optimizer, earlystopping, n_epochs, cuda, log_interval,
        logger, tb, device,
        start_epoch=-1):

    if start_epoch >= 0:  # resume from epoch
        checkpoint = logger.load_checkpoint(epoch=start_epoch)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        lr = checkpoint['lr']
        start_epoch = start_epoch + 1
    else:
        start_epoch = 0
        lr = None

    if lr:
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

    n_iters = 0
    for epoch in range(start_epoch, n_epochs):

        if logger is not None and logger.save:
            for param_group in optimizer.param_groups:
                tb.add_scalar("Learning Rate", param_group['lr'], epoch)
                logger.log_learning_rate(epoch, param_group['lr'])

            tb.add_histogram("text/embeddings", model.textBranch.embedding.weight, epoch)
            tb.add_histogram("text/rnn/weight_ih_l0", model.textBranch.rnn.weight_ih_l0, epoch)
            tb.add_histogram("text/rnn/weight_hh_l0", model.textBranch.rnn.weight_hh_l0, epoch)
            tb.add_histogram("text/rnn/bias_ih_l0", model.textBranch.rnn.bias_ih_l0, epoch)
            tb.add_histogram("text/rnn/bias_hh_l0", model.textBranch.rnn.bias_hh_l0, epoch)

            #tb.add_histogram("video/rnn/weight_ih_l0", model.videoBranch.rnn.weight_ih_l0, epoch)
            #tb.add_histogram("video/rnn/weight_hh_l0", model.videoBranch.rnn.weight_hh_l0, epoch)
            #tb.add_histogram("video/rnn/bias_ih_l0", model.videoBranch.rnn.bias_ih_l0, epoch)
            #tb.add_histogram("video/rnn/bias_hh_l0", model.videoBranch.rnn.bias_hh_l0, epoch)

            i = 0
            for conv in model.textBranch.convs1:
                tb.add_histogram("text/conv{}/weight".format(i), conv.weight, epoch)
                tb.add_histogram("text/conv{}/bias".format(i), conv.bias, epoch)
                i += 1

            # i = 0
            # for conv in model.videoBranch.convs1:
            #     tb.add_histogram("video/conv{}/weight".format(i), conv.weight, epoch)
            #     tb.add_histogram("video/conv{}/bias".format(i), conv.bias, epoch)
            #     i += 1

            tb.add_histogram("text/MFC/weight", model.textBranch.text_mapping.fc1.weight, epoch)
            tb.add_histogram("text/MFC/bias", model.textBranch.text_mapping.fc1.bias, epoch)

            tb.add_histogram("video/MFC/weight", model.videoBranch.visual_mapping.fc1.weight, epoch)
            tb.add_histogram("video/MFC/bias", model.videoBranch.visual_mapping.fc1.bias, epoch)

        # Train stage
        train_loss, n_iters = train_epoch(epoch, train_loader, model, train_loss_fn, optimizer, cuda, log_interval, logger, tb, n_iters)
        if tb is not None and logger is not None:
            tb.add_scalar("Loss/Train", train_loss, epoch)
            logger.log_train_loss_epoch(epoch, train_loss)

        # # Validation stage
        val_loss = float(mir_epoch(epoch, val_loader, model, val_loss_fn, cuda, logger))
        if tb is not None and logger is not None:
            tb.add_scalar("Loss/Validation", val_loss, epoch)
            logger.log_val_loss_epoch(epoch, val_loss)

        print('Epoch: {}/{}. Train set: Sum loss: {}'.format(epoch, n_epochs, "{0:.5f}".format(train_loss)))
        print('Epoch: {}/{}. Validation set: Sum loss: {}'.format(epoch, n_epochs, "{0:.5f}".format(val_loss)))

        print("Stepping:", end=' ')
        print(optimizer.param_groups[0]['lr'], end=' ')
        optimizer.param_groups[0]['lr'] *= 0.99
        print("-->", end=' ')
        print(optimizer.param_groups[0]['lr'])

        earlystopping(val_loss)
        if earlystopping.early_stop:
            break
        elif earlystopping.step:  # si cada 3 epoch no baja el loss, hacemos un step
            earlystopping.step = False
            earlystopping.step_counter = 0
            print("Stepping:", end=' ')
            print(optimizer.param_groups[0]['lr'], end=' ')
            optimizer.param_groups[0]['lr'] *= 0.5
            print("-->", end=' ')
            print(optimizer.param_groups[0]['lr'])
        if earlystopping.early_stop_counter == 0:
            # save best_model
            if logger is not None:
                logger.save_best_model({
                    'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'train_loss': train_loss,
                    'val_loss': val_loss,
                    'lr': optimizer.param_groups[0]['lr'],
                    'video_init_vector': train_loader.dataset.datasets[0].video_init_vector,
                    'video_end_vector': train_loader.dataset.datasets[0].video_end_vector
                })
                print("Best model saved. Epoch: {}".format(epoch))
    if logger is not None:
        logger.close()


def fine_tune(ft_dataloaders, model, train_loss_fn, optimizer, earlystopping, n_epochs, cuda, log_interval, logger, tb):
    for epoch in range(n_epochs):

        if logger.save:
            for param_group in optimizer.param_groups:
                tb.add_scalar("Learning Rate", param_group['lr'], epoch)
                logger.log_learning_rate(epoch, param_group['lr'])

            tb.add_histogram("text/rnn/weight_ih_l0", model.textBranch.gru_branch.gru.weight_ih_l0, epoch)
            tb.add_histogram("text/rnn/weight_hh_l0", model.textBranch.gru_branch.gru.weight_hh_l0, epoch)
            tb.add_histogram("text/rnn/bias_ih_l0", model.textBranch.gru_branch.gru.bias_ih_l0, epoch)
            tb.add_histogram("text/rnn/bias_hh_l0", model.textBranch.gru_branch.gru.bias_hh_l0, epoch)

            tb.add_histogram("video/rnn/weight_ih_l0", model.videoBranch.rnn.weight_ih_l0, epoch)
            tb.add_histogram("video/rnn/weight_hh_l0", model.videoBranch.rnn.weight_hh_l0, epoch)
            tb.add_histogram("video/rnn/bias_ih_l0", model.videoBranch.rnn.bias_ih_l0, epoch)
            tb.add_histogram("video/rnn/bias_hh_l0", model.videoBranch.rnn.bias_hh_l0, epoch)

            i = 0
            for conv in model.textBranch.convs1:
                tb.add_histogram("text/conv{}/weight".format(i), conv.weight, epoch)
                tb.add_histogram("text/conv{}/bias".format(i), conv.bias, epoch)
                i += 1

            i = 0
            for conv in model.videoBranch.convs1:
                tb.add_histogram("video/conv{}/weight".format(i), conv.weight, epoch)
                tb.add_histogram("video/conv{}/bias".format(i), conv.bias, epoch)
                i += 1

            tb.add_histogram("text/MFC/weight", model.textBranch.text_mapping.fc1.weight, epoch)
            tb.add_histogram("text/MFC/bias", model.textBranch.text_mapping.fc1.bias, epoch)

            tb.add_histogram("video/MFC/weight", model.videoBranch.visual_mapping.fc1.weight, epoch)
            tb.add_histogram("video/MFC/bias", model.videoBranch.visual_mapping.fc1.bias, epoch)

            # tb.add_histogram("text/MFC2.1/weight", model.fc_text.fc1.weight, epoch)
            # tb.add_histogram("text/MFC2.1/bias", model.fc_text.fc1.bias, epoch)
            # tb.add_histogram("text/MFC2.2/weight", model.fc_text.fc2.weight, epoch)
            # tb.add_histogram("text/MFC2.2/bias", model.fc_text.fc2.bias, epoch)
            #
            # tb.add_histogram("video/MFC2.1/weight", model.fc_video.fc2.weight, epoch)
            # tb.add_histogram("video/MFC2.1/bias", model.fc_video.fc2.bias, epoch)
            # tb.add_histogram("video/MFC2.2/weight", model.fc_video.fc2.weight, epoch)
            # tb.add_histogram("video/MFC2.2/bias", model.fc_video.fc2.bias, epoch)

        # Train stage
        train_loss = train_ft_epoch(epoch, ft_dataloaders, model, train_loss_fn, optimizer, cuda, log_interval, logger)
        tb.add_scalar("Loss/Train", train_loss, epoch)
        logger.log_train_loss_epoch(epoch, train_loss)

        print('Epoch: {}/{}. Train set: Average loss: {}'.format(epoch, n_epochs, "{0:.5f}".format(train_loss)))

        print("Stepping:", end=' ')
        print(optimizer.param_groups[0]['lr'], end=' ')
        optimizer.param_groups[0]['lr'] *= 0.99
        print("-->", end=' ')
        print(optimizer.param_groups[0]['lr'])

        # save the current model
        logger.save_checkpoint(epoch, {
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'train_loss': train_loss,
            'lr': optimizer.param_groups[0]['lr'],
        })
        earlystopping(train_loss)
        if earlystopping.early_stop:
            break
        elif earlystopping.step:
            earlystopping.step = False
            earlystopping.step_counter = 0
            print("Stepping:", end=' ')
            print(optimizer.param_groups[0]['lr'], end=' ')
            optimizer.param_groups[0]['lr'] *= 0.5
            print("-->", end=' ')
            print(optimizer.param_groups[0]['lr'])
    logger.close()


def fit_param_tune(train_loader, val_loader, model, train_loss_fn, val_loss_fn, optimizer, earlystopping, n_epochs, cuda, log_interval, logger, tb):
    n_iters = 0
    for epoch in range(n_epochs):

        if logger.save:
            for param_group in optimizer.param_groups:
                tb.add_scalar("Learning Rate", param_group['lr'], epoch)
                logger.log_learning_rate(epoch, param_group['lr'])

            tb.add_histogram("text/rnn/weight_ih_l0", model.textBranch.gru_branch.gru.weight_ih_l0, epoch)
            tb.add_histogram("text/rnn/weight_hh_l0", model.textBranch.gru_branch.gru.weight_hh_l0, epoch)
            tb.add_histogram("text/rnn/bias_ih_l0", model.textBranch.gru_branch.gru.bias_ih_l0, epoch)
            tb.add_histogram("text/rnn/bias_hh_l0", model.textBranch.gru_branch.gru.bias_hh_l0, epoch)

            tb.add_histogram("video/rnn/weight_ih_l0", model.videoBranch.rnn.weight_ih_l0, epoch)
            tb.add_histogram("video/rnn/weight_hh_l0", model.videoBranch.rnn.weight_hh_l0, epoch)
            tb.add_histogram("video/rnn/bias_ih_l0", model.videoBranch.rnn.bias_ih_l0, epoch)
            tb.add_histogram("video/rnn/bias_hh_l0", model.videoBranch.rnn.bias_hh_l0, epoch)

            i = 0
            for conv in model.textBranch.convs1:
                tb.add_histogram("text/conv{}/weight".format(i), conv.weight, epoch)
                tb.add_histogram("text/conv{}/bias".format(i), conv.bias, epoch)
                i += 1

            i = 0
            for conv in model.videoBranch.convs1:
                tb.add_histogram("video/conv{}/weight".format(i), conv.weight, epoch)
                tb.add_histogram("video/conv{}/bias".format(i), conv.bias, epoch)
                i += 1

            tb.add_histogram("text/MFC/weight", model.textBranch.text_mapping.fc1.weight, epoch)
            tb.add_histogram("text/MFC/bias", model.textBranch.text_mapping.fc1.bias, epoch)

            tb.add_histogram("video/MFC/weight", model.videoBranch.visual_mapping.fc1.weight, epoch)
            tb.add_histogram("video/MFC/bias", model.videoBranch.visual_mapping.fc1.bias, epoch)

        # Train stage
        train_loss, n_iters = train_epoch(epoch, train_loader, model, train_loss_fn, optimizer, cuda, log_interval, logger, tb, n_iters)
        tb.add_scalar("Loss/Train", train_loss, epoch)
        logger.log_train_loss_epoch(epoch, train_loss)

        # # Validation stage
        val_loss = val_epoch(epoch, val_loader, model, val_loss_fn, cuda, log_interval, logger)
        tb.add_scalar("Loss/Validation", val_loss, epoch)
        logger.log_val_loss_epoch(epoch, val_loss)

        print('Epoch: {}/{}. Train set: Sum loss: {}'.format(epoch, n_epochs, "{0:.5f}".format(train_loss)))
        print('Epoch: {}/{}. Validation set: Sum loss: {}'.format(epoch, n_epochs, "{0:.5f}".format(val_loss)))

        print("Stepping:", end=' ')
        print(optimizer.param_groups[0]['lr'], end=' ')
        optimizer.param_groups[0]['lr'] *= 0.99
        print("-->", end=' ')
        print(optimizer.param_groups[0]['lr'])

        earlystopping(val_loss)
        if earlystopping.early_stop:
            break
        elif earlystopping.step:  # si cada 3 epoch no baja el loss, hacemos un step
            earlystopping.step = False
            earlystopping.step_counter = 0
            print("Stepping:", end=' ')
            print(optimizer.param_groups[0]['lr'], end=' ')
            optimizer.param_groups[0]['lr'] *= 0.5
            print("-->", end=' ')
            print(optimizer.param_groups[0]['lr'])
    logger.close()


def train_epoch(epoch, train_loader, model, loss_fn, optimizer, cuda, log_interval, logger, tb, n_iters):
    model.train()
    loss_fn.train()
    train_loss = []
    train_batch_loss = []
    index = 0
    for batch_idx, inputs in enumerate(train_loader):
        batch_size = inputs[0].size(0)
        if cuda:
            inputs = map(lambda x: x.to('cuda:0', non_blocking=True), inputs)
        y_video, y_text = model(*inputs)  # y_text, y_video
        optimizer.zero_grad()
        loss = loss_fn(y_video, y_text)
        train_batch_loss.append(loss.item())
        train_loss.append(loss.item())
        loss.backward()
        clip_grad_norm_(model.parameters(), 2.0)
        optimizer.step()
        index += batch_size
        if batch_idx % log_interval == 0:
            percent = 100. * index / len(train_loader.dataset)
            message = 'Train: [{} {}/{} ({}%)]\tLoss: {:.2f}'.format(
                epoch, index,
                len(train_loader.dataset),
                int(percent),
                np.average(train_batch_loss))
            if logger is not None:
                logger.log_train_loss(epoch, index, np.average(train_batch_loss))
            print(message)
            train_batch_loss = []

            n_iters += 1

            if tb is not None:
                if batch_size == train_loader.batch_size:
                    tb.add_scalar("Loss/Train Batch", loss.item(), n_iters)
                # tb.add_histogram("grad/text/embeddings", model.textBranch.embedding.weight.grad, n_iters)
                # tb.add_histogram("grad/text/rnn/weight_ih_l0", model.textBranch.rnn.weight_ih_l0.grad, n_iters)
                # tb.add_histogram("grad/text/rnn/weight_hh_l0", model.textBranch.rnn.weight_hh_l0.grad, n_iters)
                # tb.add_histogram("grad/text/rnn/bias_ih_l0", model.textBranch.rnn.bias_ih_l0.grad, n_iters)
                # tb.add_histogram("grad/text/rnn/bias_hh_l0", model.textBranch.rnn.bias_hh_l0.grad, n_iters)
                #
                # tb.add_histogram("grad/video/rnn/weight_ih_l0", model.videoBranch.rnn.weight_ih_l0.grad, n_iters)
                # tb.add_histogram("grad/video/rnn/weight_hh_l0", model.videoBranch.rnn.weight_hh_l0.grad, n_iters)
                # tb.add_histogram("grad/video/rnn/bias_ih_l0", model.videoBranch.rnn.bias_ih_l0.grad, n_iters)
                # tb.add_histogram("grad/video/rnn/bias_hh_l0", model.videoBranch.rnn.bias_hh_l0.grad, n_iters)
                #
                # i = 0
                # for conv in model.textBranch.convs1:
                #     tb.add_histogram("grad/text/conv{}/weight".format(i), conv.weight.grad, n_iters)
                #     tb.add_histogram("grad/text/conv{}/bias".format(i), conv.bias.grad, n_iters)
                #     i += 1
                #
                # i = 0
                # for conv in model.videoBranch.convs1:
                #     tb.add_histogram("grad/video/conv{}/weight".format(i), conv.weight.grad, n_iters)
                #     tb.add_histogram("grad/video/conv{}/bias".format(i), conv.bias.grad, n_iters)
                #     i += 1
                #
                # tb.add_histogram("grad/text/MFC/weight", model.textBranch.text_mapping.fc1.weight.grad, n_iters)
                # tb.add_histogram("grad/text/MFC/bias", model.textBranch.text_mapping.fc1.bias.grad, n_iters)
                #
                # tb.add_histogram("grad/video/MFC/weight", model.videoBranch.visual_mapping.fc1.weight.grad, n_iters)
                # tb.add_histogram("grad/video/MFC/bias", model.videoBranch.visual_mapping.fc1.bias.grad, n_iters)
    return np.sum(train_loss), n_iters


def train_ft_epoch(epoch, train_ft_loaders, model, loss_fn, optimizer, cuda, log_interval, logger):
    model.train()
    loss_fn.train()
    train_loss = []
    train_batch_loss = []
    idx_accumulative = 0
    total_dataset_len = sum([len(train_loader.dataset) for train_loader in train_ft_loaders])
    init_idx = randint(0, 1)
    for train_loader_idx in [init_idx, 1 - init_idx]:
        train_loader = train_ft_loaders[train_loader_idx]
        for batch_idx, inputs in enumerate(train_loader):
            idx_accumulative += train_loader.batch_size
            if cuda:
                inputs = map(lambda x: x.to('cuda:0'), inputs)
            s_out, v_out = model(*inputs)  # y_text, y_video
            optimizer.zero_grad()
            loss = loss_fn(s_out, v_out)
            train_batch_loss.append(loss.item())
            train_loss.append(loss.item())
            loss.backward()
            clip_grad_norm_(model.parameters(), 2.0)
            optimizer.step()
            if batch_idx % log_interval == 0:
                index = idx_accumulative
                percent = 100. * idx_accumulative / total_dataset_len
                message = 'Train: [{} {}/{} ({}%)]\tLoss: {:.2f}'.format(
                    epoch, index,
                    total_dataset_len,
                    int(percent),
                    np.average(train_batch_loss))
                logger.log_train_loss(epoch, index, np.average(train_batch_loss))
                print(message)
                train_batch_loss = []
    return np.sum(train_loss)


def val_epoch(epoch, val_loader, model, loss_fn, cuda, logger, tb):
    model.eval()
    loss_fn.eval()
    val_loss_sum = 0
    n_iters = 0
    with torch.no_grad():
        index = 0
        for batch_idx, inputs in enumerate(val_loader):
            batch_size = inputs[0].size(0)
            if cuda:
                inputs = [x.to('cuda:0') for x in inputs]
            s_out, v_out = model(*inputs)
            loss_outputs = loss_fn(s_out, v_out)
            loss = loss_outputs[0] if type(loss_outputs) in (tuple, list) else loss_outputs
            val_loss_sum += loss.item()
            index += batch_size
            percent = 100. * index / len(val_loader.dataset)
            message = 'Validation: [{} {}/{} ({}%)]\tLoss: {:.2f}'.format(
                epoch, index,
                len(val_loader.dataset),
                int(percent),
                loss.item())
            if logger is not None:
                logger.log_val_loss(epoch, index, loss.item())
            if batch_size == val_loader.batch_size:
                if tb is not None:
                    tb.add_scalar("Loss/Validation Batch", loss.item(), epoch*len(val_loader.dataset) + n_iters)
                n_iters += 1
            print(message)
    return val_loss_sum


def mir_epoch(epoch, val_loader, model, loss_fn, cuda, logger):
    model.eval()
    loss_fn.eval()
    y_texts = []
    y_videos = []
    with torch.no_grad():
        index = len(val_loader.dataset)
        for batch_idx, inputs in enumerate(val_loader):
            if cuda:
                inputs = [x.to('cuda:0') for x in inputs]
            y_video, y_text = model(*inputs)
            y_videos.append(y_video)
            y_texts.append(y_text)
        y_texts = torch.cat(y_texts)
        y_videos = torch.cat(y_videos)
        r1, r5, r10, medr, meanr = loss_fn(y_videos, y_texts)  # r1, r5, r10, medr, meanr
        total_loss = r1 + r5 + r10
        message = 'Validation epoch: {}\nR1 R5 R10: {:.3f} {:.3f} {:.3f}\nMedian Rank: {:.0f}\nMean Rank: {:.2f}\nTotal Loss: {:.3f}'.format(
            epoch, r1, r5, r10, medr, meanr, total_loss)
        if logger is not None:
            logger.log_val_loss_metrics(epoch, index, r1, r5, r10, medr, meanr)
        print(message)
    return total_loss
