from Model.datasets import MSRVTTBase, VTTBaseLMDB, VTTBaseOldLMDB
from torch.utils.data import DataLoader
import time

# Usin VTT2016_A as test dataset


def test_no_lmdb_dataset(dataset_path):
    dt = DataLoader(MSRVTTBase(dataset_path, for_lmdb=False))
    start_time = time.time()
    for index, data in enumerate(dt):
        a = 1
    finish = time.time() - start_time
    print("No LMDB time:", finish)


def test_lmdb_dataset(dataset_path):
    dt = DataLoader(VTTBaseOldLMDB(dataset_path, ""))
    start_time = time.time()
    for index, data in enumerate(dt):
        a = 1
    finish = time.time() - start_time
    print("LMDB time:", finish)


if __name__ == "__main__":
    vtt16 = "Model/data/VTT2016/vtt16_A_all_voc_dataset_ssd.csv"
    vtt16_lmdb = "Model/data/all_lmdb/VTT2016_A"

    msr = "Model/data/msr_tgif_msvd/base/msr_dataset_test.csv"
    msr_lmdb = "Model/data/old_lmdb/msr"
    test_lmdb_dataset(msr_lmdb)
    test_no_lmdb_dataset(msr)