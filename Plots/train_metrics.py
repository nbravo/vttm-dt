import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from sklearn.metrics import mean_absolute_error as mse
from itertools import combinations
from matplotlib.lines import Line2D

TRAIN_LOSS_PALLETE = [["#654321", "#e6372c", "#ffa600"],
                      ["#0517e3", "#7bc8a4", "#4cc3d9"]]
VAL_Y_LIMITS = [0.005, 0.02, 0.03, 0.05]
TRAIN_Y_LIMITS = [0, 140000]


def print_cross_mse(model, resnext, runs_loss):
    combs = combinations(list(range(len(runs_loss))), 2)
    print(model, resnext)
    for i, j in combs:
        min_length = min(len(runs_loss[i]), len(runs_loss[j]))
        print("MAE", i+1, j+1, mse(runs_loss[i][:min_length], runs_loss[j][:min_length]))


def get_train_loss(log_file):
    dt = pd.read_csv(log_file, decimal=",", header=0, names=['epoch', 'loss'])
    dt['loss'] = dt['loss'].astype(np.float)
    return dt['epoch'].to_numpy(), dt['loss'].to_numpy().tolist()


def get_val_loss(log_file):
    dt = pd.read_csv(log_file, decimal=",", header=0, names=['epoch','index','r1','r5','r10','medr','meanr'])
    dt['r1'] = dt['r1'].astype(np.float)
    dt['r5'] = dt['r5'].astype(np.float)
    dt['r10'] = dt['r10'].astype(np.float)
    return (dt['epoch'].to_numpy(), dt['r1'].to_numpy().tolist(),
            dt['r5'].to_numpy().tolist(), dt['r10'].to_numpy().tolist())


def plot_train_loss(models, save=True):
    xticks_limit = 0
    y_min = 5000000
    y_max = 0
    fig, ax = plt.subplots(figsize=(6, 3.5))
    variation = ['With TGIF', 'No TGIF']
    for index, model in enumerate(models):
        runs_loss = []
        for i in range(3):
            epoch, loss = get_train_loss("Model/log/{}/run{}/train_loss_epoch.csv".format(model, i+1))
            if min(loss) < y_min:
                y_min = min(loss)
            if max(loss) > y_max:
                y_max = max(loss)
            if epoch[-1] > xticks_limit:
                xticks_limit = epoch[-1]
            runs_loss.append(loss)
            ax.plot(epoch, loss, label="{} {}".format(variation[index], i+1), color=TRAIN_LOSS_PALLETE[index][i],
                    linestyle='-', marker='.', linewidth=1, markersize=5)
    #print_cross_mse(model, runs_loss)
    ax.set_ylim([0, 140000])
    xticks = list(range(0, int(xticks_limit + xticks_limit // 8), xticks_limit // 8))
    ax.set(xlabel="Epoch", ylabel="Total Margin Ranking Loss", xticks=xticks)
    ax.grid()
    ax.legend()
    if not save:
        plt.show()
    else:
        plt.tight_layout()
        plt.savefig("Plots/train_loss/{}_train_loss.svg".format('only_dt_no_tgif_comparison'))


def plot_train_loss_no_resnext(model, save=True):
    xticks_limit = 0
    y_min = 5000000
    y_max = 0
    fig, ax = plt.subplots(figsize=(6, 3.5))
    runs_loss = []
    for i in range(3):
        epoch, loss = get_train_loss("logs/{}/run{}/train_loss_epoch.csv".format(model, i+1))
        if min(loss) < y_min:
            y_min = min(loss)
        if max(loss) > y_max:
            y_max = max(loss)
        if epoch[-1] > xticks_limit:
            xticks_limit = epoch[-1]
        runs_loss.append(loss)
        ax.plot(epoch, loss, label="run {}".format(i+1), color=TRAIN_LOSS_PALLETE[0][i],
                linestyle='-', marker='.', linewidth=1, markersize=5)
    print_cross_mse(model, "", runs_loss)
    xticks = list(range(0, int(xticks_limit + xticks_limit // 8), xticks_limit // 8))
    ax.set(xlabel="Epoch", ylabel="Total Margin Ranking Loss", xticks=xticks, ylim=TRAIN_Y_LIMITS)
    ax.grid()
    ax.legend()
    if not save:
        plt.show()
    else:
        plt.tight_layout()
        plt.savefig("train_loss/{}_train_loss.svg".format(model))


def plot_val_metrics(model, save=False):
    fig, axs = plt.subplots(2, 2)
    xticks_limit = 0
    for index, resnext in enumerate(['mediamill', 'mxnet']):
        for i in range(3):
            epoch, r1, r5, r10 = get_val_loss("Model/log/dong_2018_{}_{}/run{}/val_loss.csv".format(resnext, model, i+1))
            if epoch[-1] > xticks_limit:
                xticks_limit = epoch[-1]
            sum_ranks = [0]*len(epoch)
            for k in range(len(epoch)):
                sum_ranks[k] = r1[k] + r5[k] + r10[k]
            r1 = np.divide(r1, 100)
            r5 = np.divide(r5, 100)
            r10 = np.divide(r10, 100)
            sum_ranks = np.divide(sum_ranks, 100)
            axs[0, 0].plot(epoch, r1, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
            axs[0, 1].plot(epoch, r5, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', markersize=4)
            axs[1, 0].plot(epoch, r10,  color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
            axs[1, 1].plot(epoch, sum_ranks, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
    y_labels = ["RANK@1", "RANK@5", "RANK@10", "Sum of Ranks"]
    for index, ax in enumerate(axs.flatten()):
        # ax.set_xticks(range(0, int(ax.get_xticks()[-1]), 2))
        ax.set_xticks(list(range(0, int(xticks_limit + xticks_limit // 8), xticks_limit // 8)))
        ax.set_ylim((0, VAL_Y_LIMITS[index]))
        ax.set_ylabel(y_labels[index])
        ax.grid()

    axs[1, 0].set_xlabel("Epoch")
    axs[1, 1].set_xlabel("Epoch")
    custom_lines = [Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][0], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][0], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][1], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][1], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][2], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][2], lw=1, marker='.')]
    # plt.legend(custom_lines, ['Cosine 1', 'L2 1', 'Cosine 2', 'L2 2', 'Cosine 3', 'L2 3'],
    #                 loc='upper left', bbox_to_anchor=(0.2, -0.5), ncol=3)
    if not save:
        plt.show()
    else:
        plt.tight_layout()
        plt.savefig("val_loss/{}_l2_val_loss.svg".format(model), transparent=True)


def plot_val_metrics_no_resnext(model, save=False):
    fig, axs = plt.subplots(2, 2)
    xticks_limit = 0
    for i in range(3):
        epoch, r1, r5, r10 = get_val_loss("logs/{}/run{}/val_loss.csv".format(model, i+1))
        if epoch[-1] > xticks_limit:
            xticks_limit = epoch[-1]
        sum_ranks = [0]*len(epoch)
        for k in range(len(epoch)):
            sum_ranks[k] = r1[k] + r5[k] + r10[k]
        r1 = np.divide(r1, 100)
        r5 = np.divide(r5, 100)
        r10 = np.divide(r10, 100)
        sum_ranks = np.divide(sum_ranks, 100)
        axs[0, 0].plot(epoch, r1, label="run {}".format(i+1), color=TRAIN_LOSS_PALLETE[0][i],
                       linestyle='-', marker='.', linewidth=1, markersize=4)
        axs[0, 1].plot(epoch, r5, label="run {}".format(i+1), color=TRAIN_LOSS_PALLETE[0][i],
                       linestyle='-', marker='.', linewidth=1, markersize=4)
        axs[1, 0].plot(epoch, r10, label="run {}".format(i+1), color=TRAIN_LOSS_PALLETE[0][i],
                       linestyle='-', marker='.', linewidth=1, markersize=4)
        axs[1, 1].plot(epoch, sum_ranks, label="run {}".format(i+1), color=TRAIN_LOSS_PALLETE[0][i],
                       linestyle='-', marker='.', linewidth=1, markersize=4)
    y_labels = ["RANK@1", "RANK@5", "RANK@10", "Sum of Ranks"]
    for index, ax in enumerate(axs.flatten()):
        # ax.set_xticks(range(0, int(ax.get_xticks()[-1]), 2))
        ax.set_xticks(list(range(0, int(xticks_limit + xticks_limit // 8), xticks_limit // 8)))
        ax.set_ylim((0, VAL_Y_LIMITS[index]))
        ax.set_ylabel(y_labels[index])
        ax.grid()

    axs[1, 0].set_xlabel("Epoch")
    axs[1, 1].set_xlabel("Epoch")
    custom_lines = [Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][0], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][1], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][2], lw=1, marker='.')]
    plt.legend(custom_lines, ['run 1', 'run 2', 'run 3'],
                     loc='upper left', bbox_to_anchor=(0.2, -0.5), ncol=3)
    if not save:
        plt.show()
    else:
        plt.tight_layout()
        plt.savefig("val_loss/{}_val_loss.svg".format(model), transparent=True)

def check_min_max_val_loss():
    models_dirs = os.listdir("logs")
    min_r1 = 1e30
    min_r5 = 1e30
    min_r10 = 1e30
    min_sums = 1e30
    max_r1 = 0
    max_r5 = 0
    max_r10 = 0
    max_sums = 0

    for m in models_dirs:
        for r in os.listdir("logs/{}/".format(m)):
            val_data = pd.read_csv("logs/{}/{}/val_loss.csv".format(m, r),
                                   delimiter=",", header=0, names=['epoch', 'index', 'r1', 'r5', 'r10', 'medr', 'meanr'])
            r1 = val_data['r1'].astype(np.float)
            r5 = val_data['r5'].astype(np.float)
            r10 = val_data['r10'].astype(np.float)
            sum_ranks = [0]*len(r1)
            for k in range(len(r1)):
                sum_ranks[k] = r1[k] + r5[k] + r10[k]
            min_r1 = min(r1) if min(r1) < min_r1 else min_r1
            min_r5 = min(r5) if min(r5) < min_r5 else min_r5
            min_r10 = min(r10) if min(r10) < min_r10 else min_r10
            min_sums= min(sum_ranks) if min(sum_ranks) < min_sums else min_sums
            max_r1 = max(r1) if max(r1) > max_r1 else max_r1
            max_r5 = max(r5) if max(r5) > max_r5 else max_r5
            max_r10 = max(r10) if max(r10) > max_r10 else max_r10
            max_sums = max(sum_ranks) if max(sum_ranks) > max_sums else max_sums
    print("R1 min:", min_r1, "max:", max_r1)
    print("R5 min:", min_r5, "max:", max_r5)
    print("R10 min:", min_r10, "max:", max_r10)
    print("Sums min:", min_sums, "max:", max_sums)


def check_min_max_train_loss():
    models_dirs = os.listdir("logs")
    min_loss = 1e30
    max_loss = 0

    for m in models_dirs:
        for r in os.listdir("logs/{}/".format(m)):
            train_data = pd.read_csv("logs/{}/{}/train_loss_epoch.csv".format(m, r),
                                   delimiter=",", header=0, names=['epoch', 'loss'])
            loss = train_data['loss'].astype(np.float)
            min_loss = min(loss) if min(loss) < min_loss else min_loss
            max_loss = max(loss) if max(loss) > max_loss else max_loss
    print("MIR min:", min_loss, "max:", max_loss)


def plot_val_metrics_two_models(models, save=False):
    fig, axs = plt.subplots(2, 2)
    xticks_limit = 0
    for index, model in enumerate(models):
        for i, run in enumerate(os.listdir('Model/log/dong_2018_{}'.format(model))):
            epoch, r1, r5, r10 = get_val_loss("Model/log/dong_2018_{}/{}/val_loss.csv".format(model, run))
            if epoch[-1] > xticks_limit:
                xticks_limit = epoch[-1]
            sum_ranks = [0]*len(epoch)
            for k in range(len(epoch)):
                sum_ranks[k] = r1[k] + r5[k] + r10[k]
            r1 = np.divide(r1, 100)
            r5 = np.divide(r5, 100)
            r10 = np.divide(r10, 100)
            sum_ranks = np.divide(sum_ranks, 100)
            axs[0, 0].plot(epoch, r1, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
            axs[0, 1].plot(epoch, r5, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', markersize=4)
            axs[1, 0].plot(epoch, r10,  color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
            axs[1, 1].plot(epoch, sum_ranks, color=TRAIN_LOSS_PALLETE[index][i],
                           linestyle='-', marker='.', linewidth=1, markersize=4)
    y_labels = ["RANK@1", "RANK@5", "RANK@10", "Sum of Ranks"]
    for index, ax in enumerate(axs.flatten()):
        # ax.set_xticks(range(0, int(ax.get_xticks()[-1]), 2))
        ax.set_xticks(list(range(0, int(xticks_limit + xticks_limit // 8), xticks_limit // 8)))
        ax.set_ylim((0, VAL_Y_LIMITS[index]))
        ax.set_ylabel(y_labels[index])
        ax.grid()

    axs[1, 0].set_xlabel("Epoch")
    axs[1, 1].set_xlabel("Epoch")
    custom_lines = [Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][0], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][0], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][1], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][1], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[0][2], lw=1, marker='.'),
                    Line2D([0], [0], color=TRAIN_LOSS_PALLETE[1][2], lw=1, marker='.')]
    # plt.legend(custom_lines, ['Cosine 1', 'L2 1', 'Cosine 2', 'L2 2', 'Cosine 3', 'L2 3'],
    #                 loc='upper left', bbox_to_anchor=(0.2, -0.5), ncol=3)
    if not save:
        plt.show()
    else:
        plt.tight_layout()
        plt.savefig("Plots/val_loss/{}_val_loss.svg".format('_'.join(models)), transparent=True)


if __name__ == "__main__":
    # for m in ["dong_2018_base", "dong_2018_all_dt", "dong_2018_all_infersent", "dong_2018_only_infersent"]:
    plot_val_metrics_two_models(["only_dt", "only_dt_no_tgif"], save=True)
    #plot_train_loss(['dong_2018_only_dt', 'dong_2018_only_dt_no_tgif'], save=True)

