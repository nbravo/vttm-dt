import matplotlib.pyplot as plt
import numpy as np

import math

def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor


def plot_mir(save=False):
    names = ("A", "B", "C", "D", "E")
    width = 0.4
    y_pos = np.arange(len(names))
    # base
    # mediamill = ([0.433, 0.451, 0.439], [0.432, 0.439, 0.433], [0.429, 0.433, 0.423], [0.428, 0.432, 0.417], [0.449, 0.448, 0.429])
    # mxnet = ([0.186, 0.172, 0.169], [0.188, 0.180, 0.174], [0.184, 0.181, 0.172], [0.166, 0.171, 0.166], [0.174, 0.165,0.174])
    # # # # # inft_all
    # # mediamill = ([0.441, 0.441, 0.445], [0.443, 0.432, 0.421], [0.432, 0.435, 0.415], [0.430, 0.441, 0.424], [0.445, 0.438, 0.436])
    # # mxnet = ([0.169, 0.168, 0.171], [0.165, 0.176, 0.166], [0.169, 0.171, 0.175], [0.162, 0.164, 0.169], [0.165, 0.169, 0.168])
    # # # # # # ift_only
    # # mediamill = ([0.004, 0.004, 0.005], [0.004, 0.005, 0.005], [0.004, 0.004, 0.006], [0.003, 0.004, 0.005], [0.003, 0.004, 0.006])
    # # mxnet = ([0.003, 0.004, 0.004], [0.003, 0.004, 0.005], [0.004, 0.004, 0.003], [0.004, 0.004, 0.003], [0.005, 0.003, 0.004])
    # # # # # # dt_all
    # mediamill = ([0.289, 0.289, 0.293], [0.275, 0.276, 0.289], [0.283, 0.282, 0.294], [0.272, 0.278, 0.285], [0.291, 0.286, 0.303])
    # mxnet = ([0.075, 0.070, 0.086], [0.076, 0.073, 0.086], [0.081, 0.074, 0.085], [0.075, 0.072, 0.078], [0.0782, 0.075, 0.081])
    # # # # dt_only
    mediamill = ([0.012, 0.013, 0.011], [0.012, 0.012, 0.011], [0.013, 0.010, 0.013], [0.011, 0.011, 0.010], [0.011, 0.008, 0.011])
    plt.rcdefaults()
    fig, ax = plt.subplots()
    means = [np.mean(scores) for scores in mediamill]
    stds = [np.std(scores) for scores in mediamill]
    # means_2 = [np.mean(scores) for scores in mxnet]
    # stds_2 = [np.std(scores) for scores in mxnet]
    ax.barh(y_pos, means, width, xerr=stds, error_kw=dict(ecolor="#003f5c", lw=1, capsize=4, capthick=2),
            align="center", color="#A0CA9D",)
    for i, v in enumerate(means):
        ax.text(v + 0.013, i + .07, '{:.3f}'.format(truncate(v, decimals=3)), color='black', fontsize=8)

    # ax.barh(y_pos + width, means_2, width, xerr=stds_2, error_kw=dict(ecolor="#003f5c", lw=1, capsize=4, capthick=2),
    #         align="center", color="#4cc3d9")
    # for i, v in enumerate(means_2):
    #     ax.text(v + 0.012, i + 0.45, '{:.3f}'.format(truncate(v, decimals=3)), color='black', fontsize=8)

    ax.set(yticks=y_pos + width/2, yticklabels=names, ylim=[2*width - (1 + width), len(names)], xlim=(0, 0.5))
    ax.tick_params(axis='y', length=0)
    ax.invert_yaxis()
    #ax.legend(["Shufflenet", "MXnet"])
    ax.set_ylabel('TRECVID 2018 Set')
    ax.set_xlabel('Mean Inverted Rank')
    if not save:
        plt.show()
    if save:
        plt.tight_layout()
        plt.savefig("test_mir/dong_2018_dt_only.svg", transparent=True)


if __name__ == '__main__':
    plot_mir(save=True)
