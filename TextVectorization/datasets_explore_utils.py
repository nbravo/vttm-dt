import json


def get_msr_video_sentences(video_id):
    video_id = 'video' + str(video_id)
    msr = json.load(open('/home/nbravo/data/MSR-VTT/train_val_videodatainfo.json', 'r'))
    video_sentences = []
    for sentence in msr['sentences']:
        if sentence['video_id'] == video_id:
            video_sentences.append(sentence['caption'])
    return '\n'.join(video_sentences)


if __name__ == '__main__':
    print(get_msr_video_sentences(3098))
