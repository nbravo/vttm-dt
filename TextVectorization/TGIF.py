import TextVectorization.Flickr30m as flickr30m
from TextVectorization.bigfile import BigFile
import numpy as np
import pickle
from nltk.corpus import stopwords
import re

SENTENCE_PATH = "/run/media/nbravo/nbravo-2TB/TGIF-Release-master/data/tgif-v1.0.tsv"
SAVE_PATH = "/run/media/nbravo/Elements/TGIF/sentences/"

BOW_PATH = SAVE_PATH + "bow/"
W2V_PATH = SAVE_PATH + "w2v/"
GRU_PATH = SAVE_PATH + "gru_padded/"
LEN_PATH = SAVE_PATH + "len/"

dataset = "tgif"
COUNT_PATH = "objects/{}/count.pkl".format(dataset)
TGIF_VOC = "objects/{}/voc.pkl".format(dataset)
TGIF_IDX = "objects/{}/index.pkl".format(dataset)
TGIF_VECTORS = "objects/{}/vectors.pkl".format(dataset)

TGIF_VOC_RNN = "objects/{}/rnn/voc_sw.pkl".format(dataset)
TGIF_IDX_RNN = "objects/{}/rnn/index_sw.pkl".format(dataset)
TGIF_VECTORS_RNN = "objects/{}/rnn/vectors_sw.pkl".format(dataset)


def count_words(words, vocabulary, count):
    for word in words:
        try:
            index = vocabulary.index(word)
            count[index] += 1
        except ValueError:
            continue


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def complete_count():
    print("Loading Flickr30m vectors...")
    voc = flickr30m.get_vocabulary()
    count = np.zeros(len(voc))

    print("Loading dataset...")
    i = 0
    with open(SENTENCE_PATH, 'r') as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            count_words(normalized_words, voc, count)
            i += 1
            if i % 10000 == 0:
                print(i, "ready")
                pickle.dump(count, open(COUNT_PATH, 'wb'))
            line = fin.readline()


def create_vocabulary(threshold=5):
    count = pickle.load(open(COUNT_PATH, 'rb'))
    all_voc = np.array(flickr30m.get_vocabulary())
    all_vectors = BigFile('vec500flickr30m')

    msr_voc = all_voc[count >= threshold]
    pickle.dump(msr_voc, open(TGIF_VOC, 'wb'))

    msr_word2idx = dict()
    msr_vectors = []
    for i in range(len(msr_voc)):
        msr_word2idx[msr_voc[i]] = i
        msr_vectors.append(all_vectors.read([msr_voc[i]])[1][0])
        if i % 1000 == 0:
            print(i, "of", len(msr_voc), "ready")
    pickle.dump(msr_word2idx, open(TGIF_IDX, 'wb'))
    pickle.dump(np.array(msr_vectors), open(TGIF_VECTORS, 'wb'))


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue
    return vector / total_words


def generate_voc_for_rnn(ndims=500):
    msr_voc = list(pickle.load(open(TGIF_VOC, 'rb')))
    msr_index = pickle.load(open(TGIF_IDX, 'rb'))
    msr_vectors = list(pickle.load(open(TGIF_VECTORS, 'rb')))
    special_keys = ['<start>', '<end>', '<pad>', '<unk>']
    special_vectors = [np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims),
                      np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims)]
    special_index = {word: index for index, word in enumerate(special_keys)}
    msr_index = {key: val + len(special_keys) for key, val in msr_index.items()}
    msr_voc_rnn = np.array(special_keys + msr_voc)
    msr_index_rnn = dict(msr_index, **special_index)
    msr_vectors_rnn = np.array(special_vectors + msr_vectors)
    pickle.dump(msr_voc_rnn, open(TGIF_VOC_RNN, 'wb'))
    pickle.dump(msr_index_rnn, open(TGIF_IDX_RNN, 'wb'))
    pickle.dump(msr_vectors_rnn, open(TGIF_VECTORS_RNN, 'wb'))


if __name__ == '__main__':
    create_vocabulary()