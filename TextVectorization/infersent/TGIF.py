import torch
import pickle
from TextVectorization.infersent.Model import InferSent

if __name__ == '__main__':
    model_version = 2
    MODEL_PATH = "encoder/infersent%s.pkl" % model_version
    params_model = {'bsize': 128, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': model_version}
    model = InferSent(params_model)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()
    model = model.cuda()
    W2V_PATH = 'vectors/crawl-300d-2M.vec'
    model.set_w2v_path(W2V_PATH)
    model.build_vocab_k_words(K=100000)

    TGIF_FILE = "/run/media/nbravo/nbravo-2TB/TGIF-Release-master/data/tgif-v1.0.tsv"
    INFERSENT_PATH = "/run/media/nbravo/Elements/TGIF/base/infersent/"


    i = 0
    with open(TGIF_FILE) as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            save_id = "sentence" + str(i)
            embedding = model.encode(sentence)[0]
            pickle.dump(embedding, open(INFERSENT_PATH + save_id + ".pkl", 'wb'))
            if i % 1000 == 0:
                print(i, "ready")
            i += 1
            line = fin.readline()

