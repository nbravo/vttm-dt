import torch
import pickle
from TextVectorization.infersent.Model import InferSent
import codecs

if __name__ == '__main__':
    model_version = 2
    MODEL_PATH = "encoder/infersent%s.pkl" % model_version
    params_model = {'bsize': 128, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': model_version}
    model = InferSent(params_model)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()
    model = model.cuda()
    W2V_PATH = 'vectors/crawl-300d-2M.vec'
    model.set_w2v_path(W2V_PATH)
    model.build_vocab_k_words(K=100000)

    SET = "2"
    SUBSET = ["A", "B"]
    DATASET_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017_INFO/"
    DATASET_FILE = DATASET_PATH + \
                   "vtt.testing.data/matching.ranking.subtask.testing.data/testing.{}.subsets/tv17.vtt.descriptions.{}"
    INFERSENT_PATH = "/run/media/nbravo/Elements/VTT2017/infersent/subset_{}/{}/"
    print("Loading dataset...")
    for ss in SUBSET:
        with open(DATASET_FILE.format(SET, ss), 'r') as fin:
            line = fin.readline()
            while line:
                index, sentence = line.split(" ", 1)
                print(index)
                save_id = "sentence" + str(index) + ".pkl"
                embedding = model.encode(sentence.lower())[0]
                pickle.dump(embedding, open(INFERSENT_PATH.format(SET, ss) + save_id, 'wb'))
                line = fin.readline()

