import torch
import json
import pickle
from TextVectorization.infersent.Model import InferSent

if __name__ == '__main__':
    model_version = 2
    MODEL_PATH = "encoder/infersent%s.pkl" % model_version
    params_model = {'bsize': 128, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': model_version}
    model = InferSent(params_model)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()
    model = model.cuda()
    W2V_PATH = 'vectors/crawl-300d-2M.vec'
    model.set_w2v_path(W2V_PATH)
    model.build_vocab_k_words(K=100000)

    DATASET_FILE = "/home/nbravo/data/MSR-VTT/train_val_videodatainfo.json"
    INFERSENT_PATH = "/run/media/nbravo/Elements/MSR_all/infersent/"
    with open(DATASET_FILE, 'r') as fin:
        msr = json.load(fin)

    i = 0
    total = len(msr['sentences'])
    for data in msr['sentences']:
        sentence = data['caption'].lower()
        sentence_id = data['sen_id']
        video_id = data['video_id']
        save_id = "sentence" + str(sentence_id) + "_" + video_id
        embedding = model.encode(sentence)[0]
        pickle.dump(embedding, open(INFERSENT_PATH + save_id + ".pkl", 'wb'))
        if i % 1000 == 0:
            print(i, "of", total, "ready")
        i += 1
