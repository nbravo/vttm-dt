import torch
import pandas as pd
import pickle
from TextVectorization.infersent.Model import InferSent

if __name__ == '__main__':
    model_version = 2
    MODEL_PATH = "encoder/infersent%s.pkl" % model_version
    params_model = {'bsize': 128, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': model_version}
    model = InferSent(params_model)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()
    model = model.cuda()
    W2V_PATH = 'vectors/crawl-300d-2M.vec'
    model.set_w2v_path(W2V_PATH)
    model.build_vocab_k_words(K=100000)

    DATASET_FILE = "/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv"
    INFERSENT_PATH = "/run/media/nbravo/nbravo-2TB/MSVD/sentences/infersent/"

    data = pd.read_csv(DATASET_FILE)
    total = len(data['Description'])
    i = 0
    video_sentence_counter = dict()
    for row in data.iterrows():
        sentence = row[1]['Description']
        video_id = row[1]['VideoID']
        if video_id not in video_sentence_counter.keys():
            video_sentence_counter[video_id] = 0
        save_id = "sentence" + str(video_sentence_counter[video_id]) + "_" + video_id
        video_sentence_counter[video_id] += 1
        embedding = model.encode(sentence)[0]
        pickle.dump(embedding, open(INFERSENT_PATH + save_id + ".pkl", 'wb'))
        if i % 1000 == 0:
            print(i, "of", total, "ready")
        i += 1
