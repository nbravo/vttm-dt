import pickle
import string
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
import matplotlib.ticker as mtick
import json
import pandas as pd

MSR_FILE = '/home/nbravo/data/MSR-VTT/train_val_videodatainfo.json'
TGIF_FILE = '/run/media/nbravo/nbravo-2TB/TGIF/tgif-v1.0.tsv'
MSVD_FILE = '/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv'
VTT_2016_FILE = '/run/media/nbravo/Elements/VTT2016/vines.textDescription.A.testingSet'
VTT_2018_FOLDER = '/run/media/nbravo/nbravo-2TB/VTT2018/INFO/matching.ranking.eval.readme/tv18.vtt.descriptions.{}'
VTT_2018_SETS = ['A', 'B', 'C', 'D', 'E']


def create_training_vocab():
    vocab = {}
    sws = stopwords.words()

    #MSR
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)
        n = len(msr['sentences'])
        i = 0
        for data in msr['sentences']:
            sentence = data['caption']
            words = sentence.lower().translate(str.maketrans('', '', string.punctuation)).strip().split(' ')
            for w in words:
                if w in sws:
                    continue
                if w in vocab.keys():
                    vocab[w] += 1
                else:
                    vocab[w] = 1
            i += 1
            print('MSR', i, 'of', n)

    #TGIF
    with open(TGIF_FILE, 'r') as fin:
        i = 0
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            words = sentence.lower().translate(str.maketrans('', '', string.punctuation)).strip().split(' ')
            for w in words:
                if w in sws:
                    continue
                if w in vocab.keys():
                    vocab[w] += 1
                else:
                    vocab[w] = 1
            i += 1
            print('TGIF', i)
            line = fin.readline()

    #MSVD
    data = pd.read_csv(MSVD_FILE)
    i = 0
    n = len(data['Description'])
    for sentence in data['Description'][:]:
        words = sentence.lower().translate(str.maketrans('', '', string.punctuation)).strip().split(' ')
        for w in words:
            if w in sws:
                continue
            if w in vocab.keys():
                vocab[w] += 1
            else:
                vocab[w] = 1
        i += 1
        print('MSVD', i, 'of', n)
    pickle.dump(vocab, open('train_vocab.pkl', 'wb'))
    return vocab

def create_2016_vocab():
    vocab = {}
    sws = stopwords.words()
    with open(VTT_2016_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            id, sentence = line.strip().split(' ', 1)
            words = sentence.lower().translate(str.maketrans('', '', string.punctuation)).strip().split(' ')
            for w in words:
                if w in sws:
                    continue
                if w in vocab.keys():
                    vocab[w] += 1
                else:
                    vocab[w] = 1
            line = fin.readline()
    pickle.dump(vocab, open('vtt2016_vocab.pkl', 'wb'))
    return vocab


def create_2018_vocab():
    vocab = {}
    sws = stopwords.words()
    for s in VTT_2018_SETS:
        with open(VTT_2018_FOLDER.format(s), 'r') as fin:
            line = fin.readline()
            while line:
                id, sentence = line.strip().split(' ', 1)
                words = sentence.lower().translate(str.maketrans('', '', string.punctuation)).strip().split(' ')
                for w in words:
                    if w in sws:
                        continue
                    if w in vocab.keys():
                        vocab[w] += 1
                    else:
                        vocab[w] = 1
                line = fin.readline()
    pickle.dump(vocab, open('vtt2018_vocab.pkl', 'wb'))
    return vocab


def same_sentences():
    vtt2016_sentences = []
    vtt2018_sentences = []

    with open(VTT_2016_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            id, sentence = line.strip().split(' ', 1)
            vtt2016_sentences.append(sentence.lower())
            line = fin.readline()

    for s in VTT_2018_SETS:
        with open(VTT_2018_FOLDER.format(s), 'r') as fin:
            line = fin.readline()
            while line:
                id, sentence = line.strip().split(' ', 1)
                vtt2018_sentences.append(sentence.lower())
                line = fin.readline()

    for s in vtt2016_sentences:
        if s in vtt2018_sentences:
            print(s)


def plot_vtt_16_18_diff(n):
    vtt2016 = pickle.load(open('vtt2016_vocab.pkl', 'rb'))
    vtt2018 = pickle.load(open('vtt2018_vocab.pkl', 'rb'))
    vtt2016_words = []
    vtt2018_words = []

    words_index = dict()
    for word in vtt2018.keys():
        if vtt2018[word] > 50:
            vtt2018_words.append((word, vtt2018[word]))
    vtt2018_words = sorted(vtt2018_words, key=lambda x: x[1], reverse=True)
    print('VTT 2018 Top Words:', '\n'.join([w for w, v in vtt2018_words][:n]))
    index = 1
    for w, v in vtt2018_words:
        words_index[w] = index
        index += 1

    vtt2018_index = [i for i in range(1, index)]
    vtt2018_values = [v for w, v in vtt2018_words]
    vtt2018_total = sum(vtt2018_values)
    vtt2018_values = [100 * v / vtt2018_total for v in vtt2018_values]

    for word in words_index.keys():
        if word not in vtt2016.keys():
            vtt2016_words.append((words_index[word], word, 0))
        else:
            vtt2016_words.append((words_index[word], word, vtt2016[word]))
    print('VTT 2016 Top Words:', '\n'.join([w for i, w, v in list(sorted(vtt2016_words, key=lambda x: x[2]))][:n]))
    vtt2016_index = [i for i, w, v in vtt2016_words]
    vtt2016_values = [v for i, w, v in vtt2016_words]
    vtt2016_total = sum(vtt2016_values)
    vtt2016_values = [100 * v / vtt2016_total for v in vtt2016_values]

    fmt = '%.0f%%'
    fig, axs = plt.subplots(2, figsize=(4,5))
    axs[0].yaxis.set_major_formatter(mtick.FormatStrFormatter(fmt))
    axs[0].bar(vtt2018_index, vtt2018_values)
    axs[0].set_xlabel('VTT 2018 word index')
    axs[0].set_ylabel('VTT 2018 Percentage')
    axs[0].set_ylim((0, 6))
    axs[0].legend(['VTT 2018'])
    axs[1].bar(vtt2016_index, vtt2016_values, color='orange')
    axs[1].set_xlabel('VTT 2018 word index')
    axs[1].set_ylabel('VTT 2016 Percentage')
    axs[1].set_ylim((0, 6))
    axs[1].legend(['VTT 2016 set A'])
    axs[1].yaxis.set_major_formatter(mtick.FormatStrFormatter(fmt))
    plt.savefig('vocabulary_16_18.svg', format='svg')
    plt.show()


def plot_train_vtt_18_diff(n):
    vtt2016 = pickle.load(open('train_vocab.pkl', 'rb'))
    vtt2018 = pickle.load(open('vtt2018_vocab.pkl', 'rb'))
    vtt2016_words = []
    vtt2018_words = []

    words_index = dict()
    for word in vtt2018.keys():
        if vtt2018[word] > 50:
            vtt2018_words.append((word, vtt2018[word]))
    vtt2018_words = sorted(vtt2018_words, key=lambda x: x[1], reverse=True)
    print('VTT 2018 Top Words:', '\n'.join([w for w, v in vtt2018_words][:n]))
    index = 1
    for w, v in vtt2018_words:
        words_index[w] = index
        index += 1

    vtt2018_index = [i for i in range(1, index)]
    vtt2018_values = [v for w, v in vtt2018_words]
    vtt2018_total = sum(vtt2018_values)
    vtt2018_values = [100 * v / vtt2018_total for v in vtt2018_values]

    for word in words_index.keys():
        if word not in vtt2016.keys():
            vtt2016_words.append((words_index[word], word, 0))
        else:
            vtt2016_words.append((words_index[word], word, vtt2016[word]))
    print('Training Top Words:', '\n'.join([w for i, w, v in list(sorted(vtt2016_words, key=lambda x: x[2]))][:n]))
    vtt2016_index = [i for i, w, v in vtt2016_words]
    vtt2016_values = [v for i, w, v in vtt2016_words]
    vtt2016_total = sum(vtt2016_values)
    vtt2016_values = [100 * v / vtt2016_total for v in vtt2016_values]

    fmt = '%.0f%%'
    fig, axs = plt.subplots(2, figsize=(4,5))
    axs[0].yaxis.set_major_formatter(mtick.FormatStrFormatter(fmt))
    axs[0].bar(vtt2018_index, vtt2018_values)
    axs[0].set_xlabel('VTT 2018 word index')
    axs[0].set_ylabel('VTT 2018 Percentage')
    axs[0].set_ylim((0, 6))
    axs[0].legend(['VTT 2018'])
    axs[1].bar(vtt2016_index, vtt2016_values, color='orange')
    axs[1].set_xlabel('VTT 2018 word index')
    axs[1].set_ylabel('Training data Percentage')
    axs[1].set_ylim((0, 6))
    axs[1].legend(['Training data'])
    axs[1].yaxis.set_major_formatter(mtick.FormatStrFormatter(fmt))
    plt.savefig('vocabulary_train_18.svg', format='svg')
    plt.show()


if __name__ == '__main__':
    #create_training_vocab()
    plot_vtt_16_18_diff(10)