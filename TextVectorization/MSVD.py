from sys import argv
import TextVectorization.Flickr30m as flickr30m
import pandas as pd
import numpy as np
import pickle
from TextVectorization.bigfile import BigFile
from nltk.corpus import stopwords
import re

np.seterr(over='raise')

dataset = 'msvd'

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/MSVD/"
DATASET_FILE = "/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv"
COUNT_PATH = "objects/{}/msr_count.pkl".format(dataset)
MSR_VOC = "objects/{}/msr_voc.pkl".format(dataset)
MSR_IDX = "objects/{}/msr_index.pkl".format(dataset)
MSR_VECTORS = "objects/{}/msr_vectors.pkl".format(dataset)
MSR_VECTORS_PADDED = "objects/{}/msr_vectors_padded.pkl".format(dataset)
MSR_VOC_RNN = "objects/{}/rnn/msr_voc.pkl".format(dataset)
MSR_IDX_RNN = "objects/{}/rnn/msr_index.pkl".format(dataset)
MSR_VECTORS_RNN = "objects/{}/rnn/msr_vectors.pkl".format(dataset)
MSR_VECTORS_SW_PADDED = "objects/{}/gru/msr_vectors_sw_padded.pkl".format(dataset)

SAVE_PATH = DATASET_PATH + "sentences/"
MSR_BOW_PATH = SAVE_PATH + "bow/"
MSR_W2V_PATH = SAVE_PATH + "w2v/"
MSR_RNN_PATH = SAVE_PATH + "rnn/"
MSR_LEN_PATH = SAVE_PATH + "len/"


def count_words(words, vocabulary, count):
    for word in words:
        try:
            index = vocabulary.index(word)
            count[index] += 1
        except ValueError:
            continue


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def complete_count():
    init = 0
    try:
        init = int(argv[1])
    except Exception:
        pass

    print("Loading Flickr30m vectors...")
    voc = flickr30m.get_vocabulary()
    count = np.zeros(len(voc))

    print("Loading dataset...")
    data = pd.read_csv(DATASET_FILE)

    print("Go")
    i = 0
    total = len(data['Description'])
    for sentence in data['Description'][init:]:
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        count_words(normalized_words, voc, count)
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")
            pickle.dump(count, open(COUNT_PATH, 'wb'))


def create_vocabulary(threshold=5):
    count = pickle.load(open(COUNT_PATH, 'rb'))
    all_voc = np.array(flickr30m.get_vocabulary())
    all_vectors = BigFile('vec500flickr30m')

    msr_voc = all_voc[count >= threshold]
    pickle.dump(msr_voc, open(MSR_VOC, 'wb'))

    msr_word2idx = dict()
    msr_vectors = []
    for i in range(len(msr_voc)):
        msr_word2idx[msr_voc[i]] = i
        msr_vectors.append(all_vectors.read([msr_voc[i]])[1][0])
        if i % 1000 == 0:
            print(i, "of", len(msr_voc), "ready")
    pickle.dump(msr_word2idx, open(MSR_IDX, 'wb'))
    pickle.dump(np.array(msr_vectors), open(MSR_VECTORS, 'wb'))


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index, maxlen=35):
    index_vector = [voc_index['<start>']]
    len_word = 0
    for i in range(len(words)):
        try:
            index_vector.append(voc_index[words[i]])
        except KeyError:
            index_vector.append(voc_index['<unk>'])
        len_word += 1
        if len_word >= maxlen:
            break
    index_vector.append(voc_index['<end>'])
    padding = maxlen - len_word
    if padding <= 0:
        return np.array(index_vector, dtype=np.int32), maxlen + 2
    else:
        return np.concatenate((index_vector, [voc_index['<pad>']]*padding)).astype(np.int32), len_word + 2


def generate_voc_for_rnn(ndims=500):
    msr_voc = list(pickle.load(open(MSR_VOC, 'rb')))
    msr_index = pickle.load(open(MSR_IDX, 'rb'))
    msr_vectors = list(pickle.load(open(MSR_VECTORS, 'rb')))
    special_keys = ['<start>', '<end>', '<pad>', '<unk>']
    special_vectors = [np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims),
                      np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims)]
    special_index = {word: index for index, word in enumerate(special_keys)}
    msr_index = {key: val + len(special_keys) for key, val in msr_index.items()}
    msr_voc_rnn = np.array(special_keys + msr_voc)
    msr_index_rnn = dict(msr_index, **special_index)
    msr_vectors_rnn = np.array(special_vectors + msr_vectors)
    pickle.dump(msr_voc_rnn, open(MSR_VOC_RNN, 'wb'))
    pickle.dump(msr_index_rnn, open(MSR_IDX_RNN, 'wb'))
    pickle.dump(msr_vectors_rnn, open(MSR_VECTORS_RNN, 'wb'))


def vectorize_dataset():
    init = 0
    try:
        init = int(argv[1])
    except Exception:
        pass

    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(MSR_VOC, 'rb'))
    voc_index = pickle.load(open(MSR_IDX, 'rb'))
    voc_index_rnn = pickle.load(open(MSR_IDX_RNN, 'rb'))
    vocabulary_vectors = pickle.load(open(MSR_VECTORS, 'rb'))

    print("Loading dataset...")
    data = pd.read_csv(DATASET_FILE)

    print("Go")
    total = len(data['Description'])
    i = 0
    video_sentence_counter = dict()
    for row in data.iterrows():
        sentence = row[1]['Description']
        video_id = row[1]['VideoID']
        if video_id not in video_sentence_counter.keys():
            video_sentence_counter[video_id] = 0
        save_id = "sentence" + str(video_sentence_counter[video_id]) + "_" + video_id
        video_sentence_counter[video_id] += 1
        # normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        # bow = bag_of_words(normalized_words, vocaulary)
        # pickle.dump(bow, open(MSR_BOW_PATH + save_id + ".pkl", 'wb'))
        # sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
        # pickle.dump(sentence_vector, open(MSR_W2V_PATH + save_id + ".pkl", 'wb'))
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        gru_vector, len_word = gru_input_vector(normalized_words, voc_index_rnn)
        pickle.dump(gru_vector, open(MSR_RNN_PATH + save_id + ".pkl", 'wb'))
        pickle.dump(len_word, open(MSR_LEN_PATH + save_id + ".pkl",'wb'))
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")


if __name__ == '__main__':
    create_vocabulary()