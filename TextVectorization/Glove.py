import bcolz
import numpy as np
import pickle
import re
import nltk
from nltk.corpus import stopwords

glove_path = "glove.6B"


def parse_w2v():
    words = []
    idx = 0
    word2idx = {}
    vectors = []

    with open(f'{glove_path}/glove.6B.300d.txt', 'rb') as f:
        for l in f:
            line = l.decode().split()
            word = line[0]
            words.append(word)
            word2idx[word] = idx
            idx += 1
            vect = np.array(line[1:]).astype(np.float)
            vectors.append(vect)

    pickle.dump(np.array(vectors), open(f'{glove_path}/glove_vectors.300.pkl', 'wb'))
    # pickle.dump(words, open(f'{glove_path}/6B.300_words.pkl', 'wb'))
    # pickle.dump(word2idx, open(f'{glove_path}/6B.300_idx.pkl', 'wb'))


def get_complete_vocabulary():
    return pickle.load(open(f'{glove_path}/6B.300_words.pkl', 'rb'))


def get_vocabulary_index():
    return pickle.load(open(f'{glove_path}/6B.300_idx.pkl', 'rb'))


def get_vector_list():
    vectors = pickle.load(open(f'{glove_path}/glove_vectors.300.pkl', 'rb'))
    return vectors


def get_vector_dict():
    words = get_complete_vocabulary()
    word2idx = get_vocabulary_index()
    vectors = bcolz.open(f'{glove_path}/6B.300.dat')[:]

    glove = {w: vectors[word2idx[w]] for w in words}
    return glove


def normalize_words(s):
    """
    Given a text, cleans and normalizes it.
    """
    s = s.lower()
    # Isolate punctuation
    s = re.sub(r'([\'\"\.\(\)\!\?\-\\\/\,])', r' \1 ', s)
    # Remove some special characters
    s = re.sub(r'([\;\:\|\(\)•«\n])', ' ', s)
    s = s.replace("-", "")
    # Replace numbers and symbols with language
    s = s.replace('&', ' and ')
    s = s.replace('@', ' at ')
    s = s.split(" ")
    stop_words = set(stopwords.words('english'))
    s = [w for w in s if w not in stop_words]
    return s


if __name__ == '__main__':
    parse_w2v()