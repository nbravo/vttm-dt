from sys import argv
import TextVectorization.Glove as glove
import json
import numpy as np
import pickle

np.seterr(over='raise')

DATASET_PATH = "/run/media/nbravo/Elements/VTT2019/"
DATASET_FILE = DATASET_PATH + "matching.ranking.files/tv19.vtt.descriptions.{}"
MSR_VOC = "objects/msr_voc.pkl"
MSR_IDX = "objects/msr_index.pkl"
MSR_VECTORS = "objects/msr_vectors_padded.pkl"

BOW_VEC_PATH = DATASET_PATH + "subset_{}/bow/"
W2V_VEC_PATH = DATASET_PATH + "subset_{}/w2v/"
GRU_VEC_PATH = DATASET_PATH + "subset_{}/gru/"
LEN_VEC_PATH = DATASET_PATH + "subset_{}/lens/"


def bag_of_words(words, vocabulary_index):
    bow = np.zeros(len(vocabulary_index.keys()))
    for w in words:
        try:
            bow[vocabulary_index[w]] += 1
        except KeyError:
            continue
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors, padded=False):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            if padded:
                vector = np.copy(vocabulary_vectors[voc_index[words[i]] + 1])
            else:
                vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            if padded:
                vector_copy = np.copy(vocabulary_vectors[voc_index[word] + 1])
            else:
                vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index, padded=True, maxlen=32):
    index_vector = []
    len_word = 0
    for i in range(len(words)):
        try:
            if padded:
                index_vector.append(voc_index[words[i]] + 1)
            else:
                index_vector.append(voc_index[words[i]])
            len_word += 1
        except KeyError:
            continue
    padding = maxlen - len_word
    if padding < 0:
        return np.array(index_vector[:maxlen], dtype=np.int32), maxlen
    else:
        return np.concatenate((index_vector, np.zeros(padding))).astype(np.int32), len_word


def vectorize_dataset(SUBSET):
    print("Loading Glove vectors...")
    voc_index = pickle.load(open(MSR_IDX, 'rb'))
    vocabulary_vectors = pickle.load(open(MSR_VECTORS, 'rb'))

    bow_path = BOW_VEC_PATH.format(SUBSET)
    w2v_path = W2V_VEC_PATH.format(SUBSET)
    gru_path = GRU_VEC_PATH.format(SUBSET)
    len_path = LEN_VEC_PATH.format(SUBSET)
    with open(DATASET_FILE.format(SUBSET), 'r') as fin:
        line = fin.readline()

        while line:
            index, sentence = line.split(" ", 1)
            save_id = "sentence" + str(index)
            normalized_words = glove.normalize_words(sentence)
            bow = bag_of_words(normalized_words, voc_index)
            pickle.dump(bow, open(bow_path + save_id + ".pkl", 'wb'))
            sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors, padded=True)
            pickle.dump(sentence_vector, open(w2v_path + save_id + ".pkl", 'wb'))
            gru_vector, len_sen = gru_input_vector(normalized_words, voc_index, padded=True)
            pickle.dump(gru_vector, open(gru_path + save_id + ".pkl", 'wb'))
            pickle.dump(len_sen, open(len_path + save_id + ".pkl", 'wb'))
            line = fin.readline()


if __name__ == '__main__':
    vectorize_dataset("E")