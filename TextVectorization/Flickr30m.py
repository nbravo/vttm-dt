import re
from nltk.corpus import stopwords


def get_vocabulary():
    return open("vec500flickr30m/id.txt", encoding='ISO-8859-1').read().strip().split()


def normalize_words(s, clean_stopwords=False):
    """
    Given a text, cleans and normalizes it.
    """
    s = s.lower()
    # Isolate punctuation
    s = re.sub(r"[^A-Za-z0-9]", " ", s)
    s = s.strip().lower().split()
    if clean_stopwords:
        stop_words = set(stopwords.words('english'))
        s = [w for w in s if w not in stop_words]
    return s

if __name__ == '__main__':
    print(normalize_words("a man is singing a song and playing guitar and dancing with other s", clean_stopwords=False))
