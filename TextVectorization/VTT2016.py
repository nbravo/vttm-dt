from sys import argv
import TextVectorization.Flickr30m as flickr30m
import json
import numpy as np
import pickle
from TextVectorization.bigfile import BigFile
import codecs

np.seterr(over='raise')

w2v_dataset = 'msr'

DATASET_PATH = "/run/media/nbravo/Elements/VTT2016/"
DATASET_FILE = DATASET_PATH + "vines.textDescription.{}.testingSet"
SENT_PATH = DATASET_PATH + "sentences_{}_msr_voc/"
VOC = "objects/{}/voc.pkl".format(w2v_dataset)
IDX = "objects/{}/index.pkl".format(w2v_dataset)
RNN_IDX = "objects/{}/rnn/index.pkl".format(w2v_dataset)
VECTORS = "objects/{}/vectors.pkl".format(w2v_dataset)

BOW_PATH = SENT_PATH + "bow/"
W2V_PATH = SENT_PATH + "w2v/"
GRU_PATH = SENT_PATH + "rnn/"
LEN_PATH = SENT_PATH + "len/"


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index):
    index_vector = [voc_index['<start>']]
    len_word = 0
    for i in range(len(words)):
        try:
            index_vector.append(voc_index[words[i]])
        except KeyError:
            index_vector.append(voc_index['<unk>'])
        len_word += 1
    index_vector.append(voc_index['<end>'])
    return np.array(index_vector, dtype=np.int32), len_word + 2


def vectorize_dataset(SUBSET):
    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(VOC, 'rb'))
    voc_index = pickle.load(open(IDX, 'rb'))
    voc_rnn_index = pickle.load(open(RNN_IDX, 'rb'))
    vocabulary_vectors = pickle.load(open(VECTORS, 'rb'))

    print("Loading dataset...")
    with codecs.open(DATASET_FILE.format(SUBSET), 'r', 'iso-8859-15') as fin:
        line = fin.readline()
        while line:
            index, sentence = line.split("    ", 1)
            save_id = "sentence" + str(index)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            bow = bag_of_words(normalized_words, vocaulary)
            pickle.dump(bow, open(BOW_PATH.format(SUBSET) + save_id + ".pkl", 'wb'))
            # sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
            # pickle.dump(sentence_vector, open(W2V_PATH.format(SUBSET) + save_id + ".pkl", 'wb'))
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            gru_vector, len_word = gru_input_vector(normalized_words, voc_rnn_index)
            pickle.dump(gru_vector, open(GRU_PATH.format(SUBSET) + save_id + ".pkl", 'wb'))
            pickle.dump(len_word, open(LEN_PATH.format(SUBSET) + save_id + ".pkl", 'wb'))
            if int(index) % 100 == 0:
                print(index, "ready")
            
            line = fin.readline()

if __name__ == '__main__':
    vectorize_dataset("A")
