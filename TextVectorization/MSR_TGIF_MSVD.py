import pickle
import numpy as np
import pandas as pd
import json

from TextVectorization.bigfile import BigFile
import TextVectorization.Flickr30m as flickr30m

MSR_FILE = "/home/nbravo/data/MSR-VTT/train_val_videodatainfo.json"
MSR_DATASET = "msr"
MSR_VOC = "objects/{}/msr_voc.pkl".format(MSR_DATASET)
MSR_IDX = "objects/{}/msr_index.pkl".format(MSR_DATASET)
MSR_VECTORS = "objects/{}/msr_vectors.pkl".format(MSR_DATASET)
MSR_VOC_RNN = "objects/{}/rnn/msr_voc.pkl".format(MSR_DATASET)
MSR_IDX_RNN = "objects/{}/rnn/msr_index.pkl".format(MSR_DATASET)
MSR_VECTORS_RNN = "objects/{}/rnn/msr_vectors.pkl".format(MSR_DATASET)


TGIF_FILE = "/run/media/nbravo/nbravo-2TB/TGIF-Release-master/data/tgif-v1.0.tsv"
TGIF_DATASET = "tgif"
TGIF_VOC = "objects/{}/voc.pkl".format(TGIF_DATASET)
TGIF_IDX = "objects/{}/index.pkl".format(TGIF_DATASET)
TGIF_VECTORS = "objects/{}/vectors.pkl".format(TGIF_DATASET)
TGIF_VOC_RNN = "objects/{}/rnn/voc_sw.pkl".format(TGIF_DATASET)
TGIF_IDX_RNN = "objects/{}/rnn/index_sw.pkl".format(TGIF_DATASET)
TGIF_VECTORS_RNN = "objects/{}/rnn/vectors_sw.pkl".format(TGIF_DATASET)

MSVD_FILE = "/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv"
MSVD_DATASET = "msvd"
MSVD_VOC = "objects/{}/msr_voc.pkl".format(MSVD_DATASET)
MSVD_IDX = "objects/{}/msr_index.pkl".format(MSVD_DATASET)
MSVD_VECTORS = "objects/{}/msr_vectors.pkl".format(MSVD_DATASET)
MSVD_VOC_RNN = "objects/{}/rnn/msr_voc.pkl".format(MSVD_DATASET)
MSVD_IDX_RNN = "objects/{}/rnn/msr_index.pkl".format(MSVD_DATASET)
MSVD_VECTORS_RNN = "objects/{}/rnn/msr_vectors.pkl".format(MSVD_DATASET)

JOIN_DATASET = "msr_tgif_msvd"
JOIN_COUNT_PATH = "objects/{}/count.pkl".format(JOIN_DATASET)
JOIN_VOC = "objects/{}/voc.pkl".format(JOIN_DATASET)
JOIN_IDX = "objects/{}/index.pkl".format(JOIN_DATASET)
JOIN_VECTORS = "objects/{}/vectors.pkl".format(JOIN_DATASET)
JOIN_VOC_RNN = "objects/{}/rnn/voc.pkl".format(JOIN_DATASET)
JOIN_IDX_RNN = "objects/{}/rnn/index.pkl".format(JOIN_DATASET)
JOIN_VECTORS_RNN = "objects/{}/rnn/vectors.pkl".format(JOIN_DATASET)


def count_words(words, vocabulary, count):
    for word in words:
        try:
            index = vocabulary.index(word)
            count[index] += 1
        except ValueError:
            continue


def complete_count():
    print("Loading Flickr30m vectors...")
    voc = flickr30m.get_vocabulary()
    count = np.zeros(len(voc))

    # MSR
    print("Loading MSR...")
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)
    i = 0
    total = len(msr['sentences'])
    for data in msr['sentences']:
        sentence = data['caption']
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        count_words(normalized_words, voc, count)
        i += 1
        if i % 10000 == 0:
            print("MSR", i, "of", total, "ready")
            pickle.dump(count, open(JOIN_COUNT_PATH, 'wb'))

    # TGIF
    print("Loading TGIF...")
    i = 0
    with open(TGIF_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            count_words(normalized_words, voc, count)
            i += 1
            if i % 10000 == 0:
                print("TGIF", i, "ready")
                pickle.dump(count, open(JOIN_COUNT_PATH, 'wb'))
            line = fin.readline()

    # MSVD
    print("Loading MSVD...")
    data = pd.read_csv(MSVD_FILE)
    i = 0
    total = len(data['Description'])
    for sentence in data['Description']:
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        count_words(normalized_words, voc, count)
        i += 1
        if i % 10000 == 0:
            print("MSVD", i, "of", total, "ready")
            pickle.dump(count, open(JOIN_COUNT_PATH, 'wb'))


def complete_max_sentence_len():
    print("Loading Flickr30m vectors...")
    lens = {}

    # MSR
    print("Loading MSR...")
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)
    for data in msr['sentences']:
        sentence = data['caption']
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        try:
            lens[len(normalized_words)] += 1
        except KeyError:
            lens[len(normalized_words)] = 1

    # TGIF
    print("Loading TGIF...")
    with open(TGIF_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            try:
                lens[len(normalized_words)] += 1
            except KeyError:
                lens[len(normalized_words)] = 1
            line = fin.readline()

    # MSVD
    print("Loading MSVD...")
    data = pd.read_csv(MSVD_FILE)
    for sentence in data['Description']:
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        try:
            lens[len(normalized_words)] += 1
        except KeyError:
            lens[len(normalized_words)] = 1
    print(lens)


def create_vocabulary(threshold=5):
    count = pickle.load(open(JOIN_COUNT_PATH, 'rb'))
    all_voc = np.array(flickr30m.get_vocabulary())
    all_vectors = BigFile('vec500flickr30m')

    join_voc = all_voc[count >= threshold]
    pickle.dump(join_voc, open(JOIN_VOC, 'wb'))

    join_word2idx = dict()
    join_vectors = []
    for i in range(len(join_voc)):
        join_word2idx[join_voc[i]] = i
        join_vectors.append(all_vectors.read_one(join_voc[i]))
        if i % 1000 == 0:
            print(i, "of", len(join_voc), "ready")
    pickle.dump(join_word2idx, open(JOIN_IDX, 'wb'))
    pickle.dump(np.array(join_vectors), open(JOIN_VECTORS, 'wb'))


def join_vocabularies():
    msr_voc = pickle.load(open(MSR_VOC, 'rb'))
    tgif_voc = pickle.load(open(TGIF_VOC, 'rb'))
    msvd_voc = pickle.load(open(MSR_VOC, 'rb'))
    flatten = [word for voc in [msr_voc, tgif_voc, msvd_voc] for word in voc]
    join_voc = list(set(flatten))
    pickle.dump(join_voc, open(JOIN_VOC, 'wb'))

    all_vectors = BigFile('vec500flickr30m')
    join_word2idx = dict()
    join_vectors = []
    for i in range(len(join_voc)):
        join_word2idx[join_voc[i]] = i
        join_vectors.append(all_vectors.read([join_voc[i]])[1][0])
        if i % 1000 == 0:
            print(i, "of", len(join_voc), "ready")
    pickle.dump(join_word2idx, open(JOIN_IDX, 'wb'))
    pickle.dump(np.array(join_vectors), open(JOIN_VECTORS, 'wb'))


def generate_voc_for_rnn(ndims=500):
    join_voc = list(pickle.load(open(JOIN_VOC, 'rb')))
    join_index = pickle.load(open(JOIN_IDX, 'rb'))
    join_vectors = list(pickle.load(open(JOIN_VECTORS, 'rb')))
    special_keys = ['<start>', '<end>', '<pad>', '<unk>']
    special_vectors = [np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims),
                      np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims)]
    special_index = {word: index for index, word in enumerate(special_keys)}
    join_index = {key: val + len(special_keys) for key, val in join_index.items()}
    join_voc_rnn = np.array(special_keys + join_voc)
    join_index_rnn = dict(join_index, **special_index)
    join_vectors_rnn = np.array(special_vectors + join_vectors)
    pickle.dump(join_voc_rnn, open(JOIN_VOC_RNN, 'wb'))
    pickle.dump(join_index_rnn, open(JOIN_IDX_RNN, 'wb'))
    pickle.dump(join_vectors_rnn, open(JOIN_VECTORS_RNN, 'wb'))


def generate_voc_for_msr(ndims=500):
    msr_voc = list(pickle.load(open(MSR_VOC, 'rb')))
    msr_index = pickle.load(open(MSR_IDX, 'rb'))
    msr_vectors = list(pickle.load(open(MSR_VECTORS, 'rb')))
    special_keys = ['<start>', '<end>', '<pad>', '<unk>']
    special_vectors = [np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims),
                      np.random.uniform(-1, 1, ndims), np.random.uniform(-1, 1, ndims)]
    special_index = {word: index for index, word in enumerate(special_keys)}
    join_index = {key: val + len(special_keys) for key, val in msr_index.items()}
    join_voc_rnn = np.array(special_keys + msr_voc)
    join_index_rnn = dict(join_index, **special_index)
    join_vectors_rnn = np.array(special_vectors + msr_vectors)
    pickle.dump(join_voc_rnn, open(MSR_VOC_RNN, 'wb'))
    pickle.dump(join_index_rnn, open(MSR_IDX_RNN, 'wb'))
    pickle.dump(join_vectors_rnn, open(MSR_VECTORS_RNN, 'wb'))


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index):
    index_vector = [voc_index['<start>']]
    len_word = 0
    for i in range(len(words)):
        try:
            index_vector.append(voc_index[words[i]])
        except KeyError:
            index_vector.append(voc_index['<unk>'])
        len_word += 1
    index_vector.append(voc_index['<end>'])
    return np.array(index_vector, dtype=np.int32), len_word + 2


def vectorize_msr():
    save_path = "/run/media/nbravo/Elements/MSR_all/sentences_msr_voc/"

    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(JOIN_VOC, 'rb'))
    voc_index = pickle.load(open(JOIN_IDX, 'rb'))
    voc_index_rnn = pickle.load(open(JOIN_IDX_RNN, 'rb'))
    vocabulary_vectors = pickle.load(open(JOIN_VECTORS_RNN, 'rb'))

    print("Loading dataset...")
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)

    print("Go")
    total = len(msr['sentences'])
    i = 0
    for data in msr['sentences']:
        sentence = data['caption']
        sentence_id = data['sen_id']
        video_id = data['video_id']
        save_id = "sentence" + str(sentence_id) + "_" + video_id
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        bow = bag_of_words(normalized_words, vocaulary)
        pickle.dump(bow, open(save_path + "bow/" + save_id + ".pkl", 'wb'))
        sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
        pickle.dump(sentence_vector, open(save_path + "w2v/" + save_id + ".pkl", 'wb'))
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        gru_vector, len_word = gru_input_vector(normalized_words, voc_index_rnn)
        pickle.dump(gru_vector, open(save_path + "rnn/" + save_id + ".pkl", 'wb'))
        pickle.dump(len_word, open(save_path + "len/" + save_id + ".pkl", 'wb'))
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")


def vectorize_msvd():
    save_path = "/run/media/nbravo/nbravo-2TB/MSVD/sentences_new_gru/"

    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(JOIN_VOC, 'rb'))
    voc_index = pickle.load(open(JOIN_IDX, 'rb'))
    voc_index_rnn = pickle.load(open(JOIN_IDX_RNN, 'rb'))
    vocabulary_vectors = pickle.load(open(JOIN_VECTORS, 'rb'))

    print("Loading dataset...")
    data = pd.read_csv(MSVD_FILE)

    print("Go")
    total = len(data['Description'])
    i = 0
    video_sentence_counter = dict()
    for row in data.iterrows():
        sentence = row[1]['Description']
        video_id = row[1]['VideoID']
        if video_id not in video_sentence_counter.keys():
            video_sentence_counter[video_id] = 0
        save_id = "sentence" + str(video_sentence_counter[video_id]) + "_" + video_id
        video_sentence_counter[video_id] += 1
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        bow = bag_of_words(normalized_words, vocaulary)
        # pickle.dump(bow, open(save_path + "bow/" + save_id + ".pkl", 'wb'))
        # sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
        # pickle.dump(sentence_vector, open(save_path + "w2v/" + save_id + ".pkl", 'wb'))
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        gru_vector, len_word = gru_input_vector(normalized_words, voc_index_rnn)
        pickle.dump(gru_vector, open(save_path + "rnn/" + save_id + ".pkl", 'wb'))
        pickle.dump(len_word, open(save_path + "len/" + save_id + ".pkl", 'wb'))
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")


def vectorize_tgif():
    save_path = "/run/media/nbravo/Elements/TGIF/sentences_new_gru/"

    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(JOIN_VOC, 'rb'))
    voc_index = pickle.load(open(JOIN_IDX, 'rb'))
    voc_index_rnn = pickle.load(open(JOIN_IDX_RNN, 'rb'))
    vocabulary_vectors = pickle.load(open(JOIN_VECTORS, 'rb'))

    i = 0
    with open(TGIF_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            save_id = "sentence" + str(i)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            # bow = bag_of_words(normalized_words, vocaulary)
            # pickle.dump(bow, open(save_path + "bow/" + save_id + ".pkl", 'wb'))
            # sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
            # pickle.dump(sentence_vector, open(save_path + "w2v/" + save_id + ".pkl", 'wb'))
            # normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            gru_vector, len_word = gru_input_vector(normalized_words, voc_index_rnn)
            pickle.dump(gru_vector, open(save_path + "rnn/" + save_id + ".pkl", 'wb'))
            pickle.dump(len_word, open(save_path + "len/" + save_id + ".pkl", 'wb'))
            i += 1
            if i % 10000 == 0:
                print(i, "ready")
            line = fin.readline()

def generate_sentences_msr():
    save_path = "/run/media/nbravo/Elements/MSR_all/sentences/"
    vocabulary = pickle.load(open(JOIN_VOC, 'rb'))

    print("Loading dataset...")
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)

    i = 0
    total = len(msr['sentences'])

    for data in msr['sentences']:
        sentence = data['caption']
        sentence_id = data['sen_id']
        video_id = data['video_id']
        normalized = flickr30m.normalize_words(sentence, clean_stopwords=False)
        save_id = "sentence" + str(sentence_id) + "_" + video_id
        pickle.dump(normalized, open(save_path + "text/" + save_id + ".pkl", 'wb'))
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")

if __name__ == "__main__":
    vectorize_msvd()