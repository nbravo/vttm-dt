import sys

SETS = ["A", "B", "C", "D", "E"]
GT_PATH = "/home/nbravo/data/VTT2018/tv18.vtt.matching.ranking.gt"


def calc_inverted_rank(gt, ranking):
    return 1.0 / (ranking.index(gt) + 1)


def create_matching_dict(matching_file):
    matching_dict = dict()
    with open(matching_file, "r") as fout:
        line = fout.readline()
        while line:
            split = line.split(", ")
            matching_dict[int(split[0])] = [int(i) for i in split[1:]]
            line = fout.readline()
    return matching_dict


if __name__ == '__main__':
    matching = []
    inverted_rank_sum = []
    n = []
    for s in SETS:
        matching.append(create_matching_dict("{}.match".format(s)))
        inverted_rank_sum.append(0)
        n.append(0)

    gt_file = open(GT_PATH, "r")
    gt = gt_file.readline().strip()
    while gt:
        split = gt.split(" ")
        video_index = int(split[0])
        gt_matching = [int(i) for i in split[1:]]
        try:
            ranking = matching[0][video_index]
        except KeyError:
            gt = gt_file.readline().strip()
            continue
        for i in range(len(SETS)):
            ranking = matching[i][video_index]
            inverted_rank_sum[i] += calc_inverted_rank(gt_matching[i], ranking)
            n[i] += 1
        gt = gt_file.readline().strip()

    for i in range(len(SETS)):
        print("Mean Inverted Rank for Set", SETS[i], inverted_rank_sum[i] / n[i])
