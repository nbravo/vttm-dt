import pickle
import numpy as np
from TextVectorization.bigfile import BigFile
import re
from nltk.corpus import stopwords
import json
import TextVectorization.Flickr30m as flickr30m

MSR_COUNT_PATH = "objects/msr_flickr30m_2/msr_count.pkl"
MSR_VOC = "objects/msr_flickr30m_2/msr_voc.pkl"
MSR_IDX = "objects/msr_flickr30m_2/msr_index.pkl"
MSR_VECTORS = "objects/msr_flickr30m_2/msr_vectors.pkl"
MSR_VECTORS_PADDED = "objects/msr_flickr30m_2/msr_vectors_padded.pkl"
MSR_VOC_SW = "objects/msr_flickr30m_2/gru/msr_voc_sw.pkl"
MSR_IDX_SW = "objects/msr_flickr30m_2/gru/msr_index_sw.pkl"
MSR_VECTORS_SW = "objects/msr_flickr30m_2/gru/msr_vectors_sw.pkl"
MSR_VECTORS_SW_PADDED = "objects/msr_flickr30m_2/gru/msr_vectors_sw_padded.pkl"

TGIF_COUNT_PATH = "objects/tgif_base/count.pkl"
TGIF_VOC = "objects/tgif_base/voc.pkl"
TGIF_IDX = "objects/tgif_base/index.pkl"
TGIF_VECTORS = "objects/tgif_base/vectors.pkl"
TGIF_VOC_SW = "objects/tgif_base/gru/voc_sw.pkl"
TGIF_IDX_SW = "objects/tgif_base/gru/index_sw.pkl"
TGIF_VECTORS_SW = "objects/tgif_base/gru/vectors_sw.pkl"
TGIF_VECTORS_SW_PADDED = "objects/tgif_base/gru/vectors_sw_padded.pkl"

MSR_TGIF_VOC = "objects/msr_tgif/voc.pkl"
MSR_TGIF_VECTORS = "objects/msr_tgif/vectors.pkl"
MSR_TGIF_IDX = "objects/msr_tgif/index.pkl"
MSR_TGIF_VOC_SW = "objects/msr_tgif/gru/voc_sw.pkl"
MSR_TGIF_IDX_SW = "objects/msr_tgif/gru/index_sw.pkl"
MSR_TGIF_VECTORS_SW = "objects/msr_tgif/gru/vectors_sw.pkl"
MSR_TGIF_VECTORS_SW_PADDED = "objects/msr_tgif/gru/vectors_sw_padded.pkl"
MSR_TGIF_VECTORS_PADDED = "objects/msr_tgif/vectors_padded.pkl"

MSR_FILE = "/run/media/nbravo/nbravo-2TB/MSR/videodatainfo_2017.json"
TGIF_FILE = "/run/media/nbravo/nbravo-2TB/TGIF-Release-master/data/tgif-v1.0.tsv"

SAVE_PATH = "/run/media/nbravo/Elements/MSR_TGIF/base/text/"
MSR_BOW = SAVE_PATH + "msr/bow/"
MSR_W2V = SAVE_PATH + "msr/w2v/"
MSR_GRU = SAVE_PATH + "msr/gru_padded/"
MSR_LEN = SAVE_PATH + "msr/lens/"
TGIF_BOW = SAVE_PATH + "tgif/bow/"
TGIF_W2V = SAVE_PATH + "tgif/w2v/"
TGIF_GRU = SAVE_PATH + "tgif/gru_padded/"
TGIF_LEN = SAVE_PATH + "tgif/lens/"


def join_vocab():
    msr_voc = pickle.load(open(MSR_VOC, 'rb'))
    tgif_voc = pickle.load(open(TGIF_VOC, 'rb'))
    msr_tgif_voc = np.unique(np.concatenate((msr_voc, tgif_voc)))
    pickle.dump(msr_tgif_voc, open(MSR_TGIF_VOC, 'wb'))

    all_vectors = BigFile('vec500flickr30m')

    word2idx = dict()
    vectors = []
    for i in range(len(msr_tgif_voc)):
        word2idx[msr_tgif_voc[i]] = i
        vectors.append(all_vectors.read([msr_tgif_voc[i]])[1][0])
        if i % 1000 == 0:
            print(i, "of", len(msr_tgif_voc), "ready")
    pickle.dump(word2idx, open(MSR_TGIF_IDX, 'wb'))
    pickle.dump(np.array(vectors), open(MSR_TGIF_VECTORS, 'wb'))


def generate_voc_with_sw():
    voc = list(pickle.load(open(MSR_TGIF_VOC, 'rb')))
    msr_tgif_index = pickle.load(open(MSR_TGIF_IDX, 'rb'))
    vectors = list(pickle.load(open(MSR_TGIF_VECTORS, 'rb')))
    sw = stopwords.words('english')
    sw = [re.sub(r'(\')', '', word) for word in sw]
    all_vectors = BigFile('vec500flickr30m')
    banned_sw = [word for word in sw if len(all_vectors.read([word])[0]) == 0]
    sw = [word for word in sw if word not in banned_sw]
    msr_voc_sw = np.array(sw + voc)
    sw_index = {word: index for index, word in enumerate(sw)}
    msr_index = {key: val + len(sw) for key, val in msr_tgif_index.items()}
    msr_index_sw = dict(msr_index, **sw_index)
    sw_vectors = [all_vectors.read([word])[1][0] for word in sw]
    msr_vectors_sw = sw_vectors + vectors
    pickle.dump(msr_voc_sw, open(MSR_TGIF_VOC_SW, 'wb'))
    pickle.dump(msr_index_sw, open(MSR_TGIF_IDX_SW, 'wb'))
    pickle.dump(msr_vectors_sw, open(MSR_TGIF_VECTORS_SW, 'wb'))


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index, padded=True, maxlen=35):
    index_vector = []
    len_word = 0
    for i in range(len(words)):
        try:
            if padded:
                index_vector.append(voc_index[words[i]] + 1)
            else:
                index_vector.append(voc_index[words[i]])
            len_word += 1
        except KeyError:
            continue
    padding = maxlen - len_word
    if padding < 0:
        return np.array(index_vector[:maxlen], dtype=np.int32), maxlen
    else:
        return np.concatenate((index_vector, np.zeros(padding))).astype(np.int32), len_word


def pad_vector_matrix(sw=True):
    if sw:
        msr_vectors = pickle.load(open(MSR_TGIF_VECTORS_SW, 'rb'))
    else:
        msr_vectors = pickle.load(open(MSR_TGIF_VECTORS, 'rb'))
    pad_vector = list(np.zeros(500))
    padded_matrix = np.array([pad_vector] + msr_vectors)
    if sw:
        pickle.dump(padded_matrix, open(MSR_TGIF_VECTORS_SW_PADDED, 'wb'))
    else:
        pickle.dump(padded_matrix, open(MSR_TGIF_VECTORS_PADDED, 'wb'))


def vectorize_msr():
    vocaulary = pickle.load(open(MSR_TGIF_VOC, 'rb'))
    voc_index = pickle.load(open(MSR_TGIF_IDX, 'rb'))
    voc_index_sw = pickle.load(open(MSR_TGIF_IDX_SW, 'rb'))
    vocabulary_vectors = pickle.load(open(MSR_TGIF_VECTORS, 'rb'))

    print("Loading dataset...")
    with open(MSR_FILE, 'r') as fin:
        msr = json.load(fin)

    print("Go")
    i = 0
    total = len(msr['sentences'])
    for data in msr['sentences']:
        sentence = data['caption']
        sentence_id = data['sen_id']
        video_id = data['video_id']
        save_id = "sentence" + str(sentence_id) + "_" + video_id
        # normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=True)
        # bow = bag_of_words(normalized_words, vocaulary)
        # pickle.dump(bow, open(MSR_BOW + save_id + ".pkl", 'wb'))
        # sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
        # pickle.dump(sentence_vector, open(MSR_W2V + save_id + ".pkl", 'wb'))
        normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
        gru_vector, len_word = gru_input_vector(normalized_words, voc_index_sw)
        pickle.dump(gru_vector, open(MSR_GRU + save_id + ".pkl", 'wb'))
        pickle.dump(len_word, open(MSR_LEN + save_id + ".pkl",'wb'))
        i += 1
        if i % 10000 == 0:
            print(i, "of", total, "ready")


def vectorize_tgif():
    vocaulary = pickle.load(open(MSR_TGIF_VOC, 'rb'))
    voc_index = pickle.load(open(MSR_TGIF_IDX, 'rb'))
    voc_index_sw = pickle.load(open(MSR_TGIF_IDX_SW, 'rb'))
    vocabulary_vectors = pickle.load(open(MSR_TGIF_VECTORS, 'rb'))

    i = 0
    with open(TGIF_FILE, 'r') as fin:
        line = fin.readline()
        while line:
            _, sentence = line.split('\t', 1)
            save_id = "sentence" + str(i)
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=True)
            bow = bag_of_words(normalized_words, vocaulary)
            pickle.dump(bow, open(TGIF_BOW + save_id + ".pkl", 'wb'))
            sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
            pickle.dump(sentence_vector, open(TGIF_W2V + save_id + ".pkl", 'wb'))
            normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
            gru_vector, len_word = gru_input_vector(normalized_words, voc_index_sw)
            pickle.dump(gru_vector, open(TGIF_GRU + save_id + ".pkl", 'wb'))
            pickle.dump(len_word, open(TGIF_LEN + save_id + ".pkl", 'wb'))
            i += 1
            if i % 10000 == 0:
                print(i, "ready")
            line = fin.readline()


if __name__ == '__main__':
    pad_vector_matrix()