import pickle
import nltk
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import cv2.cv2 as cv2
import os

INFO_PATH = "/run/media/nbravo/nbravo-2TB/VTT2018/INFO/matching.ranking.eval.readme/"
SETS = ["A", "B", "C", "D", "E"]
VIDEOS_PATH = '/run/media/nbravo/nbravo-2TB/VTT2018/videos/'
SAVE_FOLDER = '/home/nbravo/vttm-dt/VideoVectorization/video_frames/'

def check_dir(dir_path, save=True):
    if save:
        try:
            os.listdir(dir_path)
        except FileNotFoundError:
            os.makedirs(dir_path)

def create_sentence_dictionary():
    sentence_dict = {"A": {}, "B": {}, "C": {}, "D": {}, "E": {}}
    for s in SETS:
        with open(INFO_PATH + "tv18.vtt.descriptions." + s, 'r') as fin:
            line = fin.readline().strip()
            while line:
                sentence_id, sentence = line.split(" ", 1)
                sentence_dict[s][sentence_id] = sentence
                line = fin.readline().strip()
    with open(INFO_PATH + 'sentence_dict.pkl', 'wb') as fout:
        pickle.dump(sentence_dict, fout)


def create_gt_dict():
    gt_dict = {}
    with open(INFO_PATH + "tv18.vtt.matching.ranking.gt", 'r') as fin:
        line = fin.readline().strip()
        while line:
            video_id, gt = line.split(" ", 1)
            gt_dict[video_id] = gt.strip().split(" ")
            line = fin.readline().strip()

    with open(INFO_PATH + "gt_object.pkl", 'wb') as fout:
        pickle.dump(gt_dict, fout)


def get_gt_sentences(video_id, gt_dict=None, sentence_dict=None):
    video_id = str(video_id)
    if gt_dict is None:
        gt_dict = pickle.load(open(INFO_PATH + "gt_object.pkl", 'rb'))
    if sentence_dict is None:
        sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    gt = gt_dict[video_id]
    sentences = []
    for s, sentence_id in enumerate(gt):
        sentences.append(sentence_dict[SETS[s]][sentence_id])
    return sentences


def get_gt_video(set_name, sentence_id, gt_dict=None):
    sentence_id = str(sentence_id)
    if gt_dict is None:
        gt_dict = pickle.load(open(INFO_PATH + "gt_object.pkl", 'rb'))
    set_index = SETS.index(set_name)
    for video_id in gt_dict.keys():
        if gt_dict[video_id][set_index] == sentence_id:
            return video_id
    return None


def get_sentences(set_name, sentence_ids, sentence_dict=None):
    if sentence_dict is None:
        sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    for sentence in sentence_ids:
        print(sentence_dict[set_name][str(sentence)])


def get_first_sentences_rank(matching_folder, video_id, sentence_dict=None):
    video_id = str(video_id)
    if sentence_dict is None:
        sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    for model in os.listdir(matching_folder):
        print(model)
        for set_name in SETS:
            with open(matching_folder + model + '/' + set_name + '.match', 'r') as fout:
                line = fout.readline().strip()
                while line:
                    s = line.split(", ")
                    v_id = s[0]
                    first_sent_id = s[1]
                    if v_id == video_id:
                        print(set_name, first_sent_id, sentence_dict[set_name][first_sent_id])
                        break
                    line = fout.readline().strip()



def get_top_n_sentences_rank(matching_folder, video_id, n, sentence_dict=None):
    video_id = str(video_id)
    top_n_sentences = {'A': [], 'B': [], 'C': [], 'D': [], 'E': []}
    if sentence_dict is None:
        sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    for set_name in SETS:
        with open(matching_folder + set_name + '.match', 'r') as fout:
            line = fout.readline().strip()
            while line:
                s = line.split(", ")
                v_id = s[0]
                ranking = s[1:]
                if v_id == video_id:
                    sentences = ranking[:n]
                    for sentence in sentences:
                        top_n_sentences[set_name].append(sentence_dict[set_name][sentence])
                line = fout.readline().strip()
    for set_name in top_n_sentences.keys():
        print(set_name)
        for sentence in top_n_sentences[set_name]:
            print(sentence)


def save_n_frames(video_id, n):
    video_path =  VIDEOS_PATH + str(video_id) + '.mp4'
    cap = cv2.VideoCapture(video_path)
    save_folder = SAVE_FOLDER + str(video_id) + '/'
    check_dir(save_folder, save=True)
    total_frames = round(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    step = total_frames // n
    ret, f = cap.read()
    count = 0
    while ret:
        if count % step == 0:
            cv2.imwrite(save_folder + str(count) + '.jpg', f)
        ret, f = cap.read()
        count += 1
    cap.release()

def get_worst_n_sentences_rank(matching_folder, video_id, n, sentence_dict=None):
    video_id = str(video_id)
    top_n_sentences = {'A': [], 'B': [], 'C': [], 'D': [], 'E': []}
    if sentence_dict is None:
        sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    for set_name in SETS:
        with open(matching_folder + set_name + '.match', 'r') as fout:
            line = fout.readline().strip()
            while line:
                s = line.split(", ")
                v_id = s[0]
                ranking = s[1:]
                if v_id == video_id:
                    sentences = ranking[-n:]
                    for sentence in sentences:
                        top_n_sentences[set_name].append(sentence_dict[set_name][sentence])
                line = fout.readline().strip()
    for set_name in top_n_sentences.keys():
        print(set_name)
        for sentence in top_n_sentences[set_name]:
            print(sentence)

def plot_testing_word_type_distribution(result_file, tag):
    n = 1
    sets = ['A', 'B', 'C', 'D', 'E']
    testing_results = pd.read_csv(result_file)
    gt_dict = pickle.load(open(INFO_PATH + "gt_object.pkl", 'rb'))
    sentence_dict = pickle.load(open(INFO_PATH + 'sentence_dict.pkl', 'rb'))
    sentence_count_by_set = {'A': [], 'B': [], 'C': [], 'D': [], 'E': []}
    sentence_rank_by_set = {'A': [], 'B': [], 'C': [], 'D': [], 'E': []}
    for i, s in enumerate(sets):
        sorted_results = testing_results.sort_values(by=s)
        sorted_video_ids = sorted_results['video_id']
        for video_id in sorted_video_ids:
            sentence = get_gt_sentences(video_id, gt_dict=gt_dict, sentence_dict=sentence_dict)[i]
            text = nltk.word_tokenize(sentence)
            tags = nltk.pos_tag(text)
            count = len(list(filter(lambda x: x[1] == tag, tags)))
            sentence_count_by_set[s].append(count)
            sentence_rank_by_set[s].append(sorted_results[sorted_results['video_id'] == video_id][s].iloc[0])
    fig, axs = plt.subplots(5, sharex=True)
    fig.suptitle(result_file.split('/')[-1].split('.')[0])
    index = list(range(1, int(len(testing_results) / n + 1)))
    for i, s in enumerate(sets):
        df = pd.DataFrame()
        df['count'] = sentence_count_by_set[s]
        df['rank'] = sentence_rank_by_set[s]
        index = df['rank'].unique()
        value = df.groupby(by='rank').mean()['count']
        axs[i].bar(index, value)
        #sums = np.mean.reduceat(sentence_count_by_set[s], np.arange(0, len(sentence_count_by_set[s]), n))
        # axs[i].bar(index, sums)
        # sums = []
        # for index, rank in enumerate(sentence_rank_by_set[s]):
        #     for p in range(0, sentence_count_by_set[s][index]):
        #         sums.append(rank)
        # axs[i].hist(sums, range=[1, 100], bins=20)
    #plt.xlabel("Ranking")
    #plt.ylabel("Average Noun count")
    plt.show()
    #fig.savefig('{}_{}.svg'.format(result_file.split('/')[-1].split('.')[0], tag), format='svg')


if __name__ == "__main__":
    #print(get_gt_sentences(865)) #1297 748 96 1689 879
    plot_testing_word_type_distribution('/home/nbravo/vttm-dt/Model/log/fine_results/only_dt_run_1.csv', 'VB')
    #get_top_n_sentences_rank('/home/nbravo/vttm-dt/Model/matching/dong_2018_all_mir_ori/run1/', 779, 3)
    #get_worst_n_sentences_rank('/home/nbravo/vttm-dt/Model/dong_2018_dt_training/matching/all_dt/run_mediamill_1/', 100, 3)
    #save_n_frames(855, 20)
    #get_sentences('A', [6, 7, 12])
    #get_first_sentences_rank('/home/nbravo/vttm-dt/TextVectorization/matches/', 855)
    #print(get_gt_video('B', 1003))

