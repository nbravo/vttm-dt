from sys import argv
import json
import numpy as np
from TextVectorization.bigfile import BigFile
import TextVectorization.Flickr30m as flickr30m
import pickle

np.seterr(over='raise')

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017_INFO/"
DATASET_FILE = DATASET_PATH + \
               "vtt.testing.data/matching.ranking.subtask.testing.data/testing.{}.subsets/tv17.vtt.descriptions.{}"

MSR_VOC = "objects/msr_all_flickr30m/msr_voc.pkl"
MSR_IDX = "objects/msr_all_flickr30m/msr_index.pkl"
MSR_IDX_SW = "objects/msr_all_flickr30m/rnn/msr_index.pkl"
MSR_VECTORS = "objects/msr_all_flickr30m/msr_vectors.pkl"

BOW_VEC_PATH = "/run/media/nbravo/Elements/VTT2017/msr_all_flickr30m/subset_{}/{}/bow/"
W2V_VEC_PATH = "/run/media/nbravo/Elements/VTT2017/msr_all_flickr30m/subset_{}/{}/w2v/"
GRU_VEC_PATH = "/run/media/nbravo/Elements/VTT2017/msr_all_flickr30m/subset_{}/{}/rnn/"
LEN_VEC_PATH = "/run/media/nbravo/Elements/VTT2017/msr_all_flickr30m/subset_{}/{}/len/"


def count_words(words, vocabulary, count):
    for word in words:
        try:
            index = vocabulary.index(word)
            count[index] += 1
        except ValueError:
            continue


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index, maxlen=35):
    index_vector = [voc_index['<start>']]
    len_word = 0
    for i in range(len(words)):
        try:
            index_vector.append(voc_index[words[i]])
        except KeyError:
            index_vector.append(voc_index['<unk>'])
        len_word += 1
        if len_word >= maxlen:
            break
    index_vector.append(voc_index['<end>'])
    padding = maxlen - len_word
    if padding <= 0:
        return np.array(index_vector, dtype=np.int32), maxlen + 2
    else:
        return np.concatenate((index_vector, [voc_index['<pad>']]*padding)).astype(np.int32), len_word + 2


def vectorize_dataset(SET, SUBSET):
    init = 0
    try:
        init = int(argv[1])
    except Exception:
        pass

    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(MSR_VOC, 'rb'))
    voc_index = pickle.load(open(MSR_IDX, 'rb'))
    voc_index_sw = pickle.load(open(MSR_IDX_SW, 'rb'))
    vocabulary_vectors = pickle.load(open(MSR_VECTORS, 'rb'))

    print("Loading dataset...")
    for ss in SUBSET:
        with open(DATASET_FILE.format(SET, ss), 'r') as fin:
            line = fin.readline()
            while line:
                index, sentence = line.split(" ", 1)
                print(index)
                save_id = "sentence" + str(index)
                normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
                bow = bag_of_words(normalized_words, vocaulary)
                pickle.dump(bow, open(BOW_VEC_PATH.format(SET, ss) + save_id + ".pkl", 'wb'))
                sentence_vector = vectorize_sentence(normalized_words, voc_index, vocabulary_vectors)
                pickle.dump(sentence_vector, open(W2V_VEC_PATH.format(SET, ss) + save_id + ".pkl", 'wb'))
                normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
                gru_vector, len_word = gru_input_vector(normalized_words, voc_index_sw)
                pickle.dump(gru_vector, open(GRU_VEC_PATH.format(SET, ss) + save_id + ".pkl", 'wb'))
                pickle.dump(len_word, open(LEN_VEC_PATH.format(SET, ss) + save_id + ".pkl", 'wb'))

                line = fin.readline()


if __name__ == '__main__':
    vectorize_dataset("2", ["A", "B"])
