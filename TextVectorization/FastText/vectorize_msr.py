from sys import argv
import fastText
import json
import FastText.utils as utils

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/MSR/"
DATASET_FILE = DATASET_PATH + "videodatainfo_2017.json"
SENT_PATH = DATASET_PATH + "sentences/"

if __name__ == '__main__':
    init = 0
    try:
        init = int(argv[1])
    except Exception:
        pass

    print("Loading model...")
    model = fastText.load_model("wiki.en.bin")

    print("Loading dataset...")
    with open(DATASET_FILE, 'r') as fin:
        msr = json.load(fin)

    print("Go")
    i = 0
    total = len(msr['sentences'])
    for sentence in msr['sentences'][init:]:
        caption = sentence['caption']
        sentence_id = sentence['sen_id']
        video_id = sentence['video_id']
        vector = utils.text_to_vector(caption, model)
        save_path = SENT_PATH + "sentence" + str(sentence_id) + "_" + video_id
        utils.save_sentence(vector, save_path)
        i += 1
        if i % 100 == 0:
            print(i, "of", total, "ready")
