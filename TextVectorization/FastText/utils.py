import csv
import re
import pickle

DATA = 'tgif-v1.0.tsv'


def read_sentences(init, end):
    sentences = []

    with open(DATA, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        i = 1
        for row in reader:
            if i < init:
                i += 1
                continue
            sentences.append(row[1])
            i += 1
            if i > end:
                break

    return sentences


def normalize(s):
    """
    Given a text, cleans and normalizes it.
    """
    s = s.lower()
    # Replace ips
    s = re.sub(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', ' _ip_ ', s)
    # Isolate punctuation
    s = re.sub(r'([\'\"\.\(\)\!\?\-\\\/\,])', r' \1 ', s)
    # Remove some special characters
    s = re.sub(r'([\;\:\|•«\n])', ' ', s)
    # Replace numbers and symbols with language
    s = s.replace('&', ' and ')
    s = s.replace('@', ' at ')
    s = s.replace('0', ' zero ')
    s = s.replace('1', ' one ')
    s = s.replace('2', ' two ')
    s = s.replace('3', ' three ')
    s = s.replace('4', ' four ')
    s = s.replace('5', ' five ')
    s = s.replace('6', ' six ')
    s = s.replace('7', ' seven ')
    s = s.replace('8', ' eight ')
    s = s.replace('9', ' nine ')
    return s


def text_to_vector(text, model):
    """
    Given a string, normalizes it and convert it to a sentence vector.
    """
    text = normalize(text)
    return model.get_sentence_vector(text)


def save_sentence_vec(index, obj):
    with open("sentences/sentence" + str(index) + ".pkl", "wb") as fp:
        pickle.dump(obj, fp)


def read_sentence_vec(index):
    with open("sentences/sentence" + str(index) + ".pkl" , "rb") as fp:
        obj = pickle.load(fp)
    return obj


def s_sentence(index, obj, path):
    with open(path + "sentence" + str(index) + ".pkl", "wb") as fp:
        pickle.dump(obj, fp)

def save_sentence(obj, path):
    with open(path + ".pkl", "wb") as fp:
        pickle.dump(obj, fp)