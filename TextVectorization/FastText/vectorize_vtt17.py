import fastText
import FastText.utils as utils


DATASET_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017_INFO/"
BOW_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017/subset_{}/{}/bow/"
BOW_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017/subset_{}/{}/bow/"
BOW_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017/subset_{}/{}/bow/"
BOW_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2017/subset_{}/{}/bow/"
SUBSET_INDEX = "5"
SUBSET_SETS = ["A", "B", "C", "D", "E"]
SENTENCE_INFO = DATASET_PATH + \
                "vtt.testing.data/matching.ranking.subtask.testing.data/testing.{}.subsets/tv17.vtt.descriptions.{}"

model = fastText.load_model("wiki.en.bin")

for s in SUBSET_SETS:
    sent_info = SENTENCE_INFO.format(SUBSET_INDEX, s)
    save_path = SEN_VEC_PATH.format(SUBSET_INDEX, s)
    print("Vectorizing subset", SUBSET_INDEX, s)
    i = 0
    with open(sent_info, 'r') as fin:
        line = fin.readline()
        while line:
            index, sentence = line.split(" ", 1)
            vector = utils.text_to_vector(sentence, model)
            utils.s_sentence(index, vector, save_path)
            i += 1
            if i % 10 == 0:
                print(i, "sentences vectorized")
            line = fin.readline()