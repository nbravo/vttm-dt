import fastText
import FastText.utils as utils

model = fastText.load_model('wiki.en.bin')

init = 100000
end = 125783
i = init

sentences = utils.read_sentences(init, end)
for sentence in sentences:
    vector = utils.text_to_vector(sentence, model)
    utils.save_sentence_vec(i, vector)
    i += 1
    print(str(i), "of", str(end))
