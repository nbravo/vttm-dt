from sys import argv
import json
import numpy as np
from TextVectorization.bigfile import BigFile
import TextVectorization.Flickr30m as flickr30m
import pickle

np.seterr(over='raise')

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/VTT2018/INFO/"
DATASET_FILE = DATASET_PATH + \
               "matching.ranking.eval.readme/tv18.vtt.descriptions.{}"
MSR_VOC = "objects/msr_tgif_msvd/voc.pkl"
MSR_IDX = "objects/msr_tgif_msvd/index.pkl"
MSR_IDX_SW = "objects/msr_tgif_msvd/rnn/index.pkl"
MSR_VECTORS = "objects/msr_tgif_msvd/vectors.pkl"

BOW_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2018/sentences/set_{}/bow/"
GRU_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2018/sentences/set_{}/rnn/"
LEN_VEC_PATH = "/run/media/nbravo/nbravo-2TB/VTT2018/sentences/set_{}/len/"


def bag_of_words(words, vocabulary):
    bow = np.zeros(len(vocabulary))
    for w in words:
        index = np.where(vocabulary == w)[0]
        if len(index) == 0:
            continue
        bow[index[0]] += 1
    return bow


def vectorize_sentence(words, voc_index, vocabulary_vectors):
    if len(words) == 0:
        return None
    vector = None
    total_words = 0
    i = 0
    while vector is None and i < len(words):
        try:
            vector = np.copy(vocabulary_vectors[voc_index[words[i]]])
            total_words += 1
            i += 1
        except KeyError:
            i += 1
            continue
    if i >= len(words):
        return vector
    for word in words[i:]:
        try:
            vector_copy = np.copy(vocabulary_vectors[voc_index[word]])
            vector += vector_copy
            total_words += 1
        except KeyError:
            continue

    return vector / total_words


def gru_input_vector(words, voc_index):
    index_vector = [voc_index['<start>']]
    len_word = 0
    for i in range(len(words)):
        try:
            index_vector.append(voc_index[words[i]])
        except KeyError:
            index_vector.append(voc_index['<unk>'])
        len_word += 1
    index_vector.append(voc_index['<end>'])
    return np.array(index_vector, dtype=np.int32), len_word + 2


def vectorize_dataset(SETS):
    print("Loading Glove vectors...")
    vocaulary = pickle.load(open(MSR_VOC, 'rb'))
    voc_index_sw = pickle.load(open(MSR_IDX_SW, 'rb'))

    print("Loading dataset...")
    for s in SETS:
        with open(DATASET_FILE.format(s), 'r') as fin:
            line = fin.readline()
            while line:
                index, sentence = line.split(" ", 1)
                print(index)
                save_id = "sentence" + str(index)
                normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
                bow = bag_of_words(normalized_words, vocaulary)
                pickle.dump(bow, open(BOW_VEC_PATH.format(s) + save_id + ".pkl", 'wb'))
                normalized_words = flickr30m.normalize_words(sentence, clean_stopwords=False)
                gru_vector, len_word = gru_input_vector(normalized_words, voc_index_sw)
                pickle.dump(gru_vector, open(GRU_VEC_PATH.format(s) + save_id + ".pkl", 'wb'))
                pickle.dump(len_word, open(LEN_VEC_PATH.format(s) + save_id + ".pkl", 'wb'))
                line = fin.readline()


if __name__ == '__main__':
    vectorize_dataset(["A", "B", "C", "D", "E"])
