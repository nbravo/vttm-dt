import cv2.cv2 as cv2
import os


def get_video_frames(video_path):
    cap = cv2.VideoCapture(video_path)
    frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    return frames


def count_total_frames(videos_dir_path):
    total_frames = 0
    videos = os.listdir(videos_dir_path)
    for i in range(len(videos)):
        total_frames += get_video_frames(videos_dir_path + videos[i])
        print(i ,"of", len(videos))
    return total_frames


if __name__ == '__main__':
    DATASET_DIR = "/run/media/nbravo/nbravo-2TB/MSVD/videos/"
    print(count_total_frames(DATASET_DIR))