import cv2.cv2 as cv2
import torchvision.models as models
import torchvision.transforms as transforms
import torch
from torch.autograd import Variable
import os
from PIL import Image
import pickle

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/MSR/videos/"
SAVE_PATH = "/run/media/nbravo/Elements/MSR/resnet152/"


def to_pil(np_array):
    np_array_img = cv2.cvtColor(np_array, cv2.COLOR_BGR2RGB)
    return Image.fromarray(np_array_img)


def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    t_frame = 1.0 / fps

    ret, f = cap.read()
    while not ret:
        ret, f = cap.read()
    fs = [to_pil(f)]
    elapsed_time = t_frame
    while ret:
        ret, f = cap.read()
        elapsed_time += t_frame
        if ret and elapsed_time >= timeskip:
            fs.append(to_pil(f))
            elapsed_time = 0
    cap.release()
    return fs


def get_vector(pil_image, layer, model, transform, cuda, aslist=False):
    if cuda:
        t_img = Variable(transform(pil_image).unsqueeze(0).cuda())
    else:
        t_img = Variable(transform(pil_image).unsqueeze(0))
    my_embedding = torch.zeros(2048)

    def copy_data(m, i, o):
        my_embedding.copy_(o.data[0,:,0,0])

    h = layer.register_forward_hook(copy_data)
    model(t_img)
    h.remove()
    return my_embedding.tolist() if aslist else my_embedding


def save_vectors(video_name, embeddings=None, max_vector=None, mean_vector=None, save_mode='embed'): # save_mode = {embed, agg}
    video_name = video_name.split(".")[0]
    if save_mode == 'agg':
        max_path = SAVE_PATH + "max/" + video_name + ".pkl"
        mean_path = SAVE_PATH + "mean/" + video_name + ".pkl"
        if max_vector is not None:
            with open(max_path, 'wb') as f_max:
                pickle.dump(list(max_vector), f_max)
        if mean_vector is not None:
            with open(mean_path, 'wb') as f_mean:
                pickle.dump(list(mean_vector), f_mean)
    elif save_mode == 'embed':
        vectors_path = SAVE_PATH + "embeddings/" + video_name + ".pkl"
        if embeddings is not None:
            with open(vectors_path, 'wb') as f_vec:
                pickle.dump(embeddings, f_vec)
        else:
            raise ValueError
    else:
        raise ValueError


def vectorize():
    video_list = os.listdir(DATASET_PATH)
    resnet152 = models.resnet152(pretrained=True)
    cuda = torch.cuda.is_available()
    if cuda:
        resnet152 = resnet152.cuda()
    layer = resnet152._modules.get('avgpool')
    resnet152.eval()
    transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )])

    n = len(video_list)
    with torch.no_grad():
        for i in range(0, n):
            frames = get_video_frames(DATASET_PATH + "video3769.mkv")
            embeddings = []
            for frame in frames:
                embeddings.append(get_vector(frame, layer, resnet152, transform, cuda, aslist=True))
            #stacked_embeds = torch.stack(embeddings)
            #max_vector = stacked_embeds.max(0).values
            #mean_vector = stacked_embeds.mean(0)
            #save_vectors(video_list[i], embeddings=embeddings, save_mode='embed')
            print(i, "of", n, "done")


def check_max_min_sequences():
    dataset_path = "/run/media/nbravo/Elements/MSR/resnet152/embeddings/"
    embeddings = os.listdir(dataset_path)

    min_len = 10000000
    max_len = 0
    i = 0
    for emb in embeddings:
        len_embed = len(pickle.load(open(dataset_path + emb, 'rb')))
        if len_embed > max_len:
            max_len = len_embed
        if  len_embed < min_len:
            min_len = len_embed
        i += 1
        if i % 100 == 0:
            print(i, "of", len(embeddings))
    return min_len, max_len


if __name__ == '__main__':
    print(vectorize())

