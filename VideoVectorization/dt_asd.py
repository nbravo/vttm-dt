import os
import pickle

MSR_PATH = '/home/nbravo/data/MSVD/dt_hist/'


def msr():
    total = 0
    files = os.listdir(MSR_PATH)
    for index, dt in enumerate(files):
        dt_hist = pickle.load(open(MSR_PATH + dt, 'rb'))
        total += sum(dt_hist[0])
        print(index + 1, 'of', len(files))
    print(total)


if __name__ == '__main__':
    msr()