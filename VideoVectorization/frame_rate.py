import cv2.cv2 as cv2
import os
import pickle
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import pandas as pd
import numpy as np
import json

PALLETE = ["#0517e3", "#7bc8a4", "#4cc3d9"]

def get_video_framerate(video_path):
    cap = cv2.VideoCapture(video_path)
    frames = round(cap.get(cv2.CAP_PROP_FPS))
    cap.release()
    return frames


def frame_rate_dataset(videos_dir_path, video_repetition_path, save_path):
    frame_rate_hist = {}
    video_repetition_dict = pickle.load(open(video_repetition_path, 'rb'))
    videos = os.listdir(videos_dir_path)
    k = 0
    for i in range(len(videos)):
        video_id = videos[i].split('.')[0]
        try:
            count = video_repetition_dict[video_id]
        except KeyError:
            continue
        fps = get_video_framerate(videos_dir_path + videos[i])
        if fps in frame_rate_hist.keys():
            frame_rate_hist[fps] += count
        else:
            frame_rate_hist[fps] = count
        k += 1
        print(k ,"videos ready")
        if i % 1000 == 0:
            pickle.dump(frame_rate_hist, open(save_path, 'wb'))
    pickle.dump(frame_rate_hist, open(save_path, 'wb'))


def create_msvd_video_repetition_dict():
    filepath = '/run/media/nbravo/nbravo-2TB/MSVD/corpus_english_clean.csv'
    df = pd.read_csv(filepath)
    videos = df['VideoID']
    video_count = np.unique(videos, return_counts=True)
    video_repetition_dict = {}
    for video_id, c in zip(video_count[0], video_count[1]):
        video_repetition_dict[video_id] = c
    pickle.dump(video_repetition_dict, open('video_repetition_msvd.pkl', 'wb'))


def create_msr_video_repetition_dict():
    filepath = '/home/nbravo/data/MSR-VTT/train_val_videodatainfo.json'
    msr = json.load(open(filepath, 'r'))
    video_repetition_dict = {}
    for sentence in msr['sentences']:
        if sentence['video_id'] in video_repetition_dict.keys():
            video_repetition_dict[sentence['video_id']] += 1
        else:
            video_repetition_dict[sentence['video_id']] = 1
    pickle.dump(video_repetition_dict, open('video_repetition_msr.pkl', 'wb'))


def plot_hist():
    framerate_hist = []
    framerate_hist.append(pickle.load(open('frame_rate_hist_msr.pkl', 'rb')))
    framerate_hist.append(pickle.load(open('frame_rate_hist_msvd.pkl', 'rb')))
    framerate_hist.append(pickle.load(open('frame_rate_hist_tgif.pkl', 'rb')))
    s = 0
    for fh in framerate_hist:
        for k in fh.keys():
            if k > 60:
                fh[60] = fh[k]
                fh.pop(k, None)
        s += sum(fh.values())
    fig = plt.figure()
    ax = fig.add_subplot(111)
    x = []
    for index, fh in enumerate(framerate_hist):
        values = []
        for framerate in fh.keys():
            for i in range(fh[framerate]):
                values.append(framerate)
        #values = fh.keys()fh.values()
        #values = list(map(lambda x: 100 * x / s, values))
        x.append(values)
    ax.hist(x, bins=[0, 5, 10, 15, 20, 25, 30], range=[0, 60], align='left')
    #ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    #ax.set_xlim(1,60)
    ax.set_xticklabels(['0-4', '5-9', '10-14', '15-19', '20-24', '25-29', '30 or more'])
    ax.set_xlabel('Framerate [frames per second]')
    ax.set_ylabel('Number of videos')
    ax.legend(['MSR-VTT', 'MSVD', 'TGIF'])
    ax.grid(axis='y')
    plt.show()
    fig.savefig('framerate_hist.svg', format='svg')


if __name__ == '__main__':
    #frame_rate_dataset('/run/media/nbravo/nbravo-2TB/MSVD/videos/', 'video_repetition_msvd.pkl', 'frame_rate_hist_msvd.pkl')
    #frame_rate_dataset('/home/nbravo/data/MSR-VTT/TrainValVideo/', 'video_repetition_msr.pkl', 'frame_rate_hist_msr.pkl')
    plot_hist()