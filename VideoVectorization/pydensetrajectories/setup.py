from distutils.core import setup, Extension

module1 = Extension("DenseTrackCPU",
	include_dirs = ["/usr/lib/include"],
	libraries = ["opencv_core", "opencv_highgui", "opencv_videoio", "opencv_imgproc", "avformat", "avdevice", "avutil", "avcodec", "swscale"],
	sources = ["DenseTrack.cpp"])


setup (name = "DenseTrackCPU",
	version = "1.0",
	description = "Generate dense trajectories with CPU",
	author = "",
	url = "",
	ext_modules = [module1]) 
