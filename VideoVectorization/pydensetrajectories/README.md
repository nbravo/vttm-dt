# pydensetrajectories

Python library to generate dense trajectories from a video

[Paper](https://hal.inria.fr/inria-00583818/document)