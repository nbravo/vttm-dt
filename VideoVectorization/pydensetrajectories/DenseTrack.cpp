#include <Python.h>
#include "DenseTrack.h"
#include "Initialize.h"
#include "Descriptors.h"
#include "OpticalFlow.h"

#include <time.h>
#include <ctime>

using namespace cv;

int show_track = 0; // set show_track = 1, if you want to visualize the trajectories

std::vector<std::vector<float>> calculate_dense_trajectories(char* video_path)
{
	VideoCapture capture;
	char* video = video_path;
	capture.open(video);

    std::vector<std::vector<float>> dense_trajectories;

    if(!capture.isOpened()) {
		fprintf(stderr, "Could not initialize capturing..\n");
		return dense_trajectories;
	}

	int frame_num = 0;
	TrackInfo trackInfo;

	InitTrackInfo(&trackInfo, track_length, init_gap);

	SeqInfo seqInfo;
	InitSeqInfo(&seqInfo, video);

//	fprintf(stderr, "video size, length: %d, width: %d, height: %d\n", seqInfo.length, seqInfo.width, seqInfo.height);

	Mat image, prev_grey, grey;

	std::vector<float> fscales(0);
	std::vector<Size> sizes(0);

	std::vector<Mat> prev_grey_pyr(0), grey_pyr(0), flow_pyr(0);
	std::vector<Mat> prev_poly_pyr(0), poly_pyr(0); // for optical flow

	std::vector<std::list<Track> > xyScaleTracks;
	int init_counter = 0; // indicate when to detect new feature points
	while(true) {
		Mat frame;
		int i, j, c;

		// get a new frame
		capture >> frame;
		if(frame.empty())
			break;

		if(frame_num < start_frame || frame_num > end_frame) {
			frame_num++;
			continue;
		}

		if(frame_num == start_frame) {
			image.create(frame.size(), CV_8UC3);
			grey.create(frame.size(), CV_8UC1);
			prev_grey.create(frame.size(), CV_8UC1);

			InitPry(frame, fscales, sizes);

			BuildPry(sizes, CV_8UC1, prev_grey_pyr);
			BuildPry(sizes, CV_8UC1, grey_pyr);

			BuildPry(sizes, CV_32FC2, flow_pyr);
			BuildPry(sizes, CV_32FC(5), prev_poly_pyr);
			BuildPry(sizes, CV_32FC(5), poly_pyr);

			xyScaleTracks.resize(scale_num);

			frame.copyTo(image);
			cvtColor(image, prev_grey, COLOR_BGR2GRAY);

			for(int iScale = 0; iScale < scale_num; iScale++) {
				if(iScale == 0)
					prev_grey.copyTo(prev_grey_pyr[0]);
				else
					resize(prev_grey_pyr[iScale-1], prev_grey_pyr[iScale], prev_grey_pyr[iScale].size(), 0, 0, INTER_LINEAR);

				// dense sampling feature points
				std::vector<Point2f> points(0);
				DenseSample(prev_grey_pyr[iScale], points, quality, min_distance);

				// save the feature points
				std::list<Track>& tracks = xyScaleTracks[iScale];
				for(i = 0; i < points.size(); i++)
					tracks.push_back(Track(points[i], trackInfo));
			}

			// compute polynomial expansion
            my::FarnebackPolyExpPyr(prev_grey, prev_poly_pyr, fscales, 7, 1.5);

			frame_num++;
			continue;
		}

		init_counter++;
		frame.copyTo(image);
		cvtColor(image, grey, COLOR_BGR2GRAY);

		// compute optical flow for all scales once
		my::FarnebackPolyExpPyr(grey, poly_pyr, fscales, 7, 1.5);
		my::calcOpticalFlowFarneback(prev_poly_pyr, poly_pyr, flow_pyr, 10, 2);

        for(int iScale = 0; iScale < scale_num; iScale++) {
            if(iScale == 0)
				grey.copyTo(grey_pyr[0]);
			else
				resize(grey_pyr[iScale-1], grey_pyr[iScale], grey_pyr[iScale].size(), 0, 0, INTER_LINEAR);

			int width = grey_pyr[iScale].cols;
			int height = grey_pyr[iScale].rows;

			// track feature points in each scale separately
			std::list<Track>& tracks = xyScaleTracks[iScale];
			for (std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end();) {
				int index = iTrack->index;
				Point2f prev_point = iTrack->point[index];
				int x = std::min<int>(std::max<int>(cvRound(prev_point.x), 0), width-1);
				int y = std::min<int>(std::max<int>(cvRound(prev_point.y), 0), height-1);

				Point2f point;
				point.x = prev_point.x + flow_pyr[iScale].ptr<float>(y)[2*x];
				point.y = prev_point.y + flow_pyr[iScale].ptr<float>(y)[2*x+1];

				if(point.x <= 0 || point.x >= width || point.y <= 0 || point.y >= height) {
					iTrack = tracks.erase(iTrack);
					continue;
				}

				// get the descriptors for the feature point
				iTrack->addPoint(point);


				// if the trajectory achieves the maximal length
				if(iTrack->index >= trackInfo.length) {
					std::vector<Point2f> trajectory(trackInfo.length+1);
					for(int i = 0; i <= trackInfo.length; ++i)
						trajectory[i] = iTrack->point[i]*fscales[iScale];

					float mean_x(0), mean_y(0), var_x(0), var_y(0), length(0);
					if(IsValid(trajectory, mean_x, mean_y, var_x, var_y, length)) {

                        //Trajectory new_traj;
                        // output the trajectory
                        std::vector<float> new_trajectory;
                        for (int i = 0; i < trackInfo.length; ++i) {
                            //printf("%f\t%f\t", trajectory[i].x,trajectory[i].y);
                            //new_traj.positions[2 * i] = trajectory[i].x;
                            //new_traj.positions[2 * i + 1] = trajectory[i].y;
                            new_trajectory.push_back(trajectory[i].x);
                            new_trajectory.push_back(trajectory[i].y);
                        }
						dense_trajectories.push_back(new_trajectory);

						//printf("\n");
					}

					iTrack = tracks.erase(iTrack);
					continue;
				}
				++iTrack;
            }

			if(init_counter != trackInfo.gap)
				continue;

			// detect new feature points every initGap frames
			std::vector<Point2f> points(0);
			for(std::list<Track>::iterator iTrack = tracks.begin(); iTrack != tracks.end(); iTrack++)
				points.push_back(iTrack->point[iTrack->index]);

			DenseSample(grey_pyr[iScale], points, quality, min_distance);
			// save the new feature points
			for(i = 0; i < points.size(); i++)
				tracks.push_back(Track(points[i], trackInfo));
		}

		init_counter = 0;
		grey.copyTo(prev_grey);
		for(i = 0; i < scale_num; i++) {
			grey_pyr[i].copyTo(prev_grey_pyr[i]);
			poly_pyr[i].copyTo(prev_poly_pyr[i]);
		}

		frame_num++;
	}

	return dense_trajectories;
}

PyObject *vectorToList_Float(const std::vector<float> &data) {
	PyObject* listObj = PyList_New(data.size());
	for (unsigned int i = 0; i < data.size(); i++) {
		PyObject *num = PyFloat_FromDouble((double) data[i]);
		PyList_SET_ITEM(listObj, i, num);
	}
	return listObj;
}


PyObject* vectorToTuple_Float(const std::vector<float> &data) {
	PyObject* tuple = PyTuple_New( data.size() );
	for (unsigned int i = 0; i < data.size(); i++) {
		PyObject *num = PyFloat_FromDouble( (double) data[i]);
		PyTuple_SET_ITEM(tuple, i, num);
	}

	return tuple;
}



PyObject* vectorVectorToTuple_Float(const std::vector< std::vector< float > > &data) {
	PyObject* tuple = PyTuple_New( data.size() );
	for (unsigned int i = 0; i < data.size(); i++) {
		PyObject* subTuple = NULL;
		subTuple = vectorToTuple_Float(data[i]);
		PyTuple_SET_ITEM(tuple, i, subTuple);
	}
	return tuple;
}


static PyObject* DenseTrack_generate(PyObject* self, PyObject *args) {
	char *video_path;

	if (!PyArg_ParseTuple(args, "s", &video_path)) {
		return NULL;
	}

	std::vector<std::vector<float>> trajectories = calculate_dense_trajectories(video_path);
	PyObject *py_trajectories = vectorVectorToTuple_Float(trajectories);

	return py_trajectories;
}

static PyMethodDef DenseTrack_methods[] = {
	//"PythonName"      C-Function name     argument presentation     description
	{"generate", DenseTrack_generate, METH_VARARGS, "Generate dense trajectories"},
	{NULL, NULL, 0, NULL}   //sentinel
};

static struct PyModuleDef densetrack =
{
    PyModuleDef_HEAD_INIT,
    "DenseTrackCPU",
    "",
    -1,          
    DenseTrack_methods
};

PyMODINIT_FUNC PyInit_DenseTrackCPU(void) {
	return PyModule_Create(&densetrack);
}
