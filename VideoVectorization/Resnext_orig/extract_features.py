import mxnet as mx
from collections import namedtuple
import numpy as np
import cv2.cv2 as cv2
import os
import pickle

IMG_SIZE = 256
CROP_SIZE = 224
DEVICE_ID = 0

Batch = namedtuple('Batch', ['data'])


def get_feat_extractor(batch_size=1, oversample=True):
    layer = "flatten0_output"
    batch_size = batch_size*10 if oversample else batch_size
    sym, arg_params, aux_params = mx.model.load_checkpoint("mxmodels80/resnext-101_rbps13k_step_3_aug_1_dist_3x2/resnext-101-1", 40)
    all_layers = sym.get_internals()
    fe_sym = all_layers[layer]
    fe_mod = mx.mod.Module(symbol=fe_sym, context=mx.gpu(DEVICE_ID), label_names=None)
    fe_mod.bind(for_training=False, data_shapes=[('data', (batch_size, 3, CROP_SIZE, CROP_SIZE))])
    fe_mod.set_params(arg_params, aux_params)
    return fe_mod


def extract_feature(model, img_buffer, sub_mean, oversample):
    img = mx.image.imdecode(img_buffer)
    mxnet_in = preprocess_images([img], sub_mean=sub_mean, oversample=oversample)
    model.forward(mxnet_in)
    features = model.get_outputs()[0].asnumpy()
    if oversample:
        features = features.reshape((len(features)//10, 10,-1)).mean(1)
    return features


def img_oversample(raw_img, width=IMG_SIZE, height=IMG_SIZE, crop_dims=CROP_SIZE):
    cropped_image, _ = mx.image.center_crop(raw_img, (crop_dims, crop_dims))
    cropped_image_1 = mx.image.fixed_crop(raw_img, 0, 0, crop_dims, crop_dims)
    cropped_image_2 = mx.image.fixed_crop(raw_img, 0, height-crop_dims, crop_dims, crop_dims)
    cropped_image_3 = mx.image.fixed_crop(raw_img, width-crop_dims, 0, crop_dims, crop_dims)
    cropped_image_4 = mx.image.fixed_crop(raw_img, width-crop_dims, height-crop_dims, crop_dims, crop_dims)
    img_list = [cropped_image.asnumpy(), cropped_image_1.asnumpy(), cropped_image_2.asnumpy(),
                cropped_image_3.asnumpy(), cropped_image_4.asnumpy()]
    return img_list


def preprocess_images(inputs, width=IMG_SIZE, height=IMG_SIZE, crop_dims=CROP_SIZE, sub_mean=False, oversample=True):
    # Scale to standardize input dimensions.
    input_ = []

    for ix, in_ in enumerate(inputs):
        raw_img = mx.image.imresize(in_, width, height)
        if sub_mean:
            raw_img = mx.image.color_normalize(raw_img.astype(np.float32), mean= mx.nd.array([123.68, 116.779, 103.939]))
        if oversample:
            # Generate center, corner, and mirrored crops.
            input_.extend(img_oversample(raw_img, width, height, crop_dims))
            input_.extend(img_oversample(mx.nd.flip(raw_img, axis=1), width, height, crop_dims))
        else:
            cropped_image, _ = mx.image.center_crop(raw_img, (crop_dims, crop_dims))
            input_.append(cropped_image.asnumpy())
    input_ = mx.nd.array(input_)
    input_ = mx.nd.swapaxes(input_, 1, 3)
    input_ = mx.nd.swapaxes(input_, 2, 3)
    return Batch([input_])


def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    single_image = False
    if frame_count == 0:
        return []
    if fps < 1 or frame_count < 0:
        single_image = True
    ret, f = cap.read()
    is_success, buff = cv2.imencode(".jpg", f)
    if not is_success:
        return []
    if single_image:
        return [buff]
    index = 0
    while not ret:
        ret, f = cap.read()
        index += 1
    is_success, buff = cv2.imencode(".jpg", f)
    if not is_success:
        return []
    fs = [buff]
    while ret:
        ret, f = cap.read()
        index += 1
        if ret and (index % fps == 0 or index % fps == (fps // 2)):
            is_success, buff = cv2.imencode(".jpg", f)
            fs.append(buff)
    cap.release()
    return fs


def msvd(fe_mod, sub_mean, oversample):
    video_dir = "/run/media/nbravo/nbravo-2TB/MSVD/videos/"
    save_dir = "/run/media/nbravo/nbravo-2TB/MSVD/resnext101/embeddings_original/"
    videos = os.listdir(video_dir)
    i = 0
    for v in videos:
        video_path = video_dir + v
        frames = get_video_frames(video_path)
        features = []
        for index, f in frames:
            feature = extract_feature(fe_mod, f, sub_mean, oversample)
            features.append(feature[0])
        features = np.array(features)
        pickle.dump(features, open(save_dir + v.split(".")[0] + ".pkl", 'wb'))
        i += 1
        print("MSVD", i, "of", len(videos), "done")


def msr(fe_mod, sub_mean, oversample):
    video_dir = "/home/nbravo/data/MSR-VTT/TrainValVideo/"
    save_dir = "/run/media/nbravo/Elements/MSR_all/resnext101/embeddings_original/"
    videos = os.listdir(video_dir)
    i = 0
    for v in videos:
        video_path = video_dir + v
        frames = get_video_frames(video_path)
        features = []
        for index, f in frames:
            feature = extract_feature(fe_mod, f, sub_mean, oversample)
            features.append(feature[0])
        features = np.array(features)
        pickle.dump(features, open(save_dir + v.split(".")[0] + ".pkl", 'wb'))
        i += 1
        print("MSR", i, "of", len(videos), "done")


def tgif(fe_mod, sub_mean, oversample):
    video_dir = "/run/media/nbravo/nbravo-2TB/TGIF/gifs/"
    save_dir = "/run/media/nbravo/Elements/TGIF/resnext101/embeddings_original/"
    videos = os.listdir(video_dir)
    init = 55407
    i = init
    for v in videos[init:60000]:
        video_path = video_dir + v
        frames = get_video_frames(video_path)
        features = []
        for f in frames:
            feature = extract_feature(fe_mod, f, sub_mean, oversample)
            features.append(feature[0])
        features = np.array(features)
        pickle.dump(features, open(save_dir + v.split(".")[0] + ".pkl", 'wb'))
        i += 1
        print("TGIF", i, "of", len(videos), "done")


def vtt2016(fe_mod, sub_mean, oversample):
    video_dir = "/run/media/nbravo/Elements/VTT2016/videos/"
    save_dir = "/run/media/nbravo/Elements/VTT2016/resnext101/embeddings_original/"
    videos = os.listdir(video_dir)
    init = 0
    i = init
    for v in videos[init:]:
        video_path = video_dir + v
        frames = get_video_frames(video_path)
        features = []
        for f in frames:
            feature = extract_feature(fe_mod, f, sub_mean, oversample)
            features.append(feature[0])
        features = np.array(features)
        pickle.dump(features, open(save_dir + v.split(".")[0] + ".pkl", 'wb'))
        i += 1
        print("VTT2016", i, "of", len(videos), "done")


def vtt2018(fe_mod, sub_mean, oversample):
    video_dir = "/run/media/nbravo/nbravo-2TB/VTT2018/videos/"
    save_dir = "/run/media/nbravo/nbravo-2TB/VTT2018/resnext101_originals/"
    videos = os.listdir(video_dir)
    init = 0
    i = init
    for v in videos[init:]:
        video_path = video_dir + v
        frames = get_video_frames(video_path)
        features = []
        for f in frames:
            feature = extract_feature(fe_mod, f, sub_mean, oversample)
            features.append(feature[0])
        features = np.array(features)
        pickle.dump(features, open(save_dir + v.split(".")[0] + ".pkl", 'wb'))
        i += 1
        print("VTT2016", i, "of", len(videos), "done")




if __name__ == "__main__":
    model_prefix = "mxmodels80/resnext-101_rbps13k_step_3_aug_1_dist_3x2/resnext-101-1"
    oversample = True
    sub_mean = True
    fe_mod = get_feat_extractor(oversample=oversample)
    vtt2018(fe_mod, sub_mean, oversample)
