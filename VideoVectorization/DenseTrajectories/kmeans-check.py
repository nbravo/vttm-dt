import matplotlib.pyplot as plt
import matplotlib
import VideoVectorization.DenseTrajectories.utils as utils
import numpy as np


def derivate(clusters, kmeans):
    for i in range(1, len(clusters)):
            d = (kmeans[i].inertia_ - kmeans[i-1].inertia_) / (len(kmeans[i].cluster_centers_) - len(kmeans[i-1].cluster_centers_))
            print(clusters[i], d)

def plot(clusters, inertias, axis="x"):
    plt.plot(clusters, inertias)
    plt.xlim(200, 8000)
    #plt.gca().set_aspect("equal", adjustable="box")
    plt.show()
    #plt.savefig("plot" + axis + ".png")



if __name__ == '__main__':
    matplotlib.use('PS')
    kmeans_x = []
    kmeans_y = []
    clusters = list(range(200, 8001, 200))
    for c in clusters:
        kmeans_x.append(utils.read_kmeans_xy(c))
        kmeans_y.append(utils.read_kmeans_xy(c, y=True))


    # print("X")
    # derivate(clusters, kmeans_x)
    # print("Y")
    # derivate(clusters, kmeans_y)

    x = np.array(clusters)
    plt.xlim(200, 8000)
    plt.plot(clusters, [kx.inertia_ for kx in kmeans_x])
    plt.plot(clusters, [ky.inertia_ for ky in kmeans_y])
    plt.axvline(x=2500)
    plt.grid(True)
    plt.xlabel("Number of clusters")
    plt.ylabel("Total squared error")
    plt.legend(["DT X clustering", "DT Y clustering"])
    plt.title("Clustering error for X and Y Dense Trajectories")
    plt.savefig("kmeans.eps", format='eps')

    #plot(clusters, [ky.inertia_ for ky in kmeans_y], axis="y")

    # for x, y in [(kx.n_clusters ,kx.inertia_) for kx in kmeans_y]:
    #     print(x,y)

    # import os
    # import numpy as np
    # from sklearn.cluster import MiniBatchKMeans
    # from sklearn.metrics import silhouette_score

    # DATASET_PATH = "/run/media/nbravo/Elements/MSR_all/"
    # DTS_LIST = os.listdir(DATASET_PATH + "dts/")
    # N_VIDEOS = len(DTS_LIST)
    # kmeans_x = utils.read_kmeans_xy(2000)
    # N_SAMPLE = N_VIDEOS // 10
    # random_dts = utils.read_object("random_" + str(N_SAMPLE) + "_dts")
    #
    # dts_x = []
    # j = 0
    # for i in random_dts:
    #     dts = utils.read_dt(DATASET_PATH + "dts/" + DTS_LIST[i])
    #     if len(dts) == 0:
    #         continue
    #     dts = np.array(dts)
    #     dts_x = dts_x + list(dts[:, 0::2])
    #     j += 1
    #     print(j, "of", N_SAMPLE)
    #
    # # print("Predicting")
    # # cluster_labels = kmeans_x.predict(dts_x)
    # print("Calculating score")
    # score = kmeans_x.score(dts_x)
    # print(score)