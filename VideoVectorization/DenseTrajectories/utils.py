import pickle


def read_dt(file_path):
    with open(file_path, "rb") as fin:
        dts = pickle.load(fin)
    return list(dts)


def save_kmeans_xy(kmeans, cluster_size, y=False):
    if not y:
        with open("kmeans/xy/kmeans_x_" + str(cluster_size) +  ".pkl", 'wb') as fp:
            pickle.dump(kmeans, fp)
    else:
        with open("kmeans/xy/kmeans_y_" + str(cluster_size) + ".pkl", 'wb') as fp:
            pickle.dump(kmeans, fp)

def read_kmeans_xy(cluster_size, y=False):
    if not y:
        with open("kmeans/xy/kmeans_x_" + str(cluster_size) + ".pkl", 'rb') as fp:
            return pickle.load(fp)
    else:
        with open("kmeans/xy/kmeans_y_" + str(cluster_size) + ".pkl", 'rb') as fp:
            return pickle.load(fp)


def save_object(name, obj):
    with open("objects/" + name + ".pkl", "wb") as fp:
        pickle.dump(obj, fp)


def read_object(name):
    with open("objects/" + name + ".pkl" , "rb") as fp:
        obj = pickle.load(fp)
    return obj
