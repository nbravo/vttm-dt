import DenseTrack
import DenseTrackCPU
import os
import random
import pickle
import time
import numpy as np

VIDEO_PATH = "/home/nbravo/data/MSR-VTT/TrainValVideo/"
SAMPLE_PATH = "objects/dt_test_video_sample_list_2.pkl"


def generate_sample(n=100):
    d = np.array(os.listdir(VIDEO_PATH))
    s = random.sample(range(len(d)), n)
    s = d[s]
    pickle.dump(s, open(SAMPLE_PATH, "wb"))
    print("Sample saved")


def test_dt_cuda_generation():
    sample = pickle.load(open(SAMPLE_PATH, "rb"))
    n = len(sample)
    t_start = time.time()
    t_init = t_start
    for i in range(len(sample)):
        DenseTrack.generate(VIDEO_PATH + sample[i])
        t_finish = time.time()
        print(i, t_finish - t_init)
        t_init = t_finish
    total_time = time.time() - t_start
    print("Total time:", total_time)


def test_dt_cpu_generation():
    sample = pickle.load(open(SAMPLE_PATH, "rb"))
    n = len(sample)
    t_start = time.time()
    t_init = t_start
    for i in range(len(sample)):
        DenseTrackCPU.generate(VIDEO_PATH + sample[i])
        t_finish = time.time()
        print(i, t_finish - t_init)
        t_init = t_finish
    total_time = time.time() - t_start
    print("Total time:", total_time)


if __name__ == "__main__":
    #test_dt_cpu_generation()
    test_dt_cuda_generation()
