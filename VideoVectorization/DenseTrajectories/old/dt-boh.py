import utils
import numpy as np
import sys
import gc
import logging

np.set_printoptions(threshold=sys.maxsize)

n_videos = 125786
n_clusters = 800
E = 2   # Number of subspaces
d = 30  # Dimension of a dense trajectories vector


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
fg = logging.FileHandler("log/boh_1.log")
fg.setLevel(logging.DEBUG)
logger.addHandler(fg)


def calc_max_radii(max_r, dense_trajs, clustering):
    pred = clustering.predict(dense_trajs)
    for i in range(0,len(pred)):
        r = utils.euclidian(clustering.cluster_centers_[pred[i]], dense_trajs[i])
        if r > max_r[pred[i]]:
            max_r[pred[i]] = r
    return max_r


def calc_all_max_radii():
    max_R  = [0] * n_clusters
    kmeans = utils.read_kmeans(124)
    for k in range(0, n_videos, 1000):
        logger.debug(k)
        dts = []
        gc.collect()
        for i in range(k, k + 1000):
            try:
                dts = dts + utils.read_dt("../TGIF/dts/" + str(i) + ".gif.json.gz")
            except KeyError:
                continue
        max_R = calc_max_radii(max_R, dts, kmeans)
        utils.save_object("max_radii_" + str(k), max_R)



if __name__ == '__main__':

    #calc_all_max_radii()
    #exit(0)


    kmeans = utils.read_kmeans(124)
    Max_R = utils.read_object("max_radii_124000") 
    fvs = []
    inter = [0] * (E + 1)
    inter[E] = 1
    fv = np.full((n_clusters * E), 0, dtype=int)

    for k in range(1, E):
        inter[k] = (k / E) ** (1 / d)
    
    for k in range(0, n_videos, 1000):
        logger.debug(k)


        dts = []
        for i in range(k, k + 1000):
            dts = dts + utils.read_dt("../TGIF/dts/" + str(i) + ".gif.json.gz")

        pred = kmeans.predict(dts)
    
        fv = np.full((n_clusters * E), 0, dtype=int)
        for i in range(len(pred)):
            r = utils.euclidian(kmeans.cluster_centers_[pred[i]], dts[i])
            for p in range(0, E):
                if inter[p] * Max_R[pred[i]] < r <= inter[p + 1] * Max_R[pred[i]]:
                    fv[pred[i] * E + p] = fv[pred[i] * E + p] + 1

        utils.save_object("boh_" + str(k), fv)



