import gzip
import json
import pickle

DEV_PATH = '/home/nbravo/vttm-dt/'
DATASET = 'TGIF/'


def euclidian(a, b):
    distance = 0.00000
    for i in range(len(a)):
        distance += (a[i] - b[i]) * (a[i] - b[i])
    return distance ** (1 / 2)


def read_dt(file_path):
    with gzip.GzipFile(file_path, "r") as fin:
        dts = fin.read()
    return json.loads(dts.decode('utf-8'))


def save_kmeans(kmeans, i, y=False):
    with open("kmeans/kmeans" + str(i) + ".pkl", 'wb') as fp:
        pickle.dump(kmeans, fp)


def save_kmeans_xy(kmeans, cluster_size, i, y=False):
    if not y:
        with open("kmeans/kmeans_x_" + str(cluster_size) + "_" + str(i) + ".pkl", 'wb') as fp:
            pickle.dump(kmeans, fp)
    else:
        with open("kmeans/kmeans_y_" + str(cluster_size) + "_" + str(i) + ".pkl", 'wb') as fp:
            pickle.dump(kmeans, fp)


def read_kmeans(i):
    with open("kmeans/kmeans" + str(i) + ".pkl", 'rb') as fp:
        kmeans = pickle.load(fp)

    return kmeans


def save_pca(pca, i):
    with open("pca/pca" + str(i) + ".pkl", 'wb') as fp:
        pickle.dump(pca, fp)


def read_pca(i):
    with open("pca/pca" + str(i) + ".pkl", 'rb') as fp:
        pca = pickle.load(fp)

    return pca


def save_object(name, obj):
    with open("objects/" + name + ".pkl", "wb") as fp:
        pickle.dump(obj, fp)


def read_object(name):
    with open("objects/" + name + ".pkl" , "rb") as fp:
        obj = pickle.load(fp)
    return obj
