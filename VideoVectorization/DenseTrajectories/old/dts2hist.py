import gzip
import json
import pickle

CLUSTER_SIZE=800
n = 1880
PATH_LOAD = '/run/media/nbravo/nbravo-2TB/VTT2017/dts/{}.mp4.json.gz'
PATH_SAVE = '/run/media/nbravo/nbravo-2TB/VTT2017/dts_hist/{}.mp4.json'


def read_kmeans(i):
    with open("objects/kmeans" + str(i) + ".pkl", 'rb') as fin:
        kmeans = pickle.load(fin)
    return kmeans


def read_dt(i, clustering):
    with gzip.GzipFile(PATH_LOAD.format(i), "r") as fin:
        dts = fin.read()
    dts = json.loads(dts.decode('utf-8'))
    dt_histogram = [0] * CLUSTER_SIZE
    if len(dts) == 0:
        return dt_histogram
    classes = clustering.predict(dts)
    for c in classes:
        dt_histogram[c] += 1
    return dt_histogram


def save_dt_hist(dt_hist):
    with open(PATH_SAVE.format(i), 'w') as fout:
        json.dump(dt_hist, fout)


if __name__ == "__main__":
    clustering = read_kmeans(124)
    clustering.verbose = False
    for i in range(1, 1881):
        dt_hist = read_dt(i, clustering)
        save_dt_hist(dt_hist)
        if i % 100 == 0:
            print(i, "loaded successfully")
