import utils
import gc
import random
import logging
from sklearn.decomposition import IncrementalPCA

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
fg = logging.FileHandler("log/pca_1.log")
fg.setLevel(logging.DEBUG)
logger.addHandler(fg)

n_videos = 125782
initial_dim = 800
final_dim = 220
boh_batch_size = 1000


def boh_hist(dt, clustering):
    if len(dt) == 0:
        return None
    boh_histogram = [0] * initial_dim
    classes = clustering.predict(dt)
    for c in classes:
        boh_histogram[c] += 1
    return boh_histogram


def initial_pca():
    random_all = random.sample(range(0, n_videos), n_videos)
    utils.save_object("random_dts_for_boh", random_all)
    logger.info("Random dts for boh saved")
    kmeans = utils.read_kmeans(124)
    kmeans.verbose = False

    bohs = []
    for i in range(0, boh_batch_size):
        dt = utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(random_all[i]) + ".gif.json.gz")
        hist = boh_hist(dt, kmeans)
        if hist is None:
            continue
        bohs = bohs + [hist]

    pca = IncrementalPCA(n_components=220, batch_size=boh_batch_size)
    pca.fit_transform(bohs)
    utils.save_pca(pca, 0)
    logger.info("Initial PCA done!")


def complete_pca():
    random_all_dts = utils.read_object("random_dts_for_boh")
    #random_all_dts = random.sample(range(0, n_videos), n_videos)
    #utils.save_object("random_all_dts_3", random_all_dts)
    kmeans = utils.read_kmeans(124)
    kmeans.verbose = False
    pca = utils.read_pca(0)
    for k in range(1, 10):
        logger.debug("K = " + str(k))
        bohs = []
        gc.collect()
        bohs = []
        for i in range(k * boh_batch_size, (k + 1) * boh_batch_size):
            dt = utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(random_all_dts[i]) + ".gif.json.gz")
            hist = boh_hist(dt, kmeans)
            if hist is None:
                continue
            bohs.append(hist)

        pca = pca.partial_fit(bohs)
        utils.save_pca(pca, k)
        logger.info("Pca " + str(k) + " saved!")


if __name__ == '__main__':
    #initial_pca()
    complete_pca()
