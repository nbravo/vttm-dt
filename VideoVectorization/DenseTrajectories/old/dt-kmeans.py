import utils as utils
import gc
import random
import logging
from sklearn.cluster import MiniBatchKMeans

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
fg = logging.FileHandler("log/all_clustering_3.log")
fg.setLevel(logging.DEBUG)
logger.addHandler(fg)

N_VIDEOS = 125782
CLUSTER_SIZE = 800
DT_BATCH_SIZE = 1000
CLUSTERING_BATCH_SIZE = 20000


def test_cluster_numbers():
    clusters = list(range(100, 2001, 100))
    random_dts = random.sample(range(1, N_VIDEOS), DT_BATCH_SIZE)
    dt = []
    
    for i in random_dts:
        dt = dt + utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(i) + ".gif.json.gz")
        print(i)
        logger.debug(i)

    logger.info("Len dts: " + str(len(dt)))
    
    for i in range(0, len(clusters)):
        logger.debug("Clusters " + str(clusters[i]))
        kmeans = MiniBatchKMeans(n_clusters=clusters[i], batch_size=20000, n_init=10, compute_labels=True, verbose=True)
        kmeans.fit(dt)
        utils.save_kmeans(kmeans, i)


def test_cluster_numbers_xy():
    clusters_sizes = list(range(100, 2001, 100))
    random_dts = random.sample(range(1, N_VIDEOS), 10000)
    utils.save_object("random_10k_dts", random_dts)
    kmeans_x = []
    kmeans_y = []
    for i in range(0, len(clusters_sizes)):
        kmeans_x.append(MiniBatchKMeans(n_clusters=clusters_sizes[i], batch_size=20000, n_init=10, compute_labels=False, verbose=True))
        kmeans_y.append(MiniBatchKMeans(n_clusters=clusters_sizes[i], batch_size=20000, n_init=10, compute_labels=False, verbose=True))

    dts_x = []
    dts_y = []

    for i in random_dts[:1000]:
        dts = utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(i) + ".gif.json.gz")
        dts_x = dts_x + dts[0::2]
        dts_y = dts_y + dts[1::2]
        print(i)
        logger.debug(i)
    dts = None
    gc.collect()

    logger.info("Len dts X: " + str(len(dts_x)))
    logger.info("Len dts Y: " + str(len(dts_y)))


    for i in range(0, len(clusters_sizes)):
        logger.debug("Clusters " + str(clusters_sizes[i]))
        km_x = kmeans_y[i]
        km_y = kmeans_y[i]

        km_x.fit(dts_x)
        km_y.fit(dts_y)
        utils.save_kmeans_xy(km_x, clusters_sizes[i], i)
        utils.save_kmeans_xy(km_y, clusters_sizes[i], i, y=True)


def initial_clustering():
    random_all_dts = random.sample(range(0, N_VIDEOS), N_VIDEOS)
    utils.save_object("random_all_dts", random_all_dts)
    logger.info("Random dts saved")

    dt = []
    for i in range(0, 1000):
        dt = dt + utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(random_all_dts[i]) + ".gif.json.gz")
        logger.debug(random_all_dts[i])

    kmeans = MiniBatchKMeans(n_clusters=CLUSTER_SIZE, batch_size=CLUSTERING_BATCH_SIZE, n_init=10, compute_labels=True, verbose=True)
    kmeans.fit(dt)
    utils.save_kmeans(kmeans, 0)
    logger.info("Initial clustering done!")


def complete_clustering():
    #random_all_dts = utils.read_object("random_all_dts")
    random_all_dts = random.sample(range(100000, N_VIDEOS), 25782)
    utils.save_object("random_all_dts_2", random_all_dts)
    for k in range(0, 26):
        logger.debug("K = " + str(k))
        dt = None
        gc.collect()
        dt = []
        for i in range(k*1000, (k+1)*1000):
            dt = dt + utils.read_dt(utils.DEV_PATH + utils.DATASET + "dts/" + str(random_all_dts[i]) + ".gif.json.gz")
            logger.debug(random_all_dts[i])
        kmeans = utils.read_kmeans((k + 100) - 1)
    
        i = 0
        while i < len(dt):
            kmeans.partial_fit(dt[i:i + CLUSTERING_BATCH_SIZE])
            i += CLUSTERING_BATCH_SIZE
        utils.save_kmeans(kmeans, (k + 100))
        logger.info("Kmeans " + str(k + 100) + " saved!")


if __name__ == '__main__':
    test_cluster_numbers_xy()
