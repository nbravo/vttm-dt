import gzip
import json
import pickle
import pandas as pd
from sys import argv

CLUSTER_SIZE=800
DATASET_PATH = '/run/media/nbravo/Elements/VTT2019/'
PATH_LOAD = DATASET_PATH + 'dts/{}'
PATH_SAVE = DATASET_PATH + 'dts_hist/{}'


def read_kmeans(i):
    with open("objects/kmeans" + str(i) + ".pkl", 'rb') as fin:
        kmeans = pickle.load(fin)
    return kmeans


def read_dt(dt_name, clustering):
    with gzip.GzipFile(PATH_LOAD.format(dt_name), "r") as fin:
        dts = fin.read()
    dts = json.loads(dts.decode('utf-8'))
    dt_histogram = [0] * CLUSTER_SIZE
    if len(dts) == 0:
        return dt_histogram
    classes = clustering.predict(dts)
    for c in classes:
        dt_histogram[c] += 1
    return dt_histogram


def save_dt_hist(dt_name, dt_hist):
    with open(PATH_SAVE.format(dt_name), 'w') as fout:
        json.dump(dt_hist, fout)


if __name__ == "__main__":
    init = 0
    dts_dir = pd.read_csv(DATASET_PATH + "dts_dir.csv").dt_name
    end = len(dts_dir)
    try:
        init = int(argv[1])
        end = int(argv[2])
    except Exception:
        pass
    clustering = read_kmeans(124)
    clustering.verbose = False
    for i in range(init, end):
        dt_hist = read_dt(dts_dir[i], clustering)
        save_dt_hist(dts_dir[i][:-3], dt_hist)
        if i % 100 == 0:
            print(i, "of", len(dts_dir))
