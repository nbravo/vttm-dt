import os
import DenseTrack
import pickle
from sklearn.cluster import MiniBatchKMeans
import numpy as np
import VideoVectorization.DenseTrajectories.utils as utils
import gzip
import json

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/TGIF/gifs/"
DT_PATH = "/run/media/nbravo/nbravo-2TB/TGIF/dts/"
DT_HIST_PATH = "/run/media/nbravo/Elements/TGIF/dt_hist/"
HIST_PATH = "/home/nbravo/data/TGIF/dt_hist/"

videos_list = os.listdir(DATASET_PATH)
N = len(videos_list)


def obtain_dense_trajs():
    for i in range(N):
        video_name = videos_list[i].split(".")[0]
        dt = DenseTrack.generate(DATASET_PATH + videos_list[i])
        with open(DT_PATH + video_name + ".pkl", "wb") as fp:
            pickle.dump(dt, fp)
        print("Done", i, "of", N)


def create_hists():
    kmeans_x = utils.read_kmeans_xy(10, y=False)
    kmeans_x.verbose = False
    kmeans_y = utils.read_kmeans_xy(10, y=True)
    kmeans_y.verbose = False
    init = 80250
    end = N
    for i in range(init, end):
        dt_name = videos_list[i].split(".")[0]
        with gzip.GzipFile(DT_PATH + dt_name + ".gif.json.gz", "r") as fin:
            dt = fin.read()
            dt = json.loads(dt.decode('utf-8'))
            if len(dt) == 0:
                hist_x = np.zeros(2500)
                hist_y = np.zeros(2500)
            else:
                dt = np.array(dt)
                hist_x = kmeans_x.predict(list(dt[:, 0::2]))
                hist_y = kmeans_y.predict(list(dt[:, 1::2]))
            hist = [list(hist_x), list(hist_y)]
            with open(DT_HIST_PATH + dt_name + ".pkl", "wb") as hist_fin:
                pickle.dump(hist, hist_fin)
            print("Done", i, "of", N)


def hists():
    init = 60000
    end = N
    for i in range(init, end):
        dt_name = videos_list[i].split(".")[0]
        with open(DT_HIST_PATH + dt_name + ".pkl", "rb") as fin:
            dt_hist = pickle.load(fin)
            hist_x = np.zeros(2500)
            hist_y = np.zeros(2500)
            if max(dt_hist[0]) > 0:
                for k in dt_hist[0]:
                    hist_x[k] += 1
            if max(dt_hist[1]) > 0:
                for k in dt_hist[1]:
                    hist_y[k] += 1
            hist = [hist_x, hist_y]
            with open(HIST_PATH + dt_name + ".pkl", "wb") as hist_fin:
                pickle.dump(hist, hist_fin)
            if i % 500 == 0:
                print("Done", i, "of", N)

if __name__ == '__main__':
    hists()