import VideoVectorization.DenseTrajectories.utils as utils
import gc
import random
import logging
from sklearn.cluster import MiniBatchKMeans
import os
import numpy as np

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
fg = logging.FileHandler("all_clustering.log")
fg.setLevel(logging.DEBUG)
logger.addHandler(fg)

DATASET_PATH = "/run/media/nbravo/Elements/MSR_all/"

DTS_LIST = os.listdir(DATASET_PATH + "dts/")
N_VIDEOS = len(DTS_LIST)
CLUSTER_SIZE_X = 2500
CLUSTER_SIZE_Y = 2500
DT_BATCH_SIZE = 700
CLUSTERING_BATCH_SIZE = 20000


def test_cluster_numbers_xy():
    clusters_sizes = list(range(200, 1001, 200))
    N_SAMPLE = N_VIDEOS // 10
    #random_dts = random.sample(range(0, N_VIDEOS), N_SAMPLE)
    #utils.save_object("random_" + str(N_SAMPLE) + "_dts", random_dts)
    random_dts = utils.read_object("random_" + str(N_SAMPLE) + "_dts")
    kmeans_x = []
    kmeans_y = []
    for i in range(0, len(clusters_sizes)):
        kmeans_x.append(MiniBatchKMeans(n_clusters=clusters_sizes[i], batch_size=CLUSTERING_BATCH_SIZE, n_init=10, compute_labels=True, verbose=True))
        kmeans_y.append(MiniBatchKMeans(n_clusters=clusters_sizes[i], batch_size=CLUSTERING_BATCH_SIZE, n_init=10, compute_labels=True, verbose=True))

    dts_x = []
    dts_y = []

    j = 0
    for i in random_dts:
        dts = utils.read_dt(DATASET_PATH + "dts/" + DTS_LIST[i])
        if len(dts) == 0:
            continue
        dts = np.array(dts)
        dts_x = dts_x + list(dts[:, 0::2])
        dts_y = dts_y + list(dts[:, 1::2])
        logger.debug(str(j) + ": " + str(i))
        j += 1
    dts = None
    gc.collect()

    logger.info("Len dts X: " + str(len(dts_x)))
    logger.info("Len dts Y: " + str(len(dts_y)))

    for i in range(0, len(clusters_sizes)):
        logger.debug("Clusters " + str(clusters_sizes[i]))
        km_x = kmeans_x[i]
        km_y = kmeans_y[i]
        km_x.fit(dts_x)
        km_y.fit(dts_y)
        utils.save_kmeans_xy(km_x, clusters_sizes[i])
        utils.save_kmeans_xy(km_y, clusters_sizes[i], y=True)


def initial_clustering_xy():
    random_all_dts = random.sample(range(0, N_VIDEOS), N_VIDEOS)
    utils.save_object("random_all_dts_xy", random_all_dts)

    dts_x = []
    dts_y = []
    for  i in range(0, DT_BATCH_SIZE):
        dts = utils.read_dt(DATASET_PATH + "dts/" + DTS_LIST[random_all_dts[i]])
        if len(dts) == 0:
            continue
        dts = np.array(dts)
        dts_x = dts_x + list(dts[:, 0::2])
        dts_y = dts_y + list(dts[:, 1::2])
    kmeans_x = MiniBatchKMeans(n_clusters=CLUSTER_SIZE_X, batch_size=CLUSTERING_BATCH_SIZE, n_init=10, compute_labels=False, verbose=True)
    kmeans_x.fit(dts_x)
    utils.save_kmeans_xy(kmeans_x, 0, y=False)
    kmeans_y = MiniBatchKMeans(n_clusters=CLUSTER_SIZE_Y, batch_size=CLUSTERING_BATCH_SIZE, n_init=10, compute_labels=False, verbose=True)
    kmeans_y.fit(dts_y)
    utils.save_kmeans_xy(kmeans_y, 0, y=True)


def complete_clustering_xy():
    random_all_dts = utils.read_object('random_all_dts_xy')
    i = 0
    for k in range(2, 20):
        if i >= N_VIDEOS:
            break
        print(k)
        dts_x = None
        dts_y = None
        gc.collect()
        dts_x = []
        dts_y = []
        for i in range(k*DT_BATCH_SIZE, (k+1)*DT_BATCH_SIZE):
            try:
                dt_file = DTS_LIST[random_all_dts[i]]
            except IndexError:
                break
            dts = utils.read_dt(DATASET_PATH + "dts/" + dt_file)
            if len(dts) == 0:
                continue
            dts = np.array(dts)
            dts_x = dts_x + list(dts[:, 0::2])
            dts_y = dts_y + list(dts[:, 1::2])
        kmeans_x = utils.read_kmeans_xy(k - 1, y=False)
        kmeans_y = utils.read_kmeans_xy(k - 1, y=True)

        j = 0
        while j < len(dts_x):
            kmeans_x.partial_fit(dts_x[j:j + CLUSTERING_BATCH_SIZE])
            j += CLUSTERING_BATCH_SIZE
        utils.save_kmeans_xy(kmeans_x, k, y=False)

        j = 0
        while j < len(dts_y):
            kmeans_y.partial_fit(dts_y[j:j + CLUSTERING_BATCH_SIZE])
            j += CLUSTERING_BATCH_SIZE
        utils.save_kmeans_xy(kmeans_y, k, y=True)


if __name__ == '__main__':
    complete_clustering_xy()
