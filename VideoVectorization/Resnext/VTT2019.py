import cv2.cv2 as cv2
import torchvision.transforms as transforms
import torch
from torch.autograd import Variable
import os
from PIL import Image
import pretrainedmodels
import pretrainedmodels.utils as utils

DATASET_PATH = "/run/media/nbravo/Elements/VTT2019/"
VIDEOS_PATH = DATASET_PATH + "videos/"


def to_pil(np_array):
    np_array_img = cv2.cvtColor(np_array, cv2.COLOR_BGR2RGB)
    return Image.fromarray(np_array_img)


def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    t_frame = 1.0 / fps

    ret, f = cap.read()
    while not ret:
        ret, f = cap.read()
    fs = [to_pil(f)]
    elapsed_time = t_frame
    while ret:
        ret, f = cap.read()
        elapsed_time += t_frame
        if ret and elapsed_time >= timeskip:
            fs.append(to_pil(f))
            elapsed_time = 0
    cap.release()
    return fs


def get_vector(pil_image, model, transform, cuda):
    if cuda:
        t_img = Variable(transform(pil_image).unsqueeze(0).cuda(), requires_grad=False)
    else:
        t_img = Variable(transform(pil_image).unsqueeze(0), requires_grad=False)
    layer = model.avg_pool
    my_embedding = torch.zeros(2048)

    def copy_data(m, i, o):
        my_embedding.copy_(o.data[0,:,0,0])

    h = layer.register_forward_hook(copy_data)
    model(t_img)
    h.remove()

    return my_embedding


def save_vectors(video_name, max_vector, mean_vector):
    video_name = video_name.split(".")[0]
    max_path = DATASET_PATH + "resnext101/max/" + video_name + ".pkl"
    mean_path = DATASET_PATH + "resnext101/mean/" + video_name + ".pkl"
    with open(max_path, 'wb') as f_max:
        torch.save(max_vector, f_max)
    with open(mean_path, 'wb') as f_mean:
        torch.save(mean_vector, f_mean)


if __name__ == '__main__':

    video_list = os.listdir(VIDEOS_PATH)
    resnext101 = pretrainedmodels.resnext101_64x4d(num_classes=1000, pretrained='imagenet')
    cuda = torch.cuda.is_available()
    if cuda:
        resnext101 = resnext101.cuda()
    transform = utils.TransformImage(resnext101)

    n = len(video_list)
    with torch.no_grad():
        for i in range(0, n):
            frames = get_video_frames(VIDEOS_PATH + video_list[i])
            embeddings = []
            for frame in frames:
                embeddings.append(get_vector(frame, resnext101, transform, cuda))
            embeddings = torch.stack(embeddings)
            max_vector = embeddings.max(0).values
            mean_vector = embeddings.mean(0)
            save_vectors(video_list[i], max_vector, mean_vector)
            print(i, "of", n, "done")