import cv2.cv2 as cv2
from torchvision import transforms
from VideoVectorization.Resnext.mxnet_resnext101 import KitModel as Resnext101
import torch
import os
import pickle
import numpy as np
from PIL import Image

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/TGIF/gifs/"
SAVE_PATH = "/run/media/nbravo/Elements/TGIF/resnext101/"


def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    single_image = False
    if frame_count <= 0:
        return torch.Tensor([])
    t_frame = 1.0 / fps
    if fps < 1:
        single_image = True
    original_frame_skip = int(timeskip / t_frame)
    if original_frame_skip == 0:
        original_frame_skip = 1
    ret, f = cap.read()
    if single_image:
        return [f]
    index = 0
    while not ret:
        ret, f = cap.read()
        index += 1
    frames = [index]
    fs = [f]
    add = True
    next_frame = original_frame_skip
    while ret:
        ret, f = cap.read()
        index += 1
        if ret and index % next_frame == 0:
            fs.append(f)
            frames.append(index)
            next_frame = next_frame + original_frame_skip + add
            add = not add
    cap.release()
    return fs


def get_vectors(np_array_img_list, model, transform, device):
    embeddings = []
    for np_array_img in np_array_img_list:
        t_img = transform(Image.fromarray(np_array_img))
        x = torch.Tensor(np.array(t_img).transpose((2, 0, 1)))  # W x H x C -> C x W x H
        x = x.unsqueeze(0).to(device)
        embeddings.append(model(x).squeeze(0))
    return torch.stack(embeddings)


def save_vectors(video_name, embeddings=None, max_vector=None, mean_vector=None, save_mode='embed'): # save_mode = {embed, agg}
    video_name = video_name.split(".")[0]
    if save_mode == 'agg':
        max_path = SAVE_PATH + "max/" + video_name + ".pkl"
        mean_path = SAVE_PATH + "mean/" + video_name + ".pkl"
        if max_vector is not None:
            with open(max_path, 'wb') as f_max:
                pickle.dump(list(max_vector), f_max)
        if mean_vector is not None:
            with open(mean_path, 'wb') as f_mean:
                pickle.dump(list(mean_vector), f_mean)
    elif save_mode == 'embed':
        vectors_path = SAVE_PATH + "embeddings/" + video_name + ".pkl"
        if embeddings is not None:
            with open(vectors_path, 'wb') as f_vec:
                pickle.dump(embeddings, f_vec)
        else:
            raise ValueError
    else:
        raise ValueError


def vectorize():
    video_list = os.listdir(DATASET_PATH)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    resnext101 = Resnext101(weight_file='imagenet1k-resnext-101-64x4d-0000.params')
    resnext101 = resnext101.to(device)
    resnext101.eval()
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
    ])

    n = len(video_list)
    with torch.no_grad():
        for i in range(60000, n):
            frames = get_video_frames(DATASET_PATH + video_list[i]) # 61201
            embeddings = get_vectors(frames, resnext101, transform, device)
            # max_vector = embeddings.max(0).values
            # mean_vector = embeddings.mean(0)
            embeddings = embeddings.cpu().numpy()
            save_vectors(video_list[i], embeddings=embeddings, save_mode='embed')
            print(i, "of", n, "done")


if __name__ == '__main__':
    vectorize()