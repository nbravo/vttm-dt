import cv2.cv2 as cv2
from torchvision import transforms
from VideoVectorization.Resnext.mxnet_resnext101 import KitModel as Resnext101
import torch
import os
import pickle
import numpy as np
from PIL import Image

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/MSVD/videos/"
SAVE_PATH = "/run/media/nbravo/nbravo-2TB/MSVD/resnext101/"

def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    t_frame = 1.0 / fps
    original_frame_skip = int(timeskip / t_frame)

    ret, f = cap.read()
    index = 0
    while not ret:
        ret, f = cap.read()
        index += 1
    frames = [index]
    fs = [f]
    add = True
    next_frame = original_frame_skip
    while ret:
        ret, f = cap.read()
        index += 1
        if ret and index % next_frame == 0:
            fs.append(f)
            frames.append(index)
            next_frame = next_frame + original_frame_skip + add
            add = not add
    cap.release()
    return fs


def get_vectors(np_array_img_list, model, transform, device):
    embeddings = []
    for np_array_img in np_array_img_list:
        t_img = transform(Image.fromarray(np_array_img))
        x = torch.Tensor(np.array(t_img).transpose((2, 0, 1)))  # W x H x C -> C x W x H
        x = x.unsqueeze(0).to(device)
        embeddings.append(model(x).squeeze(0))
    return torch.stack(embeddings)


def save_vectors(video_name, embeddings=None, max_vector=None, mean_vector=None, save_mode='embed'): # save_mode = {embed, agg}
    video_name = video_name.split(".")[0]
    if save_mode == 'agg':
        max_path = SAVE_PATH + "max/" + video_name + ".pkl"
        mean_path = SAVE_PATH + "mean/" + video_name + ".pkl"
        if max_vector is not None:
            with open(max_path, 'wb') as f_max:
                pickle.dump(list(max_vector), f_max)
        if mean_vector is not None:
            with open(mean_path, 'wb') as f_mean:
                pickle.dump(list(mean_vector), f_mean)
    elif save_mode == 'embed':
        vectors_path = SAVE_PATH + "embeddings/" + video_name + ".pkl"
        if embeddings is not None:
            with open(vectors_path, 'wb') as f_vec:
                pickle.dump(embeddings, f_vec)
        else:
            raise ValueError
    else:
        raise ValueError


def vectorize():
    video_list = os.listdir(DATASET_PATH)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    resnext101 = Resnext101(weight_file='imagenet1k-resnext-101-64x4d-0000.params')
    resnext101 = resnext101.to(device)
    resnext101.eval()
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
    ])

    n = len(video_list)
    with torch.no_grad():
        for i in range(0, n):
            frames = get_video_frames(DATASET_PATH + video_list[i])
            embeddings = get_vectors(frames, resnext101, transform, device)
            # max_vector = embeddings.max(0).values
            # mean_vector = embeddings.mean(0)
            embeddings = embeddings.cpu().numpy()
            save_vectors(video_list[i], embeddings=embeddings, save_mode='embed')
            print(i, "of", n, "done")


def check_max_min_sequences():
    dataset_path = "/run/media/nbravo/nbravo-2TB/MSVD/resnext101/embeddings/"
    embeddings = os.listdir(dataset_path)

    # {21: 157, 6: 16, 15: 143, 11: 161, 9: 99, 17: 121, 13: 138, 12: 93, 22: 12, 29: 41, 19: 142, 31: 44, 14: 73, 10: 70, 23: 60, 30: 9, 36: 13, 34: 12, 25: 34, 7: 42, 18: 29, 32: 12, 74: 2, 27: 43, 53: 5, 16: 69, 8: 45, 33: 18, 46: 5, 57: 4, 35: 14, 20: 56, 43: 8, 28: 21, 26: 18, 24: 19, 44: 4, 48: 4, 63: 5, 60: 2, 42: 4, 40: 4, 5: 5, 56: 1, 41: 10, 51: 3, 47: 6, 59: 4, 95: 1, 39: 9, 37: 10, 4: 4, 45: 6, 61: 3, 64: 2, 91: 1, 81: 1, 78: 1, 94: 2, 70: 2, 86: 1, 92: 1, 38: 6, 87: 1, 68: 2, 50: 2, 73: 1, 84: 1, 65: 1, 71: 2, 55: 2, 49: 3, 120: 1, 62: 1, 52: 1, 72: 1, 89: 1}
    # min_len = 10000000
    # max_len = 0
    i = 0
    lenghts = dict()
    for emb in embeddings:
        len_embed = len(pickle.load(open(dataset_path + emb, 'rb')))
        if len_embed  not in lenghts.keys():
            lenghts[len_embed] = 1
        else:
            lenghts[len_embed] += 1
        # if len_embed > max_len:
        #     max_len = len_embed
        # if  len_embed < min_len:
        #     min_len = len_embed
        i += 1
        if i % 100 == 0:
            print(i, "of", len(embeddings))
    return lenghts



if __name__ == '__main__':
    vectorize()