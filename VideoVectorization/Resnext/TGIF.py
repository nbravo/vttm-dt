import cv2.cv2 as cv2
from torchvision import transforms
from VideoVectorization.Resnext.mxnet_resnext101 import KitModel as Resnext101
import torch
import os
import pickle
import numpy as np
from PIL import Image

DATASET_PATH = "/run/media/nbravo/nbravo-2TB/TGIF/gifs/"
SAVE_PATH = "/run/media/nbravo/Elements/TGIF/resnext101/"


def get_video_frames(path, timeskip=0.5):
    cap = cv2.VideoCapture(path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    single_image = False
    if frame_count == 0:
        return torch.Tensor([])
    t_frame = 1.0 / fps
    if fps < 1 or frame_count < 0:
        single_image = True
    original_frame_skip = int(timeskip / t_frame)
    if original_frame_skip == 0:
        original_frame_skip = 1
    ret, f = cap.read()
    if single_image:
        return [f]
    index = 0
    while not ret:
        ret, f = cap.read()
        index += 1
    frames = [index]
    fs = [f]
    add = True
    next_frame = original_frame_skip
    while ret:
        ret, f = cap.read()
        index += 1
        if ret and index % next_frame == 0:
            fs.append(f)
            frames.append(index)
            next_frame = next_frame + original_frame_skip + add
            add = not add
    cap.release()
    return fs


def get_vectors(np_array_img_list, model, transform, device):
    embeddings = []
    for np_array_img in np_array_img_list:
        t_img = transform(Image.fromarray(np_array_img))
        x = torch.Tensor(np.array(t_img).transpose((2, 0, 1)))  # W x H x C -> C x W x H
        x = x.unsqueeze(0).to(device)
        embeddings.append(model(x).squeeze(0))
    if len(embeddings) == 0:
        return torch.Tensor([])
    return torch.stack(embeddings)


def save_vectors(video_name, embeddings=None, max_vector=None, mean_vector=None, save_mode='embed'): # save_mode = {embed, agg}
    video_name = video_name.split(".")[0]
    if save_mode == 'agg':
        max_path = SAVE_PATH + "max/" + video_name + ".pkl"
        mean_path = SAVE_PATH + "mean/" + video_name + ".pkl"
        if max_vector is not None:
            with open(max_path, 'wb') as f_max:
                pickle.dump(list(max_vector), f_max)
        if mean_vector is not None:
            with open(mean_path, 'wb') as f_mean:
                pickle.dump(list(mean_vector), f_mean)
    elif save_mode == 'embed':
        vectors_path = SAVE_PATH + "embeddings/" + video_name + ".pkl"
        if embeddings is not None:
            with open(vectors_path, 'wb') as f_vec:
                pickle.dump(embeddings, f_vec)
        else:
            raise ValueError
    else:
        raise ValueError


def vectorize():
    video_list = os.listdir(DATASET_PATH)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    resnext101 = Resnext101(weight_file='imagenet1k-resnext-101-64x4d-0000.params')
    resnext101 = resnext101.to(device)
    resnext101.eval()
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
    ])

    n = len(video_list)
    with torch.no_grad():
        for i in range(2947, 60000):
            frames = get_video_frames(DATASET_PATH + video_list[i]) # 2947
            embeddings = get_vectors(frames, resnext101, transform, device)
            # max_vector = embeddings.max(0).values
            # mean_vector = embeddings.mean(0)
            embeddings = embeddings.cpu().numpy()
            save_vectors(video_list[i], embeddings=embeddings, save_mode='embed')
            print(i, "of", 60000, "done")

def check_max_min_sequences():
    dataset_path = "/run/media/nbravo/Elements/TGIF/resnext101/embeddings/"
    embeddings = os.listdir(dataset_path)
    #{7: 15281, 6: 19592, 5: 23583, 11: 6078, 4: 20288, 9: 9631, 3: 2447, 12: 2518, 22: 210, 8: 10643, 15: 1143, 14: 1371, 16: 852, 13: 2629, 26: 82, 17: 1299, 29: 42, 10: 5149, 18: 417, 24: 136, 19: 388, 25: 103, 21: 335, 1: 687, 28: 54, 35: 22, 39: 22, 30: 47, 44: 8, 20: 276, 33: 24, 37: 14, 23: 155, 27: 70, 36: 12, 41: 12, 82: 3, 62: 3, 59: 1, 34: 18, 32: 17, 52: 4, 38: 5, 31: 33, 50: 5, 78: 1, 43: 14, 47: 6, 45: 4, 48: 5, 93: 1, 76: 4, 66: 1, 56: 3, 51: 5, 49: 2, 69: 1, 61: 1, 40: 8, 57: 4, 54: 1, 68: 1, 46: 2, 72: 1, 135: 1, 63: 1, 53: 2, 67: 2, 55: 1, 65: 1}

    # min_len = 10000000
    # max_len = 0
    i = 0
    lenghts = dict()
    for emb in embeddings:
        len_embed = len(pickle.load(open(dataset_path + emb, 'rb')))
        if len_embed  not in lenghts.keys():
            lenghts[len_embed] = 1
        else:
            lenghts[len_embed] += 1
        # if len_embed > max_len:
        #     max_len = len_embed
        # if  len_embed < min_len:
        #     min_len = len_embed
        i += 1
        if i % 100 == 0:
            print(i, "of", len(embeddings))
    return lenghts

if __name__ == '__main__':
    vectorize()