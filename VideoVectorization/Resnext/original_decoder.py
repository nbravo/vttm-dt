from TextVectorization.bigfile import BigFile
import numpy as np
import pickle


def vtt2016():
    save_path = "/run/media/nbravo/Elements/VTT2016/resnext101/embeddings_original/"
    path = "/home/nbravo/VisualSearch/tv2016train/FeatureData/pyresnext-101_rbps13k,flatten0_output,os/"
    bf = BigFile(path)
    with open(path + "id.txt") as fin:
        ids = fin.readlines()
    ids = ids[0].strip().split(" ")
    assert len(ids) == bf.shape()[0], "Lengths not equals"
    frame_dict = {}

    for name in ids:
        _, video_id, frame_id = name.split("_")
        if video_id in frame_dict:
            frame_dict[video_id].append(int(frame_id))
        else:
            frame_dict[video_id] = [int(frame_id)]

    k = 0
    for video_id in frame_dict:
        frames_id = sorted(frame_dict[video_id])
        frames_id = list(map(lambda f: "tv2016train_" + video_id + "_" + str(f), frames_id))
        vectors = bf.read(frames_id)[1]
        vectors = np.array(vectors)
        pickle.dump(vectors, open(save_path + video_id.split("video")[1] + ".pkl", 'wb'))
        k += 1
        print(k, "of", len(frame_dict.keys()))


def msvd():
    save_path = "/run/media/nbravo/nbravo-2TB/MSVD/resnext101/embeddings_original/"
    path = "/home/nbravo/VisualSearch/msvd/FeatureData/pyresnext-101_rbps13k,flatten0_output,os/"
    bf = BigFile(path)
    with open(path + "id.txt") as fin:
        ids = fin.readlines()
    ids = ids[0].strip().split(" ")
    assert len(ids) == bf.shape()[0], "Lengths not equals"
    frame_dict = {}

    for name in ids:
        s = name.rsplit("_", 1)
        video_id = s[0]
        frame_id = s[1]
        if video_id in frame_dict:
            frame_dict[video_id].append(int(frame_id))
        else:
            frame_dict[video_id] = [int(frame_id)]

    k = 0
    for video_id in frame_dict:
        frames_id = sorted(frame_dict[video_id])
        frames_id = list(map(lambda f: video_id + "_" + str(f), frames_id))
        vectors = bf.read(frames_id)[1]
        vectors = np.array(vectors)
        pickle.dump(vectors, open(save_path + video_id.rsplit("_", 2)[0] + ".pkl", 'wb'))
        k += 1
        print(k, "of", len(frame_dict.keys()))


if __name__ == '__main__':
    msvd()