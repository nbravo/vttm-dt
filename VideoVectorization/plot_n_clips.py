import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import OrderedDict

matplotlib.use('PS')


MSR_COUNT = {24: 259, 43: 125, 26: 185, 21: 727, 22: 387, 25: 668, 23: 957, 41: 120, 36: 48, 29: 419, 53: 71, 35: 202, 52: 22, 31: 341, 37: 173, 27: 562, 28: 22, 19: 139, 49: 100, 61: 34, 33: 248, 51: 103, 44: 22, 34: 37, 39: 134, 47: 111, 55: 61, 32: 71, 58: 16, 45: 106, 57: 80, 59: 36, 20: 247, 54: 21, 48: 4, 42: 23, 56: 19, 40: 23, 46: 26, 30: 19, 38: 36, 50: 3, 18: 1, 60: 2}
TGIF_COUNT = {7: 15281, 6: 19592, 5: 23583, 11: 6078, 4: 20288, 9: 9631, 3: 2447, 12: 2518, 22: 210, 8: 10643, 15: 1143, 14: 1371, 16: 852, 13: 2629, 26: 82, 17: 1299, 29: 42, 10: 5149, 18: 417, 24: 136, 19: 388, 25: 103, 21: 335, 1: 687, 28: 54, 35: 22, 39: 22, 30: 47, 44: 8, 20: 276, 33: 24, 37: 14, 23: 155, 27: 70, 36: 12, 41: 12, 82: 3, 62: 3, 59: 1, 34: 18, 32: 17, 52: 4, 38: 5, 31: 33, 50: 5, 78: 1, 43: 14, 47: 6, 45: 4, 48: 5, 93: 1, 76: 4, 66: 1, 56: 3, 51: 5, 49: 2, 69: 1, 61: 1, 40: 8, 57: 4, 54: 1, 68: 1, 46: 2, 72: 1, 135: 1, 63: 1, 53: 2, 67: 2, 55: 1, 65: 1}
MSVD_COUNT = {21: 157, 6: 16, 15: 143, 11: 161, 9: 99, 17: 121, 13: 138, 12: 93, 22: 12, 29: 41, 19: 142, 31: 44, 14: 73, 10: 70, 23: 60, 30: 9, 36: 13, 34: 12, 25: 34, 7: 42, 18: 29, 32: 12, 74: 2, 27: 43, 53: 5, 16: 69, 8: 45, 33: 18, 46: 5, 57: 4, 35: 14, 20: 56, 43: 8, 28: 21, 26: 18, 24: 19, 44: 4, 48: 4, 63: 5, 60: 2, 42: 4, 40: 4, 5: 5, 56: 1, 41: 10, 51: 3, 47: 6, 59: 4, 95: 1, 39: 9, 37: 10, 4: 4, 45: 6, 61: 3, 64: 2, 91: 1, 81: 1, 78: 1, 94: 2, 70: 2, 86: 1, 92: 1, 38: 6, 87: 1, 68: 2, 50: 2, 73: 1, 84: 1, 65: 1, 71: 2, 55: 2, 49: 3, 120: 1, 62: 1, 52: 1, 72: 1, 89: 1}

if __name__ == '__main__':
    msr = list(OrderedDict(sorted(MSR_COUNT.items())).items())
    tgif = list(OrderedDict(sorted(TGIF_COUNT.items())).items())
    msvd = list(OrderedDict(sorted(MSVD_COUNT.items())).items())
    msr_n_clips = []
    msr_accu = []
    msr_total = 0
    msvd_n_clips = []
    msvd_accu = []
    msvd_total = 0
    tgif_n_clips = []
    tgif_accu = []
    tgif_total = 0
    for n_clip, count in msr:
        msr_total += count
        msr_accu.append(msr_total)
        msr_n_clips.append(n_clip)

    for n_clip, count in msvd:
        msvd_total += count
        msvd_accu.append(msvd_total)
        msvd_n_clips.append(n_clip)

    for n_clip, count in tgif:
        tgif_total += count
        tgif_accu.append(tgif_total)
        tgif_n_clips.append(n_clip)

    msr_accu = [acc / msr_total for acc in msr_accu]
    msvd_accu = [acc / msvd_total for acc in msvd_accu]
    tgif_accu = [acc / tgif_total for acc in tgif_accu]
    plt.plot(msr_n_clips, msr_accu)
    plt.plot(msvd_n_clips, msvd_accu)
    plt.plot(tgif_n_clips, tgif_accu)
    plt.legend(['MSR', 'MSVD', 'TGIF'])
    plt.grid()
    plt.xticks(ticks=range(0, msr_n_clips[-1] + 20, 5))
    plt.xlim((0, msr_n_clips[-1] + 20))
    plt.yticks(ticks=np.arange(0, 1.1, 0.05))
    plt.ylim((0, 1))
    plt.xlabel("Number of clips extracted")
    plt.ylabel("% of videos with less or equal number of clips extracted")
    #plt.plot(tgif)
    #plt.plot(msvd)
    #plt.show()
    plt.savefig("video_frame_limit.eps", format="eps")
