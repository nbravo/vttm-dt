import torch
from VideoVectorization.mxnet_model.mxnet_resnext101 import KitModel as Resnext101
from torchvision import transforms
from VideoVectorization.mxnet_model.synset import synset
from PIL import Image
import numpy as np

if __name__ == '__main__':
    model = Resnext101(weight_file='imagenet1k-resnext-101-64x4d-0000.params')
    model.eval()
    model = model.cuda()
    image_path = "elephant.jpg"
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    img = Image.open(image_path)
    preprocessing = transforms.Compose([
        transforms.Resize((224, 224)),
    ])
    x = preprocessing(img)
    x = torch.Tensor(np.array(x).transpose((2, 0, 1)))  # W x H x C -> C x W x H
    x = x.unsqueeze(0).cuda()

    with torch.no_grad():
        output = model(x)
    max, argmax = output.data.squeeze().max(0)
    class_id = argmax.item()
    class_name = synset[class_id]

    # Print the top-5 Results:
    h_x = output.data.squeeze()
    probs, idx = h_x.sort(0, True)
    print('Top-5 Results: ')
    for i in range(0, 5):
        print('{:.2f}% -> {}'.format(probs[i] * 100.0, synset[idx[i].item()]))
    str_final_label = 'The Image is a ' + class_name + '.'
    print(str_final_label)